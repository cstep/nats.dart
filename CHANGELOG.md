## 0.0.5

- upgrade all dependencies to latest versions
- additional test coverage

## 0.0.4

- fix pub score
- upgrade Kiwi dependency to 2.0.0

## 0.0.3

- BREAKING: Rename `data` argument to `payload` for methods `Client.publish()` and `Client.request()`

## 0.0.1

- Initial version
