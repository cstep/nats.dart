import 'package:nats_dart/nats_dart.dart';
import 'package:mockito/mockito.dart';
import '../MessageProtoMocks.dart';

class MessageProtoFactoryMock extends Mock implements MessageProtoFactory {
  @override
  InfoMessageProtoMock info({String serverId, String version, String goVersion, String host, int port, int maxPayload, int protocol, int clientId, bool authRequired, bool tlsRequired, bool tlsVerify, List<String> connectUrls}) {
    return InfoMessageProtoMock({
      "serverId"    : serverId,
      "version"     : version,
      "goVersion"   : goVersion,
      "host"        : host,
      "port"        : port,
      "maxPayload"  : maxPayload,
      "clientId"    : clientId,
      "protocol"    : protocol,
      "authRequired": authRequired,
      "tlsRequired" : tlsRequired,
      "tlsVerify"   : tlsVerify,
      "connectUrls" : connectUrls,
    });
  }

  @override
  ConnectMessageProtoMock connect(ServerOptions options) {
    return ConnectMessageProtoMock({
      "options": options,
    });
  }

  @override
  PubMessageProtoMock pub(String subject, int byteLength, {String replySubject}) {
    return PubMessageProtoMock({
      "subject"     : subject,
      "byteLength"  : byteLength,
      "replySubject": replySubject,
    });
  }

  @override
  SubMessageProtoMock sub(String subject, String sid, {String queue}) {
    return SubMessageProtoMock({
      "subject": subject,
      "sid"    : sid,
      "queue"  : queue,
    });
  }

  @override
  UnsubMessageProtoMock unsub(String sid, {int max}) {
    return UnsubMessageProtoMock({
      "sid": sid,
      "max": max,
    });
  }

  @override
  DataMessageProtoMock data(String subject, String sid, int byteLength, {String replySubject}) {
    return DataMessageProtoMock({
      "subject"     : subject,
      "sid"         : sid,
      "byteLength"  : byteLength,
      "replySubject": replySubject,
    });
  }

  @override
  PingMessageProtoMock ping(){
    return PingMessageProtoMock({});
  }

  @override
  PongMessageProtoMock pong(){
    return PongMessageProtoMock({});
  }

  @override
  OkMessageProtoMock ok() {
    return OkMessageProtoMock({});
  }

  @override
  ErrorMessageProtoMock error(String message) {
    return ErrorMessageProtoMock({
      "message": message
    });
  }
}
