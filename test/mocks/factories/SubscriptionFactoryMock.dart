import 'package:nats_dart/nats_dart.dart';
import 'package:mockito/mockito.dart';
import '../SubscriptionMocks.dart';

class SubscriptionFactoryMock extends Mock implements SubscriptionFactory {
  @override
  SubscriptionMock subscription(ProtocolHandler protocolHandler, String subject, {String queue, int max, String sid}) {
    return SubscriptionMock({
      "protocolHandler": protocolHandler,
      "subject"        : subject,
      "queue"          : queue,
      "max"            : max,
      "sid"            : sid,
    });
  }

  @override
  RequestSubscriptionMock requestSubscription(ProtocolHandler protocolHandler, String inbox) {
    return RequestSubscriptionMock({
      "protocolHandler": protocolHandler,
      "inbox"          : inbox,
    });
  }
}
