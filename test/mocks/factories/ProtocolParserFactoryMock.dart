import 'dart:convert';
import 'package:mockito/mockito.dart';
import 'package:nats_dart/nats_dart.dart';
import '../ProtocolParserMock.dart';

class ProtocolParserFactoryMock extends Mock implements ProtocolParserFactory {
  @override
  ProtocolParserMock create(Encoding encoding) => ProtocolParserMock();
}
