//import 'package:nats_dart/nats_dart.dart';
import 'dart:convert';
import 'dart:typed_data';
import 'package:nats_dart/nats_dart.dart';
import 'package:mockito/mockito.dart';
import '../MessageMocks.dart';

class MessageFactoryMock extends Mock implements MessageFactory {
  @override
  ConnectMessageMock connect(ServerOptions options) {
    return ConnectMessageMock({
      "options": options
    });
  }

  @override
  DataMessageMock data(String subject, String sid, Uint8List payload, {Encoding encoding = ascii, String replySubject}) {
    return DataMessageMock({
      "subject"     : subject,
      "sid"         : sid,
      "payload"     : payload,
      "encoding"    : encoding,
      "replySubject": replySubject,
    });
  }

  @override
  ErrorMessageMock error(String message) {
    return ErrorMessageMock({
      "message": message,
    });
  }

  @override
  InfoMessageMock info(String serverId, String version, String go, String host, int port, int maxPayload, int protocol, int clientId, {bool authRequired, bool tlsRequired, bool tlsVerify, List<String> connectUrls}) {
    return InfoMessageMock({
      "serverId"    : serverId,
      "version"     : version,
      "go"          : go,
      "host"        : host,
      "port"        : port,
      "maxPayload"  : maxPayload,
      "protocol"    : protocol,
      "clientId"    : clientId,
      "authRequired": authRequired,
      "tlsRequired" : tlsRequired,
      "tlsVerify"   : tlsVerify,
      "connectUrls" : connectUrls,
    });
  }

  @override
  OkMessageMock ok() {
    return OkMessageMock({});
  }

  @override
  PingMessageMock ping() {
    return PingMessageMock({});
  }

  @override
  PongMessageMock pong() {
    return PongMessageMock({});
  }

  @override
  PubMessageMock pub(String subject, dynamic payload, {Encoding encoding = ascii, String replySubject}) {
    return PubMessageMock({
      "subject"     : subject,
      "payload"  : payload,
      "encoding"    : encoding,
      "replySubject": replySubject,
    });
  }

  @override
  SubMessageMock sub(String subject, String sid, {String queue}) {
    return SubMessageMock({
      "subject": subject,
      "sid"    : sid,
      "queue"  : queue,
    });
  }

  @override
  UnsubMessageMock unsub(String sid, {int max}) {
    return UnsubMessageMock({
      "sid": sid,
      "max": max,
    });
  }

  // --

  @override
  DataMessageMock dataFromProto(DataMessageProto proto, Uint8List payload, {Encoding encoding = ascii}) {
    return DataMessageMock({
      "proto"   : proto,
      "payload" : payload,
      "encoding": encoding,
    });
  }

  @override
  ErrorMessageMock errorFromProto(ErrorMessageProto proto) {
    return ErrorMessageMock({
      "proto": proto,
    });
  }

  @override
  InfoMessageMock infoFromProto(InfoMessageProto proto) {
    return InfoMessageMock({
      "proto": proto,
    });
  }

  @override
  OkMessageMock okFromProto(OkMessageProto proto) {
    return OkMessageMock({
      "proto": proto,
    });
  }

  @override
  PingMessageMock pingFromProto(PingMessageProto proto) {
    return PingMessageMock({
      "proto": proto,
    });
  }

  @override
  PongMessage pongFromProto(PongMessageProto proto) {
    return PongMessageMock({
      "proto": proto,
    });
  }

  // --

  @override
  SubMessageMock subFromSubscription(Subscription subscription) {
    return SubMessageMock.fromSubscription(subscription);
  }

  @override
  UnsubMessageMock unsubFromSubscription(Subscription subscription, {int max}) {
    return UnsubMessageMock.fromSubscription(subscription, max: max);
  }
}
