import 'package:nats_dart/nats_dart.dart';
import 'package:mockito/mockito.dart';

import '../ProtocolHandlerMock.dart';

class ProtocolHandlerFactoryMock extends Mock implements ProtocolHandlerFactory {
  @override
  ProtocolHandlerMock create(ServerSet servers) => ProtocolHandlerMock(servers);
}
