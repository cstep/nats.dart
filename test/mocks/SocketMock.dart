import 'dart:async';
import 'dart:convert';
import 'dart:io';
import 'dart:typed_data';

import 'package:faker_extended/faker_extended.dart';
import 'package:mockito/mockito.dart';

class SocketMock extends Mock implements Socket {
  final _streamController = StreamController<Uint8List>();

  bool _isClosed = false;

  Stream<Uint8List>     __broadcastStream;
  Stream<Uint8List> get  _broadcastStream => (__broadcastStream ??= _streamController.stream.asBroadcastStream());

  final List<int> _collectedData = [];
  List<int> get   collectedData => _collectedData;

  SocketMock() {
    when(asBroadcastStream()).thenAnswer((_) => _broadcastStream);
    when(encoding).thenReturn(ascii);
  }

  factory SocketMock.connect([Duration latency]) {
    return SocketMock()..simulateConnectionEstablished(latency);
  }

  void simulateConnectionEstablished([Duration latency]) {
    _isClosed = false;
    latency ??= Duration(milliseconds: 100);
    Future.delayed(latency, simulateIncomingInfoMessage);
  }

  void simulateIncomingInfoMessage() {
    if (_isClosed) return; // -- early
    _streamController.add(ascii.encode('INFO {"server_id":"NDAVBRAOAC4HGFWODYJ2QNBX3XYBHMF5KSERQU6KCL5KMV7UWZTXJLWR","version":"2.0.4","go":"go1.11.13","host":"0.0.0.0","port":4222,"max_payload":1048576,"protocol":1,"client_id":4,"auth_required":false}\r\n')); // TODO: should we faker-ize this string?
  }

  void simulateIncomingErrorMessage([String message]) {
    if (_isClosed) return; // -- early
    message ??= faker.lorem.sentence();
    _streamController.add(ascii.encode('-ERR $message\r\n'));
  }

  void simulateIncomingDataMessage(String sid, [String payload]) {
    if (_isClosed) return; // -- early
    payload ??= faker.lorem.sentences(faker.randomGenerator.integer(10)).join(' ');
    var subject = faker.lorem.word();
    _streamController.add(ascii.encode('MSG $subject $sid ${payload.length}\r\n$payload\r\n'));
  }

  Future simulateClose() {
    if (_isClosed) return Future.value(); // -- early
    _isClosed = true;
    return _streamController.close();
  }

  @override
  void add(List<int> data) {
    _collectedData.addAll(data);
  }
}
