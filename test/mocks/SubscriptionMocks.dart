import 'dart:async';
import 'package:nats_dart/nats_dart.dart';
import '_ConstructorValueMock.dart';

abstract class SubscriptionMockBase extends ConstructorValueMock implements Subscription {
  @override int  get max            => values['max']??0;
  @override void set max(int value) => values['max'] = value;

  @override int    get receivedCount => 0;

  @override
  Stream<DataMessage> get stream {
    return asNonBroadcastStream((values['protocolHandler'] as ProtocolHandler)
      .on<DataMessage>())
      .where((message) => (message.sid == values['sid']))
      .asBroadcastStream();
  }

  SubscriptionMockBase(Map<String, dynamic> values) : super(values);

  @override
  dynamic noSuchMethod(Invocation invocation, {Object/*?*/ returnValue, Object/*?*/ returnValueForMissingStub = const Object()}) {
    if (invocation.memberName == #_stream) {
      // _stream is normally private and as such, when using this mock, wont be accessible from inside our package
      return this.stream; // proxy our mock stream
    } else {
      return super.noSuchMethod(invocation);
    }
  }
}

class SubscriptionMock        extends SubscriptionMockBase implements Subscription        {        SubscriptionMock(Map<String, dynamic> values) : super(values); }
class RequestSubscriptionMock extends SubscriptionMock     implements RequestSubscription { RequestSubscriptionMock(Map<String, dynamic> values) : super(values); }
