import 'package:nats_dart/nats_dart.dart';

import '_ConstructorValueMock.dart';

abstract class MessageProtoMock extends ConstructorValueMock implements MessageProto {
  MessageProtoMock(Map<String, dynamic> values): super(values);

  @override
  String toProtocolString() => Enum.asString(messageType);
}

class InfoMessageProtoMock    extends MessageProtoMock implements InfoMessageProto    { @override MessageType get messageType => MessageType.INFO;       InfoMessageProtoMock(Map<String, dynamic> values): super(values); }
class PubMessageProtoMock     extends MessageProtoMock implements PubMessageProto     { @override MessageType get messageType => MessageType.PUB;         PubMessageProtoMock(Map<String, dynamic> values): super(values); }
class PingMessageProtoMock    extends MessageProtoMock implements PingMessageProto    { @override MessageType get messageType => MessageType.PING;       PingMessageProtoMock(Map<String, dynamic> values): super(values); }
class PongMessageProtoMock    extends MessageProtoMock implements PongMessageProto    { @override MessageType get messageType => MessageType.PONG;       PongMessageProtoMock(Map<String, dynamic> values): super(values); }
class OkMessageProtoMock      extends MessageProtoMock implements OkMessageProto      { @override MessageType get messageType => MessageType.OK;           OkMessageProtoMock(Map<String, dynamic> values): super(values); }
class ErrorMessageProtoMock   extends MessageProtoMock implements ErrorMessageProto   { @override MessageType get messageType => MessageType.ERROR;     ErrorMessageProtoMock(Map<String, dynamic> values): super(values); }
class DataMessageProtoMock    extends MessageProtoMock implements DataMessageProto    { @override MessageType get messageType => MessageType.MESSAGE;    DataMessageProtoMock(Map<String, dynamic> values): super(values); }

class ConnectMessageProtoMock extends MessageProtoMock implements ConnectMessageProto { @override MessageType get messageType => MessageType.CONNECT; ConnectMessageProtoMock(Map<String, dynamic> values): super(values);
  @override void set options(ServerOptions newValue) => super.values['options'] = newValue;
}

class SubMessageProtoMock     extends MessageProtoMock implements SubMessageProto     { @override MessageType get messageType => MessageType.SUB;         SubMessageProtoMock(Map<String, dynamic> values): super(values);
  @override void set subject(String newValue) => super.values['subject'] = newValue;
  @override void set sid(    String newValue) => super.values['sid']     = newValue;
  @override void set queue(  String newValue) => super.values['queue']   = newValue;
}

class UnsubMessageProtoMock   extends MessageProtoMock implements UnsubMessageProto   { @override MessageType get messageType => MessageType.UNSUB;     UnsubMessageProtoMock(Map<String, dynamic> values): super(values);
  @override void set sid(    String newValue) => super.values['sid'] = newValue;
  @override void set max(    int    newValue) => super.values['max'] = newValue;
}
