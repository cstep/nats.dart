import 'dart:convert';
import 'dart:mirrors';
import 'dart:typed_data';
import 'package:nats_dart/nats_dart.dart';
import 'MessageProtoMocks.dart';
import '_ConstructorValueMock.dart';

abstract class MessageMock extends ConstructorValueMock implements Message {
  MessageMock(values): super(values);

  @override String toProtocolString() => this.proto.toProtocolString();

  @override
  dynamic noSuchMethod(Invocation invocation, {Object/*?*/ returnValue, Object/*?*/ returnValueForMissingStub = const Object()}) {
    if (invocation.isGetter) {
      return (super.noSuchMethod(invocation)??reflect(values['proto']).delegate(invocation));
    } else {
      return super.noSuchMethod(invocation);
    }
  }
}

abstract class PayloadMessageMock extends MessageMock implements PayloadMessage {
  @override Uint8List get encodedPayload => (payload is  List<int> ? payload : (encoding??ascii).encode('$payload'));
  @override String    get decodedPayload => (payload is! List<int> ? payload : (encoding??ascii).decode(  payload ));

  PayloadMessageMock(Map<String, dynamic> values): super(values..putIfAbsent('payload', () => ''));
}

class ConnectMessageMock extends MessageMock        implements ConnectMessage { @override ConnectMessageProtoMock get proto => values.putIfAbsent('proto', () => ConnectMessageProtoMock({})); ConnectMessageMock(Map<String, dynamic> values): super(values); }
class ErrorMessageMock   extends MessageMock        implements ErrorMessage   { @override ErrorMessageProtoMock   get proto => values.putIfAbsent('proto', () =>   ErrorMessageProtoMock({}));   ErrorMessageMock(Map<String, dynamic> values): super(values); }
class InfoMessageMock    extends MessageMock        implements InfoMessage    { @override InfoMessageProtoMock    get proto => values.putIfAbsent('proto', () =>    InfoMessageProtoMock({}));    InfoMessageMock(Map<String, dynamic> values): super(values); }
class OkMessageMock      extends MessageMock        implements OkMessage      { @override OkMessageProtoMock      get proto => values.putIfAbsent('proto', () =>      OkMessageProtoMock({}));      OkMessageMock(Map<String, dynamic> values): super(values); }
class PingMessageMock    extends MessageMock        implements PingMessage    { @override PingMessageProtoMock    get proto => values.putIfAbsent('proto', () =>    PingMessageProtoMock({}));    PingMessageMock(Map<String, dynamic> values): super(values); }
class PongMessageMock    extends MessageMock        implements PongMessage    { @override PongMessageProtoMock    get proto => values.putIfAbsent('proto', () =>    PongMessageProtoMock({}));    PongMessageMock(Map<String, dynamic> values): super(values); }
class DataMessageMock    extends PayloadMessageMock implements DataMessage    { @override DataMessageProtoMock    get proto => values.putIfAbsent('proto', () =>    DataMessageProtoMock({}));    DataMessageMock(Map<String, dynamic> values): super(values); }
class PubMessageMock     extends PayloadMessageMock implements PubMessage     { @override PubMessageProtoMock     get proto => values.putIfAbsent('proto', () =>     PubMessageProtoMock({}));     PubMessageMock(Map<String, dynamic> values): super(values); }

class SubMessageMock extends MessageMock implements SubMessage {
  @override SubMessageProtoMock get proto => values.putIfAbsent('proto', () => SubMessageProtoMock({}));

  SubMessageMock(Map<String, dynamic> values): super(values);

  SubMessageMock.fromSubscription(Subscription subscription): this({
    "subject": subscription.subject,
    "sid": subscription.sid,
    "queue": subscription.queue,
  });
}

class UnsubMessageMock extends MessageMock implements UnsubMessage {
  @override UnsubMessageProtoMock get proto => values.putIfAbsent('proto', () => UnsubMessageProtoMock({}));

  UnsubMessageMock(Map<String, dynamic> values): super(values);

  UnsubMessageMock.fromSubscription(Subscription subscription, {int max}): this({
    "sid": subscription.sid,
    "max": max
  });
}
