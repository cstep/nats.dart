import 'package:nats_dart/nats_dart.dart';
import 'package:mockito/mockito.dart';

class ServerOptionsMock extends Mock implements ServerOptions {
  Map<String, dynamic> values;

  ServerOptionsMock({this.values = const {}}) {
    when(toJsonString()).thenReturn('{optionsJson}');
    when(isFinalized).thenReturn(false);
    when(tlsRequired).thenReturn(false);
    when(username).thenReturn(values['username']);
    when(password).thenReturn(values['password']);
  }
}
