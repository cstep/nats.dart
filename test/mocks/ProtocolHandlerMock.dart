import 'dart:async';
import 'package:nats_dart/nats_dart.dart';
import 'package:mockito/mockito.dart';

class ProtocolHandlerMock extends Mock implements ProtocolHandler {
  final StreamController    _mainStream          = StreamController.broadcast();
  final SubscriptionFactory _subscriptionFactory = container.resolve<SubscriptionFactory>();

  ServerSet servers;

  ProtocolHandlerMock([this.servers]) {
    when(unsubscribe(any)).thenReturn(null); // -- we'll be verifying (later) that this method actually gets called

    when(subscribe(any, queue: anyNamed('queue'), max: anyNamed('max'))).thenAnswer((i)
      => _subscriptionFactory.subscription(this, i.positionalArguments.first, queue: i.namedArguments[#queue], max: i.namedArguments[#max]));

    when(registerRequest(any)).thenAnswer((i)
      => _subscriptionFactory.subscription(this, i.positionalArguments.first));

    when(connect(any, timeout: anyNamed('timeout'))).thenAnswer((i)
      => Future(() => null));
  }

  @override
  Stream<T> on<T extends Event>() => _mainStream.stream.where((event) => event is T).cast<T>();

  void fakeIncoming<T extends IncomingMessage>(T message) => _mainStream.add(message);
}
