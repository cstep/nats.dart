import 'dart:async';
import 'dart:convert';
import 'dart:typed_data';
import 'package:nats_dart/nats_dart.dart';
import 'package:mockito/mockito.dart';

/* TODO
   I'm not a fan of how this is being done right now... it just doesn't feel right.
   At the moment, I'm thinking it might be best to start using the ProtocolParser more like a 2-way
   encoder instead -- meaning it would handle converting BOTH directions between (protocol stream data <=> DataMessage instances)
   Doing it this way would remove the coupling between the Socket used by the ProtocolHandler and this ProtocolParser
   meaning that this Parser (which is really just a stream transformer) would then be free to transform any incoming stream
   without being constrained to receiving a Stream<Uint8List>
 */

class ProtocolParserMock extends Mock implements ProtocolParser {
  @override final List<int> SEPARATOR = [];

  // -------- delegation
  StreamTransformer<String, Message>  _delegate;
  @override Stream<Message>           bind(Stream<Uint8List> stream) => _delegate.bind(LineSplitter().bind(ascii.decoder.bind(stream))); // -- assume ascii and just blindly split on lines
  @override StreamTransformer<RS, RT> cast<RS, RT>()                 => _delegate.cast<RS, RT>();

  ProtocolParserMock() {
    _delegate = StreamTransformer<String, Message>.fromHandlers(
      handleData: (String messageData, EventSink<Message> sink) {
        _parseMessages(messageData).forEach(sink.add);
      }
    );
  }

  List<Message> _parseMessages(String messageData) {
    MessageFactory _messageFactory = container.resolve<MessageFactory>();
    List<Message>  messages        = [];

    switch (Enum.fromString<MessageType>(messageData.split(RegExp(r'\s+')).first)) {
      case MessageType.MESSAGE: messages.add(_messageFactory.data(null, null, null)                              ); break;
      case MessageType.INFO   : messages.add(_messageFactory.info(null, null, null, null, null, null, null, null)); break;
      case MessageType.PING   : messages.add(_messageFactory.ping()                                              ); break;
      case MessageType.PONG   : messages.add(_messageFactory.pong()                                              ); break;
      case MessageType.OK     : messages.add(_messageFactory.ok()                                                ); break;
      case MessageType.ERROR  : messages.add(_messageFactory.error(null)                                         ); break;

      default: throw FormatException('Invalid message packet received: Unsupported incoming message type: ${messageData}');
    }

    return messages;
  }
}
