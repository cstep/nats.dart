import 'dart:async';
import 'dart:mirrors';
import 'package:nats_dart/nats_dart.dart';
import 'package:event_bus/event_bus.dart';
import 'package:faker_extended/faker_extended.dart';
import 'package:pedantic/pedantic.dart';
import '../helpers/ioOverrides.dart';
import '../helpers/matchers/matchers.dart';
import '../mocks/SocketMock.dart';
import '../mocks/factories/MessageFactoryMock.dart';
import '../mocks/factories/MessageProtoFactoryMock.dart';
import '../mocks/factories/ProtocolParserFactoryMock.dart';
import '../mocks/factories/SubscriptionFactoryMock.dart';

void main() {
  container
    ..unregister<MessageProtoFactory>()  ..registerInstance<MessageProtoFactory  >(MessageProtoFactoryMock()  )
    ..unregister<MessageFactory>()       ..registerInstance<MessageFactory       >(MessageFactoryMock()       )
    ..unregister<SubscriptionFactory>()  ..registerInstance<SubscriptionFactory  >(SubscriptionFactoryMock()  )
    ..unregister<ProtocolParserFactory>()..registerInstance<ProtocolParserFactory>(ProtocolParserFactoryMock());

  ProtocolHandler handler;
  EventBus        reflectedEventStream;
  SocketMock      reflectedSocket;

  /// don't make the test runner guess when we're done
  void closeEventStream([Duration delay]) {
    delay ??= Duration(seconds: 0); // -- immediate
    Future.delayed(delay, reflectedEventStream.streamController.close)
      .then((_) => reflectedSocket?.close());
  }

  setUp(() {
    handler              = ProtocolHandler(ServerSet.from([DEFAULT_HOST])); // -- TODO: mock this ServerSet... somehow... so we're not dependent on its proper function in order to test our ProtocolHandler
    reflectedEventStream = reflect(handler).getField(MirrorSystem.getSymbol('_eventBus', reflect(handler).type.owner)).reflectee;
  });

  tearDown(() {
    closeEventStream();
  });

  test('can be created', () {
    var expectedServers = ServerSet.from([DEFAULT_HOST]); // -- TODO: mock this... somehow... so we're not dependent on its proper function in order to test our ProtocolHandler
    var handler         = ProtocolHandler(expectedServers);

    var actualServers = reflect(handler).getField(MirrorSystem.getSymbol('_servers', reflect(handler).type.owner)).reflectee;

    expect(actualServers,       equals(expectedServers), reason: 'Supplied servers not used by instance');
    expect(handler.isConnected, isFalse,                 reason: '`isConnected` reporting value other than expected');
    expect(handler.isClosed,    isFalse,                 reason:    '`isClosed` reporting value other than expected');
    expect(handler.isDraining,  isFalse,                 reason:  '`isDraining` reporting value other than expected');
  });

  test('can connect', () async {
    var stream = reflectedEventStream.on();

    // ======== Connect
    unawaited(handler.connect(ServerOptions()));

    // ======== Verify Results
    // -------- emitted events
    expect(stream, emitsInOrder([isA<InfoMessage>(), isA<ConnectEvent>(), emitsDone]));

    // -------- protocol handler state
    await stream.firstWhere((e) => e is ConnectEvent); // -- wait for connection to be complete
    expect(handler.isConnected, isTrue,  reason: '`isConnected` reporting value other than expected');
    expect(handler.isClosed,    isFalse, reason:    '`isClosed` reporting value other than expected');
    expect(handler.isDraining,  isFalse, reason:  '`isDraining` reporting value other than expected');

    // ======== Finish
    closeEventStream(); // -- don't make the test runner guess when we're done
  }, timeout: Timeout(Duration(seconds: 2)));

  //@depends('can connect')
  test('can disconnect', () async {
    var stream = reflectedEventStream.on<Event>();
    var whenConnected = stream.firstWhere((e) => e is ConnectEvent); // -- wait for connection to be complete

    // ======== Connect
    unawaited(handler.connect(ServerOptions()));

    // ======== Close the Connection
    unawaited(whenConnected.then((_) async {
      // -------- close the connection
      reflectedSocket = reflect(handler).getField(MirrorSystem.getSymbol('_socket', reflect(handler).type.owner)).reflectee;
      await reflectedSocket.simulateClose(); // -- this closes the socket, but not our event stream

      expect(handler.isConnected, isFalse, reason: '`isConnected` reporting value other than expected');
      expect(handler.isClosed,    isTrue,  reason:    '`isClosed` reporting value other than expected');
      expect(handler.isDraining,  isFalse, reason:  '`isDraining` reporting value other than expected');

      // ======== Finish
      closeEventStream(); // -- don't make the test runner guess when we're done
    }));

    // ======== Verify Results
    // -------- emitted events
    expect(stream, emitsInOrder([
      emitsThrough(isA<ConnectEvent>()),
      isA<DisconnectEvent>(),
      emitsDone,
    ]));
  }, timeout: Timeout(Duration(seconds: 2)));

  /// TODO
  ///       all of these tests are checking to be sure we see a command appear on the socket, but we should *also* be checking that the command is well-formed
  ///       this needs to be done *without* relying on the Message/MessageProto classes' implementation of toProtocolString()
  ///       this means (one possibility, at least) implementing the spy pattern to spy on our protocolHandler instance, intercept, and inspect the [Message]
  ///       being submitted to the [ProtocolHandler.sendCommand()] method to be sure it contains all the options/values that were expected
  group('can send commands', () {
    final List<int> SEPARATOR = container.resolve<ProtocolParserFactory>().create(null).SEPARATOR;
    void expectMessageType(MessageType messageType, {String reason}) {
      var startingSequence = reflectedSocket.encoding.encode(Enum.asString(messageType)) + SEPARATOR;
      expect(reflectedSocket.collectedData, startsWithInOrder(startingSequence), reason: reason);
      reflectedSocket.collectedData.removeRange(0, startingSequence.length);
    }

    setUp(() {
      return handler.connect(ServerOptions())
        ..then((_) {
          reflectedSocket = reflect(handler).getField(MirrorSystem.getSymbol('_socket', reflect(handler).type.owner)).reflectee;
          reflectedSocket.collectedData.clear();
        });
    });

    test('sendPing()', () { // -- no optional arguments
      handler.sendPing();
      expectMessageType(MessageType.PING);
      closeEventStream(); // -- don't make the test runner guess when we're done
    });

    test('sendPong()', () { // -- no optional arguments
      handler.sendPong();
      expectMessageType(MessageType.PONG, reason: 'Data written to socket other than expected');
      closeEventStream(); // -- don't make the test runner guess when we're done
    });

    test('publish()', () {
      var expectedSubject = faker.lorem.word();
      var expectedPayload = faker.lorem.sentences(faker.randomGenerator.integer(10)).join();

      handler.publish(expectedSubject, expectedPayload);

      expectMessageType(MessageType.PUB, reason: 'Data written to socket other than expected');
      closeEventStream(); // -- don't make the test runner guess when we're done
    });

    group('subscribe()', () {
      setUp(() {
        // -------- verify events are fired
        Stream<NonMessageEvent> mainStream = reflectedEventStream.on<NonMessageEvent>();
        expect(mainStream, emitsInOrder([
          isA<SubscribeEvent>(),
          emitsDone
        ]), reason: 'Fired event-sequence other than expected');
      });

      test('open-ended', () {
        // ======== Subscribe
        var subscription = handler.subscribe(faker.lorem.word());

        // ======== Verify Results
        // -------- socket sends to server
        expectMessageType(MessageType.SUB,          reason: 'Data written to socket other than expected'      );
        expect(handler.subscriptions, hasLength(1), reason: 'Subscription not added to collection as expected');

        // -------- subscription is correct
        expect(subscription,          isA<Subscription>(),    reason: 'returned subscription of type other than expected'                                     );
        expect(subscription.max,      equals(0),              reason: 'Recorded value for `max` other than expected'                                          );
        expect(handler.subscriptions, contains(subscription), reason: 'Returned subscription unexpectedly differs from the one tracked by the ProtocolHandler');

        closeEventStream(); // -- don't make the test runner guess when we're done
      });

      test('with limit', () async {
        // ======== Subscribe
        var subscription = handler.subscribe(faker.lorem.word(), max: 1);

        // ======== Verify Results
        // -------- socket sends to server
        expectMessageType(MessageType.SUB,          reason: 'Data written to socket other than expected'      );
        expect(handler.subscriptions, hasLength(1), reason: 'Subscription not added to collection as expected');

        // -------- subscription is correct
        expect(subscription,          isA<Subscription>(),    reason: 'returned subscription of type other than expected'                                     );
        expect(subscription.max,      equals(1),              reason: 'Recorded value for `max` other than expected'                                          );
        expect(handler.subscriptions, contains(subscription), reason: 'Returned subscription unexpectedly differs from the one tracked by the ProtocolHandler');

        closeEventStream(); // -- don't make the test runner guess when we're done
      });
    });

    //@depends('subscribe()')
    group('unsubscribe()', () {
      Subscription subscription;

      setUp(() {
        var expectedSubject = faker.lorem.word();
        subscription        = handler.subscribe(expectedSubject);
        reflectedSocket.collectedData.clear();
        expect(handler.subscriptions, hasLength(1), reason: 'Unexpected state after test setup');
      });

      test('immediate', () {
        // ======== Verify Events are (eventually) Fired
        Stream<NonMessageEvent> mainStream = reflectedEventStream.on<NonMessageEvent>();
        expect(mainStream, emitsInOrder([
          matchesSid<UnsubscribeEvent>(subscription),
          emitsDone
        ]), reason: 'Fired event-sequence other than expected');

        // ======== Unsubscribe
        handler.unsubscribe(subscription);

        // ======== Verify Results
        // -------- socket sends to server
        expectMessageType(MessageType.UNSUB,   reason: 'Data written to socket other than expected'          );
        expect(handler.subscriptions, isEmpty, reason: 'Subscription not removed from collection as expected');

        closeEventStream(); // -- don't make the test runner guess when we're done
      });

      //depends('can receive messages') TODO <-- implement that test
      test('later', () {
        int expectedMax = faker.randomGenerator.integer(10, min: 1);

        // ======== Verify Events are (eventually) Fired
        Stream<NonMessageEvent> mainStream = reflectedEventStream.on<NonMessageEvent>();
        expect(mainStream, emitsInOrder([
          neverEmits(isA<UnsubscribeEvent>()),
          emitsDone
        ]), reason: 'Fired event-sequence other than expected');

        // ======== Unsubscribe
        handler.unsubscribe(subscription, max: expectedMax);

        // ======== Verify Results
        // -------- socket sends to server
        expectMessageType(MessageType.UNSUB, reason: 'Data written to socket other than expected');

        // -------- subscription(s) are correct
        expect(handler.subscriptions, hasLength(1),        reason: 'Subscription was unexpectedly removed from collection early');
        expect(subscription.max,      equals(expectedMax), reason: 'Recorded value for `max` other than expected'               );

        closeEventStream(); // -- don't make the test runner guess when we're done
      });
    });

    //@depends('subscribe()')
    //@depends('unsubscribe()')
    test('registerRequest()', () {
      // ======== Verify Events are (eventually) Fired
      Stream<NonMessageEvent> mainStream = reflectedEventStream.on<NonMessageEvent>();
      expect(mainStream, emitsInOrder([
        isA<RequestEvent>(),
        neverEmits(isA<UnsubscribeEvent>()),
        emitsDone
      ]), reason: 'Fired event-sequence other than expected');

      // ======== Make Request
      var subscription = handler.registerRequest(faker.lorem.word());

      // ======== Verify Results
      // -------- socket sends to server
      expectMessageType(MessageType.SUB,   reason: 'Data written to socket other than expected');
      expectMessageType(MessageType.UNSUB, reason: 'Data written to socket other than expected');

      // -------- subscription(s) are correct
      expect(subscription,          isA<RequestSubscription>(), reason: 'returned subscription of type other than expected'                                     );
      expect(handler.subscriptions, hasLength(1),               reason: 'Subscription not added to collection as expected'                                      );
      expect(handler.subscriptions, contains(subscription),     reason: 'Returned subscription unexpectedly differs from the one tracked by the ProtocolHandler');

      closeEventStream(); // -- don't make the test runner guess when we're done
    });
  });
}
