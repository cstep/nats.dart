import 'dart:convert';
import 'dart:mirrors';
import 'package:nats_dart/nats_dart.dart';
import 'package:faker_extended/faker_extended.dart';
import 'package:test/test.dart';
import 'package:mockito/mockito.dart';
import '../mocks/ProtocolHandlerMock.dart';
import '../mocks/ServerOptionsMock.dart';
import '../mocks/factories/ProtocolHandlerFactoryMock.dart';
import '../mocks/factories/SubscriptionFactoryMock.dart';

void main() {
  container
    ..unregister<SubscriptionFactory   >()..registerInstance<SubscriptionFactory   >(SubscriptionFactoryMock()   )
    ..unregister<ProtocolHandlerFactory>()..registerInstance<ProtocolHandlerFactory>(ProtocolHandlerFactoryMock());

  Client              client;
  ProtocolHandlerMock handler;

  setUp(() {
    handler = container.resolve<ProtocolHandlerFactory>().create(null);
    client  = Client(handler, ClientOptions());
  });

  test('Inheritance validation', () {
    expect(client, isA<Events>(), reason: 'Client instance type other than expected');
  });

  test('connect() method', () async {
    var expectedHost      = faker.internet.uri(faker.randomGenerator.element(['http', 'https'])) + ':${faker.randomGenerator.integer(5000, min: 1000)}';
    var expectedUsername  = faker.internet.userName();
    var expectedPassword  = faker.internet.password();

    Client client = await Client.connect(ServerSet.from([expectedHost]), serverOptions: ServerOptionsMock(values: {
      "username": expectedUsername,
      "password": expectedPassword,
    }));

    var handlerMirror = reflect(client).getField(MirrorSystem.getSymbol('_protocolHandler', reflect(client).type.owner));

    var reflectedServerSet = handlerMirror.getField(MirrorSystem.getSymbol('servers', handlerMirror.type.owner)).reflectee;
    expect(reflectedServerSet, isA<ServerSet>()
      .having((x) => x.length,        'length', equals(1))
      .having((x) => x.currentServer, 'host',   isA<Server>().having((y) => y.url.toString(), 'url', equals(expectedHost))),
      reason: 'Generated host list other than expected');

    var reflectedHandler = handlerMirror.reflectee;
    var verification     = verify(reflectedHandler.connect(captureAny, timeout: anyNamed('timeout')));
    expect(verification.callCount,      equals(1), reason: 'ProtocolHandler.connect() called an unexpected number of times');
    expect(verification.captured.first, isA<ServerOptions>()
      .having((x) => x.username, 'username', equals(expectedUsername))
      .having((x) => x.password, 'password', equals(expectedPassword)),
      reason: 'Generated connection spec other than expected');
  });

  test('connectSingle() method', () async {
    var expectedHost      = faker.internet.uri(faker.randomGenerator.element(['http', 'https'])) + ':${faker.randomGenerator.integer(5000, min: 1000)}';
    var expectedUsername  = faker.internet.userName();
    var expectedPassword  = faker.internet.password();

    Client client = await Client.connectSingle(expectedHost, serverOptions: ServerOptionsMock(values: {
      "username": expectedUsername,
      "password": expectedPassword,
    }));

    var handlerMirror = reflect(client).getField(MirrorSystem.getSymbol('_protocolHandler', reflect(client).type.owner));

    var reflectedServerSet = handlerMirror.getField(MirrorSystem.getSymbol('servers', handlerMirror.type.owner)).reflectee;
    expect(reflectedServerSet, isA<ServerSet>()
      .having((x) => x.length,        'length', equals(1))
      .having((x) => x.currentServer, 'host',   isA<Server>().having((y) => y.url.toString(), 'url', equals(expectedHost))),
      reason: 'Generated host list other than expected');

    var reflectedHandler = handlerMirror.reflectee;
    var verification     = verify(reflectedHandler.connect(captureAny, timeout: anyNamed('timeout')));
    expect(verification.callCount,      equals(1), reason: 'ProtocolHandler.connect() called an unexpected number of times');
    expect(verification.captured.first, isA<ServerOptions>()
      .having((x) => x.username, 'username', equals(expectedUsername))
      .having((x) => x.password, 'password', equals(expectedPassword)),
      reason: 'Generated connection spec other than expected');
  });

  group('publish() method', () {
    test('with blank subject', () {
      expect(
        () => client.publish(""),
        throwsA(NatsError.BAD_SUBJECT),
        reason: "Blank subject should throw a Nats.Error.BAD_SUBJECT exception, but did not"
      );
    });

    test('with valid subject', () {
      var expectedCallCount = 1;
      var expectedSubject   = faker.lorem.word();

      client.publish(expectedSubject);

      var verification = verify(handler.publish(captureAny, any));
      expect(verification.callCount,      equals(expectedCallCount), reason: 'ProtocolHandler.publish() called an unexpected number of times'    );
      expect(verification.captured.first, equals(expectedSubject),   reason: 'ProtocolHandler.publish() called with unexpected value for subject');
    });

    test('with optional arguments', () {
      var expectedPayload   = faker.lorem.word();
      var expectedReplySubject = faker.lorem.word();
      var expectedEncoding  = faker.randomGenerator.element([ascii, utf8, latin1]);

      client.publish(faker.lorem.word(), payload: expectedPayload, replySubject: expectedReplySubject, encoding: expectedEncoding);

      var verification = verify(handler.publish(any, captureAny, replySubject: captureAnyNamed('replySubject'), encoding: captureAnyNamed('encoding')));
      expect(verification.callCount,   equals(1),                    reason: 'ProtocolHandler.publish() called an unexpected number of times'          );
      expect(verification.captured,    hasLength(3),                 reason: 'ProtocolHandler.publish() called with unexpected number of arguments'    );
      expect(verification.captured[0], equals(expectedPayload),      reason: 'ProtocolHandler.publish() called with unexpected value for payload'      );
      expect(verification.captured[1], equals(expectedReplySubject), reason: 'ProtocolHandler.publish() called with unexpected value for reply-subject');
      expect(verification.captured[2], equals(expectedEncoding),     reason: 'ProtocolHandler.publish() called with unexpected value for encoding'     );
    });
  });

  group('subscribe() method', () {
    test('with blank subject', () {
      expect(
        () => client.subscribe(""),
        throwsA(NatsError.BAD_SUBJECT),
        reason: "Null subject should throw a NatsError.BAD_SUBJECT exception, but did not"
      );
    });

    test('with valid subject', () {
      var expectedSubject = faker.lorem.word();
      var actual          = client.subscribe(expectedSubject);

      expect(actual,         isA<Subscription>(),     reason: 'Client.subscribe should return a subscription, but did not');
      expect(actual.subject, equals(expectedSubject), reason: 'Returned subscription subject other than expected'         );
    });

    test('with queue name', () {
      var expectedQueueName = faker.lorem.word();
      var actual          = client.subscribe(faker.lorem.word(), queue: expectedQueueName);

      expect(actual,       isA<Subscription>(),       reason: 'Client.subscribe should return a subscription, but did not');
      expect(actual.queue, equals(expectedQueueName), reason: 'Returned subscription queue name other than expected'      );
    });

    test('with max/limit', () {
      var expectedMax = faker.randomGenerator.integer(10, min: 1);
      var actual      = client.subscribe(faker.lorem.word(), max: expectedMax);

      expect(actual,     isA<Subscription>(), reason: 'Client.subscribe should return a subscription, but did not');
      expect(actual.max, equals(expectedMax), reason: 'Returned subscription max/limit other than expected'       );
    });

    test('with handler', () async {
      var expectedMessage = faker.lorem.sentence();

      var messageHandler = HandlerMock((message, {error}) => null);

      var waitForIt = untilCalled(messageHandler.call(any, error: anyNamed('error')))
        .timeout(Duration(seconds: 1), onTimeout: () => fail('Custom message handler unexpectedly never called'))
        .then((_) {
          verify(messageHandler.call(any, error: anyNamed('error'))).called(1);
        });

      Subscription subscription = client.subscribe(faker.lorem.word(), handler: messageHandler);
      handler.fakeIncoming(DataMessage(subscription.subject, subscription.sid, ascii.encode(expectedMessage)));

      return waitForIt;
    });
  });

  test('sendPing() method', () {
    client.sendPing();
    verify(handler.sendPing()).called(1);
  });

  group('request() method', () {
    test('with blank subject', () {
      expect(
        () => client.request(""),
        throwsA(NatsError.BAD_SUBJECT),
        reason: "Null subject should throw a NatsError.BAD_SUBJECT exception, but did not"
      );
    });

    test('with valid subject', () {
      Future doTest () {
        var expectedSubject = faker.lorem.word();
        var actual          = client.request(expectedSubject, timeout: Duration(milliseconds: 1000)); // -- pass our own, short timeout, so the test isn't wasting time waiting

        return expectLater(actual, isA<Future<DataMessage>>(), reason: 'Returned value other than expected')
          .then((_) {
            var verification = verify(handler.registerRequest(captureAny));
            expect(verification.callCount,      equals(1),               reason: 'ProtocolHandler.registerRequest() called an unexpected number of times'    );
            expect(verification.captured.first, equals(expectedSubject), reason: 'ProtocolHandler.registerRequest() called with unexpected value for subject');
          })
          .then((_) => actual);
      }

      expect(doTest(), throwsA(NatsError.REQUEST_TIMEOUT), reason: "Expected TIMEOUT error not thrown"); // this will happen since nothing is actually spoofing a response to our request
    });

    test('with custom onTimeout handler', () {
      var timeoutHandler = expectAsync1((_) => null, count: 1, reason: 'Custom timeout handler not called as expected');

      client.request(faker.lorem.word(), onTimeout: timeoutHandler, timeout: Duration()); // -- pass our own, short timeout, so the test isn't wasting time waiting
    });
  });
}

// expectAsync1() returns a wrapper of type `function(List(argument1))` which doesn't match our required signature of `function(argument1)`
// so we do this crap so that, instead of `expectAsync1(..., count: 1)` (which operates on a method), we can use `verify(...).called(1)` (which operates on a class);
abstract class HandlerFake extends Fake {
  void call(DataMessage message, {NatsError error});
}

class HandlerMock extends Mock implements HandlerFake {
  final MessageHandler callback;

  HandlerMock(this.callback) {
    when(call(any, error: anyNamed('error'))).thenAnswer((i) => callback(i.positionalArguments.first, error: i.namedArguments['error']));
  }
}
