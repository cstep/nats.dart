import 'package:nats_dart/nats_dart.dart';
import 'package:faker_extended/faker_extended.dart';
import 'package:test/test.dart';
import 'package:mockito/mockito.dart';

void main() {
  test('inheritance validation', () {
    var instance = SubscriptionList();

    expect(instance, isA<List<Subscription>>());
    expect(instance, isA<EventEmitter>()      );
  });

  group('Additions', () {
    SubscriptionList subscriptions;

    setUp(() {
      subscriptions = SubscriptionList();
    });

    test('via `add()`', () {
//      var eventHandler = expectAsync1((SubscriptionEvent event) => null, count: 1, reason: "SubscribeEvent not fired as expected");
//      var listener     = globalEventBus.on<SubscribeEvent>().listen(eventHandler);

      subscriptions.add(SubscriptionMock());

//      expectLater(subscriptions.length, equals(1), reason: "Subscription not added to collection as expected")
//        .then((_) {
//          listener.cancel();
//      });

      expect(subscriptions.length, equals(1), reason: 'subscription not added as expected');
    });

    test('via `[]=` operator', () {
      try {
        subscriptions[1] = SubscriptionMock();
        fail("Expected error not thrown");
      } on TestFailure {
        rethrow;
      } catch(e) {
        expect(e, isUnsupportedError, reason: "Thrown error other than expected");
      }

      expect(subscriptions, isEmpty, reason: "Length unexpectedly changed after (expected) failed element addition");
    });
  });

  // @depends('Additions')
  group('Removal', () {
    test('via `unsubscribe()`', () {
      var subscription = SubscriptionMock();
      var collection   = SubscriptionList()
        ..add(subscription)
        ..add(SubscriptionMock());

      collection.unsubscribe(subscription);
      expect(collection.length, equals(1), reason: 'subscription not removed as expected');
    });

    test('via `unsubscribeAll()`', () {
      var collection   = SubscriptionList()
        ..add(SubscriptionMock())
        ..add(SubscriptionMock());

      collection.unsubscribeAll();
      expect(collection, isEmpty, reason: 'Not all subscriptions removed as expected');
    });

    test('via `remove()`', () {
      var subscription = SubscriptionMock();
      var collection   = SubscriptionList()
        ..add(subscription)
        ..add(SubscriptionMock());

      collection.remove(subscription);
      expect(collection.length, equals(1), reason: 'subscription not removed as expected');
    });

    test('Cross-contamination', () {
      var subscriptionA = SubscriptionMock();
      var subscriptionB = SubscriptionMock();

      var collection1 = SubscriptionList()..add(subscriptionA);
      var collection2 = SubscriptionList()..add(subscriptionB); // ignore: unused_local_variable

      expect(subscriptionA.sid,      isNotNull,                             reason: 'subscriptionA unexpectedly missing an sid'                      );
      expect(subscriptionA.sid,      equals(subscriptionB.sid),             reason: 'subscriptionA and subscriptionB unexpectedly mismatched sids'   );
      expect(subscriptionA.hashCode, isNot(equals(subscriptionB.hashCode)), reason: 'subscriptionA and subscriptionB unexpectedly matching hashCodes');

      collection1.unsubscribe(subscriptionB); // -- cancel a subscription from a different collection (and also NOT in this collection)
      expect(collection1.length, equals(1), reason: 'non-member subscription unexpectedly removed from non-owner collection');
    });
  });
}

class SubscriptionMock extends Mock implements Subscription {
  static final String mySid    = faker.guid.guid();
  @override final int hashCode = faker.lorem.word().hashCode; // ignore: hash_and_equals

  SubscriptionMock() {
    when(this.sid).thenReturn(mySid);
  }

}
