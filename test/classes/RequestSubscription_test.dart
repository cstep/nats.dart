import 'dart:mirrors';

import 'package:nats_dart/nats_dart.dart';
import 'package:test/test.dart';

//@depends(Subscription_test.dart)
void main() {
  test('inheritance validation', () {
    // -- by using reflection, we don't need to fake/mock/stub any dependencies/injections to pass to a constructor and instantiate
    // -- and since we're already using it below, there's no extra overhead
    expect(RequestSubscription, predicate((Type type) => reflectType(type).isSubtypeOf(reflectType(Subscription)), 'a subtype of <Subscription>'));
  });

  test('doesn\'t allow any of parent class\'s optional params', () {
    var mainConstructor = reflectClass(RequestSubscription).declarations.values
      .whereType<MethodMirror>()
      .where((MethodMirror method     ) => method.isConstructor)
      .where((MethodMirror constructor) => constructor.constructorName == Symbol(""))
      .single;

    expect(mainConstructor.parameters, hasLength(2), reason: 'Number of constructor parameters other than expected');
    expect(mainConstructor.parameters, contains(predicate((ParameterMirror param) => param.type.reflectedType == ProtocolHandler, "an argument of type ProtocolHandler"    )));
    expect(mainConstructor.parameters, contains(predicate((ParameterMirror param) => param.type.reflectedType == String,          "a subject/inbox argument of type String")));
    // 👍 we've checked both possible params, no others and no optionals can exist ✔
  });
}
