import 'dart:async';
import 'dart:convert';
import 'dart:mirrors';
import 'dart:typed_data';
import 'package:nats_dart/nats_dart.dart';
import 'package:faker_extended/faker_extended.dart';
import 'package:test/test.dart';
import '../mocks/MessageMocks.dart';
import '../mocks/MessageProtoMocks.dart';
import '../mocks/factories/MessageFactoryMock.dart';
import '../mocks/factories/MessageProtoFactoryMock.dart';


void main() {
  container
    ..unregister<MessageProtoFactory>()..registerInstance<MessageProtoFactory>(MessageProtoFactoryMock())
    ..unregister<MessageFactory>()     ..registerInstance<MessageFactory     >(MessageFactoryMock());

  group('can be created', () {
    test('with required args only', () {
      var actual = ProtocolParser();

      expect(actual,          isA<ProtocolParser>(), reason: 'ProtocolParser instance type other than expected'   );
      expect(actual.encoding, equals(ascii),         reason: 'ProtocolParser default encoding other than expected');
    });

    group('with optional args too', () {
      test('encoding', () {
        var expectedEncoding = utf8;
        var actual           = ProtocolParser(expectedEncoding);

        expect(actual,          isA<ProtocolParser>(),    reason: 'ProtocolParser instance type other than expected');
        expect(actual.encoding, equals(expectedEncoding), reason: 'ProtocolParser encoding other than expected'     );
      });
    });
  });

  group('can parse messages', () {
    ProtocolParser parser;
    Symbol         parseMethod;

    List<Message> tryParse(String messageData) {
      return reflect(parser).invoke(parseMethod, [parser.encoding.encode(messageData)]).reflectee;
    }

    setUp(() {
      parser      = ProtocolParser();
      parseMethod = MirrorSystem.getSymbol('_parseMessages', reflectClass(ProtocolParser).owner);
    });

    group('single messages', () {
      test('INFO', () {
        var expectedInfo = {
          "server_id"    : faker.lorem.word(),
          "version"      : faker.lorem.word(),
          "go"           : faker.lorem.word(),
          "host"         : faker.lorem.word(),
          "port"         : faker.randomGenerator.integer(100),
          "max_payload"  : faker.randomGenerator.integer(100),
          "client_id"    : faker.randomGenerator.integer(100),
          "proto"        : faker.randomGenerator.integer(1),
          "auth_required": faker.randomGenerator.boolean(),
          "tls_required" : faker.randomGenerator.boolean(),
          "tls_verify"   : faker.randomGenerator.boolean(),
          "connect_urls" : faker.lorem.words(faker.randomGenerator.integer(10))
        };
        String protoString = 'INFO ${json.encode(expectedInfo)}\r\n';

        var result = tryParse(protoString);
        expect(result,        isA<List<Message>>(), reason: "Parsed messages are of type other than expected");
        expect(result,        hasLength(1),         reason: "Parsed messages count other than expected"      );
        expect(result.single, isA<InfoMessage>(),   reason: "Parsed message is of type other than expected"  );

        InfoMessageProtoMock actual = result.single.proto;
        expectedInfo.keys.forEach((String key) {
          var mockKey = key.replaceAllMapped(RegExp(r'_+(\w)'), (match) => match.group(1).toUpperCase()); // -- our mocks use camelCase instead of snake_case like the parser is expecting
          mockKey = (mockKey == 'go'    ? 'goVersion' : mockKey); // -- our mocks use slightly different keys
          mockKey = (mockKey == 'proto' ? 'protocol'  : mockKey); // -- our mocks use slightly different keys

          expect(actual.values, containsPair(mockKey, expectedInfo[key]), reason: "Parsed value for `$key` other than expected");
        });
      });

      group('MSG', () {
        ExpectedDataMessage expected;

        setUp(() {
          expected = ExpectedDataMessage()
            ..subject      = faker.lorem.word()
            ..sid          = faker.lorem.word()
            ..payload      = faker.lorem.sentences(faker.randomGenerator.integer(10)).join(' ');
        });

        test('without payload', () {
          // ======== Prepare Message
          expected.payload = null;

          // ======== Submit Message
          var protoString = 'MSG ${expected.subject} ${expected.sid} ${expected.byteLength}\r\n${expected.payload}\r\n';
          var result      = tryParse(protoString);

          // ======== Verify Results
          expect(result, isA<List<Message>>(),             reason: "Parsed messages are of type other than expected");
          expect(result, hasLength(1),                     reason: "Parsed messages count other than expected"      );
          expect(result, everyElement(isA<DataMessage>()), reason: "Parsed message is of type other than expected"  );

          verifyDataMessage(result.single, expected);
        });

        test('without replySubject', () {
          // ======== Submit Message
          var protoString = 'MSG ${expected.subject} ${expected.sid} ${expected.byteLength}\r\n${expected.payload}\r\n';
          var result      = tryParse(protoString);

          // ======== Verify Results
          expect(result, isA<List<Message>>(),             reason: "Parsed messages are of type other than expected");
          expect(result, hasLength(1),                     reason: "Parsed messages count other than expected"      );
          expect(result, everyElement(isA<DataMessage>()), reason: "Parsed message is of type other than expected"  );

          verifyDataMessage(result.single, expected);
        });

        test('with replySubject', () {
          // ======== Prepare Message
          expected.replySubject = faker.lorem.word();

          // ======== Submit Message
          var protoString = 'MSG ${expected.subject} ${expected.sid} ${expected.replySubject} ${expected.byteLength}\r\n${expected.payload}\r\n';
          var result      = tryParse(protoString);

          // ======== Verify Results
          expect(result, isA<List<Message>>(),             reason: "Parsed messages are of type other than expected");
          expect(result, hasLength(1),                     reason: "Parsed messages count other than expected"      );
          expect(result, everyElement(isA<DataMessage>()), reason: "Parsed message is of type other than expected"  );

          verifyDataMessage(result.single, expected);
        });

        test('multi-line messages', () { // -- messages that include \r\n inside payload
          // ======== Prepare Message
          expected.payload = expected.payload + '\r\n' + expected.payload;

          // ======== Submit Message
          var protoString = 'MSG ${expected.subject} ${expected.sid} ${expected.byteLength}\r\n${expected.payload}\r\n';
          var result      = tryParse(protoString);

          // ======== Verify Results
          expect(result, isA<List<Message>>(),             reason: "Parsed messages are of type other than expected");
          expect(result, hasLength(1),                     reason: "Parsed messages count other than expected"      );
          expect(result, everyElement(isA<DataMessage>()), reason: "Parsed message is of type other than expected"  );

          verifyDataMessage(result.single, expected);
        });
      });

      test('PING', () {
        var protoString = 'PING\r\n';

        var result = tryParse(protoString);
        expect(result,        isA<List<Message>>(), reason: "Parsed messages are of type other than expected");
        expect(result,        hasLength(1),         reason: "Parsed messages count other than expected"      );
        expect(result.single, isA<PingMessage>(),   reason: "Parsed message is of type other than expected"  );
      });

      test('PONG', () {
        var protoString = 'PONG\r\n';

        var result = tryParse(protoString);
        expect(result,        isA<List<Message>>(), reason: "Parsed messages are of type other than expected");
        expect(result,        hasLength(1),         reason: "Parsed messages count other than expected"      );
        expect(result.single, isA<PongMessage>(),   reason: "Parsed message is of type other than expected"  );
      });

      test('+OK', () {
        var protoString = '+OK\r\n';

        var result = tryParse(protoString);
        expect(result,        isA<List<Message>>(), reason: "Parsed messages are of type other than expected");
        expect(result,        hasLength(1),         reason: "Parsed messages count other than expected"      );
        expect(result.single, isA<OkMessage>(),     reason: "Parsed message is of type other than expected"  );
      });

      test('-ERR', () {
        var expectedMessage = faker.lorem.sentence();
        var protoString     = '-ERR $expectedMessage\r\n';

        var result = tryParse(protoString);
        expect(result,        isA<List<Message>>(), reason: "Parsed messages are of type other than expected");
        expect(result,        hasLength(1),         reason: "Parsed messages count other than expected"      );
        expect(result.single, isA<ErrorMessage>(),  reason: "Parsed message is of type other than expected"  );

        ErrorMessageProto actual = result.single.proto;
        expect(actual.message, equals(expectedMessage), reason: "Parsed error message other than expected");
      });
    });

    //@depends('single messages');
    group('multiple messages', () {
      test('simple messages', () {
        var expectedMessageCount = faker.randomGenerator.integer(10, min: 2);
        var protoList            = <String>[];
        var expectedMessages     = <String>[];

        // ======== Generate Messages
        for(var i = 0; i < expectedMessageCount; i++) {
          expectedMessages.add(faker.lorem.sentence());
          protoList.add('-ERR ${expectedMessages[i]}\r\n');
        }

        // ======== Submit Messages
        var result = tryParse(protoList.join(''));

        // ======== Verify Results
        expect(result, isA<List<Message>>(),              reason: "Parsed messages are of type other than expected");
        expect(result, hasLength(expectedMessageCount),   reason: "Parsed messages count other than expected"      );
        expect(result, everyElement(isA<ErrorMessage>()), reason: "Parsed messages are of type other than expected");

        for(var i = 0; i < result.length; i++) {  // -- loop through each resultant message
          ErrorMessageMock actual = result[i];
          expect(actual.message, equals(expectedMessages[i]), reason: "Message[$i]: Parsed error message other than expected");
        }
      });

      test('complex messages', () { // -- multi-part messages span more than just a single proto line
        var expectedMessageCount = faker.randomGenerator.integer(10, min: 2);
        var protoList            = <String             >[];
        var expectedMessages     = <ExpectedDataMessage>[];

        // ======== Generate Messages
        for(var i = 0; i < expectedMessageCount; i++) {
          var expectedMessage = ExpectedDataMessage()
            ..subject    = faker.lorem.word()
            ..sid        = faker.lorem.word()
            ..payload    = faker.lorem.sentences(faker.randomGenerator.integer(10)).join('\r\n');

          expectedMessages.add(expectedMessage);
          protoList.add('MSG ${expectedMessage.subject} ${expectedMessage.sid} ${expectedMessage.byteLength}\r\n${expectedMessage.payload}\r\n');
        }

        // ======== Submit Messages
        var result = tryParse(protoList.join(''));

        // ======== Verify Results
        expect(result, isA<List<Message>>(),             reason: "Parsed messages are of type other than expected");
        expect(result, hasLength(expectedMessageCount),  reason: "Parsed messages count other than expected"      );
        expect(result, everyElement(isA<DataMessage>()), reason: "Parsed messages are of type other than expected");

        for(var i = 0; i < result.length; i++) { // -- loop through each resultant message
          verifyDataMessage(result[i], expectedMessages[i], reasonPrefix: 'Message[$i]');
        }
      });

      test('mixed messages', () {
        const int SIMPLE  = 0;
        const int COMPLEX = 1;

        var expectedMessages = <dynamic>[null, null];
        var protoList        = <String>[null, null];

        // ======== Generate Messages
        // -------- simple message
        expectedMessages[SIMPLE] = faker.lorem.sentence();
        protoList[SIMPLE]        = '-ERR ${expectedMessages[SIMPLE]}\r\n';

        // -------- complex message
        var expectedMessage = ExpectedDataMessage()
          ..subject = faker.lorem.word()
          ..sid     = faker.lorem.word()
          ..payload = faker.lorem.sentences(faker.randomGenerator.integer(10)).join('\r\n');

        expectedMessages[COMPLEX] = expectedMessage;
        protoList[COMPLEX]        = 'MSG ${expectedMessage.subject} ${expectedMessage.sid} ${expectedMessage.byteLength}\r\n${expectedMessage.payload}\r\n';

        // ======== Submit Messages
        var result = tryParse(protoList.join(''));

        // ======== Verify Results
        expect(result, isA<List<Message>>(),          reason: "Parsed messages are of type other than expected"           );
        expect(result, hasLength(2),                  reason: "Parsed messages count other than expected"                 );
        expect(result, contains(isA<ErrorMessage>()), reason: "Parsed messages do not include an ErrorMessage as expected");
        expect(result, contains(isA<DataMessage>()),  reason: "Parsed messages do not include a DataMessage as expected"  );

        // -------- verify simple message
        ErrorMessageMock actualSimple = result.whereType<ErrorMessage>().first;
        expect(actualSimple.message, equals(expectedMessages[SIMPLE]), reason: "SIMPLE Message: Parsed message other than expected");

        // -------- verify complex message
        verifyDataMessage(result.whereType<DataMessage>().first, expectedMessages[COMPLEX], reasonPrefix: 'COMPLEX Message');
      });
    });
  });

  group('can be used as a stream transformer', () {
    test('Inheritance validation', () {
      expect(ProtocolParser(), isA<StreamTransformer<Uint8List, Message>>());
    });

    //@depends('can parse messages')
    test('transforms raw protocol messages into Message instances', () {
      void onDataHandler (Message message) {
        expect(message, isA<PingMessage>(), reason: 'Transformed message type other than expected');
      }

      // ======== Set Up Our Stream(s)
      var source      = StreamController<Uint8List>.broadcast();
      var transformed = source.stream.transform(ProtocolParser());

      // ======== Verify Our Transformation
      var listener = transformed.listen(expectAsync1(onDataHandler, count: 1, reason: 'No messages came out of the transformed stream'));

      // ======== Submit a RAW Message
      source.add(ascii.encode('PING\r\n'));
      source.close();

      return listener.asFuture() // -- return as a future so the test runner will wait for the stream actually finish before declaring success
        ..then((_) => listener.cancel());
    });

    test('handles transforms raw protocol messages delivered in separate chunks', () {
      void onDataHandler (Message message) {
        expect(message,                          isA<DataMessage>(), reason: 'Transformed message type other than expected'                      );
        expect((message as DataMessage).payload, isNotEmpty,         reason: 'Split portion of message not included in transfomation as expected');
      }

      // ======== Set Up Our Stream(s)
      var source      = StreamController<Uint8List>.broadcast();
      var transformed = source.stream.transform(ProtocolParser());

      // ======== Verify Our Transformation
      var listener = transformed.listen(expectAsync1(onDataHandler, count: 1, reason: 'No messages came out of the transformed stream'));

      // ======== Submit a RAW Message
      var payload = faker.lorem.sentences(faker.randomGenerator.integer(10)).join(' ');
      source.add(ascii.encode('MSG ${faker.lorem.word()} ${faker.lorem.word()} ${payload.length}\r\n')); // -- send the payload in a separate chunk
      source.add(ascii.encode('${payload}\r\n'));
      source.close();

      return listener.asFuture() // -- return as a future so the test runner will wait for the stream actually finish before declaring success
        ..then((_) => listener.cancel());
    }, skip: 'Not supported, yet. TODO');
  });
}

void verifyDataMessage(DataMessageMock actual, ExpectedDataMessage expectedMessage, {String reasonPrefix}) {
  if ((reasonPrefix??'').isNotEmpty) {
    reasonPrefix += ': ';
  }

  expect(actual.proto.values,                    containsPair('subject',      expectedMessage.subject     ), reason: "${reasonPrefix}Parsed value for `subject` other than expected"     );
  expect(actual.proto.values,                    containsPair('sid',          expectedMessage.sid         ), reason: "${reasonPrefix}Parsed value for `sid` other than expected"         );
  expect(actual.proto.values,                    containsPair('byteLength',   expectedMessage.byteLength  ), reason: "${reasonPrefix}Parsed value for `byteLength` other than expected"  );
  expect(actual.encoding.decode(actual.payload), equals(expectedMessage.payload),                            reason: "${reasonPrefix}Parsed payload other than expected"                 );

  if ((expectedMessage.replySubject??'') != null) {
    expect(actual.proto.values, containsPair('replySubject', expectedMessage.replySubject), reason: "${reasonPrefix}Parsed value for `replySubject` other than expected");
  }
}

class ExpectedDataMessage {
  String subject;
  String sid;
  String replySubject;
  int    byteLength;

  // -- handling payload this way allows us to auto-calculate the value for [byteLength], but also allows us to override [byteLength]'s value if needed
  String _payload;
  String get payload => _payload;
  void   set payload(String value) {
    value ??= '';

    _payload   = value;
    byteLength = value.length;
  }
}
