import 'dart:convert';

import 'package:nats_dart/nats_dart.dart';
import 'package:test/test.dart';
import 'package:faker_extended/faker_extended.dart';

void main() {
  String      expectedName;
  PayloadType expectedPayloadType;
  Encoding    expectedEncoding;
  int         expectedProtocol;
  bool        expectedVerbose;
  bool        expectedPedantic;
  bool        expectedTlsRequired;
  bool        expectedEcho;
  String      expectedAuthToken;
  String      expectedUsername;
  String      expectedPassword;

  setUp(() {
    expectedName        = faker.lorem.word();
    expectedPayloadType = faker.randomGenerator.element(PayloadType.values);
    expectedEncoding    = faker.randomGenerator.element([latin1, ascii, utf8]);
    expectedProtocol    = faker.randomGenerator.integer(1);
    expectedVerbose     = faker.randomGenerator.boolean();
    expectedPedantic    = faker.randomGenerator.boolean();
    expectedTlsRequired = faker.randomGenerator.boolean();
    expectedEcho        = faker.randomGenerator.boolean();
    expectedAuthToken   = faker.guid.guid();
    expectedUsername    = faker.internet.userName();
    expectedPassword    = faker.internet.password();
  });

  group('constructors', () {
    void _checkResult(ServerOptions actual, bool useToken) {
      expect(actual.name,        equals(expectedName       ), reason: 'Unexpectedly failed to initialize option `name`'       );
      expect(actual.payloadType, equals(expectedPayloadType), reason: 'Unexpectedly failed to initialize option `payloadType`');
      expect(actual.encoding,    equals(expectedEncoding   ), reason: 'Unexpectedly failed to initialize option `encoding`'   );
      expect(actual.protocol,    equals(expectedProtocol   ), reason: 'Unexpectedly failed to initialize option `protocol`'   );
      expect(actual.verbose,     equals(expectedVerbose    ), reason: 'Unexpectedly failed to initialize option `verbose`'    );
      expect(actual.pedantic,    equals(expectedPedantic   ), reason: 'Unexpectedly failed to initialize option `pedantic`'   );
      expect(actual.tlsRequired, equals(expectedTlsRequired), reason: 'Unexpectedly failed to initialize option `tlsRequired`');
      expect(actual.echo,        equals(expectedEcho       ), reason: 'Unexpectedly failed to initialize option `echo`'       );

      if (useToken) {
        expect(actual.authToken,   equals(expectedAuthToken), reason: 'Unexpectedly failed to initialize option `authToken`'  );
      } else {
        expect(actual.username,    equals(expectedUsername ), reason: 'Unexpectedly failed to initialize option `username`'   );
        expect(actual.password,    equals(expectedPassword ), reason: 'Unexpectedly failed to initialize option `password`'   );
      }
    }

    group('named args', () {
      test('with username|password', () {
        var actual = ServerOptions(
          name       : expectedName,
          payloadType: expectedPayloadType,
          encoding   : expectedEncoding,
          protocol   : expectedProtocol,
          verbose    : expectedVerbose,
          pedantic   : expectedPedantic,
          tlsRequired: expectedTlsRequired,
          echo       : expectedEcho,
          username   : expectedUsername,
          password   : expectedPassword
        );

        _checkResult(actual, false);
      });

      test('with token', () {
        var actual = ServerOptions(
          name       : expectedName,
          payloadType: expectedPayloadType,
          encoding   : expectedEncoding,
          protocol   : expectedProtocol,
          verbose    : expectedVerbose,
          pedantic   : expectedPedantic,
          tlsRequired: expectedTlsRequired,
          echo       : expectedEcho,
          authToken  : expectedAuthToken,
        );

        _checkResult(actual, true);
      });

      test('with both', () {
        expect(() => ServerOptions(
          name       : expectedName,
          payloadType: expectedPayloadType,
          encoding   : expectedEncoding,
          protocol   : expectedProtocol,
          verbose    : expectedVerbose,
          pedantic   : expectedPedantic,
          tlsRequired: expectedTlsRequired,
          echo       : expectedEcho,
          authToken  : expectedAuthToken,
          username   : expectedUsername,
          password   : expectedPassword
        ), throwsA(NatsError.BAD_AUTHENTICATION), reason: 'Expected error not thrown');
      });
    });

    // @depends('named args')
    group('from other', () {
      test('with username|password', () {
        var other = ServerOptions(
          name       : expectedName,
          payloadType: expectedPayloadType,
          encoding   : expectedEncoding,
          protocol   : expectedProtocol,
          verbose    : expectedVerbose,
          pedantic   : expectedPedantic,
          tlsRequired: expectedTlsRequired,
          echo       : expectedEcho,
          username   : expectedUsername,
          password   : expectedPassword
        );

        _checkResult(ServerOptions.from(other), false);
      });

      test('with token', () {
        var other = ServerOptions(
          name       : expectedName,
          payloadType: expectedPayloadType,
          encoding   : expectedEncoding,
          protocol   : expectedProtocol,
          verbose    : expectedVerbose,
          pedantic   : expectedPedantic,
          tlsRequired: expectedTlsRequired,
          echo       : expectedEcho,
          authToken  : expectedAuthToken,
        );

        _checkResult(ServerOptions.from(other), true);
      });
    });
  });

  group('field accessibility', () {
    test('modifiable by default', () {
      var actual = ServerOptions()
        ..name        = expectedName
        ..payloadType = expectedPayloadType
        ..encoding    = expectedEncoding
        ..protocol    = expectedProtocol
        ..verbose     = expectedVerbose
        ..pedantic    = expectedPedantic
        ..tlsRequired = expectedTlsRequired
        ..echo        = expectedEcho
        ..authToken   = expectedAuthToken
        ..username    = expectedUsername
        ..password    = expectedPassword;

      expect(actual.name,        equals(expectedName       ), reason: 'Unexpectedly failed to initialize option `name`'       );
      expect(actual.payloadType, equals(expectedPayloadType), reason: 'Unexpectedly failed to initialize option `payloadType`');
      expect(actual.encoding,    equals(expectedEncoding   ), reason: 'Unexpectedly failed to initialize option `encoding`'   );
      expect(actual.protocol,    equals(expectedProtocol   ), reason: 'Unexpectedly failed to initialize option `protocol`'   );
      expect(actual.verbose,     equals(expectedVerbose    ), reason: 'Unexpectedly failed to initialize option `verbose`'    );
      expect(actual.pedantic,    equals(expectedPedantic   ), reason: 'Unexpectedly failed to initialize option `pedantic`'   );
      expect(actual.tlsRequired, equals(expectedTlsRequired), reason: 'Unexpectedly failed to initialize option `tlsRequired`');
      expect(actual.echo,        equals(expectedEcho       ), reason: 'Unexpectedly failed to initialize option `echo`'       );
      expect(actual.authToken,   equals(expectedAuthToken  ), reason: 'Unexpectedly failed to initialize option `authToken`'  );
      expect(actual.username,    equals(expectedUsername   ), reason: 'Unexpectedly failed to initialize option `username`'   );
      expect(actual.password,    equals(expectedPassword   ), reason: 'Unexpectedly failed to initialize option `password`'   );
    });

    // @depends('modifiable by default')
    test('unmodifiable after finalization', () {
      var actual = ServerOptions()
        ..name        = faker.lorem.word()
        ..payloadType = faker.randomGenerator.element(PayloadType.values)
        ..encoding    = faker.randomGenerator.element([latin1, ascii, utf8])
        ..protocol    = faker.randomGenerator.integer(1)
        ..verbose     = faker.randomGenerator.boolean()
        ..pedantic    = faker.randomGenerator.boolean()
        ..tlsRequired = faker.randomGenerator.boolean()
        ..echo        = faker.randomGenerator.boolean()
        ..authToken   = faker.guid.guid()
        ..username    = faker.internet.userName()
        ..password    = faker.internet.password();

      actual.finalize();

      expect(() { actual.name        = null; }, throwsA(isStateError), reason: 'Expected error not thrown when modifying option `name`'       );
      expect(() { actual.payloadType = null; }, throwsA(isStateError), reason: 'Expected error not thrown when modifying option `payloadType`');
      expect(() { actual.encoding    = null; }, throwsA(isStateError), reason: 'Expected error not thrown when modifying option `encoding`'   );
      expect(() { actual.protocol    = null; }, throwsA(isStateError), reason: 'Expected error not thrown when modifying option `protocol`'   );
      expect(() { actual.verbose     = null; }, throwsA(isStateError), reason: 'Expected error not thrown when modifying option `verbose`'    );
      expect(() { actual.pedantic    = null; }, throwsA(isStateError), reason: 'Expected error not thrown when modifying option `pedantic`'   );
      expect(() { actual.tlsRequired = null; }, throwsA(isStateError), reason: 'Expected error not thrown when modifying option `tlsRequired`');
      expect(() { actual.echo        = null; }, throwsA(isStateError), reason: 'Expected error not thrown when modifying option `echo`'       );
      expect(() { actual.authToken   = null; }, throwsA(isStateError), reason: 'Expected error not thrown when modifying option `authToken`'  );
      expect(() { actual.username    = null; }, throwsA(isStateError), reason: 'Expected error not thrown when modifying option `username`'   );
      expect(() { actual.password    = null; }, throwsA(isStateError), reason: 'Expected error not thrown when modifying option `password`'   );
    });
  });

  // @depends('modifiable by default')
  group('JSON Serializable', () {
    ServerOptions options;

    setUp(() {
      options = ServerOptions()
        ..payloadType = expectedPayloadType
        ..encoding    = expectedEncoding
        ..protocol    = expectedProtocol
        ..verbose     = expectedVerbose
        ..pedantic    = expectedPedantic
        ..tlsRequired = expectedTlsRequired
        ..echo        = expectedEcho;
    });

    test('optional properties omitted', () {
      var actual = options.toMap();

      expect(actual.containsKey('lang'),       isTrue,                      reason: 'JSON-serialized key `lang` unexpectedly missing'            );
      expect(actual.containsKey('version'),    isTrue,                      reason: 'JSON-serialized key `version` unexpectedly missing'         );
      expect(actual['payloadType'],            equals(options.payloadType), reason: 'JSON-serialized value for `payloadType` other than expected');
      expect(actual['encoding'],               equals(options.encoding   ), reason: 'JSON-serialized value for `encoding` other than expected'   );
      expect(actual['protocol'],               equals(options.protocol   ), reason: 'JSON-serialized value for `protocol` other than expected'   );
      expect(actual['verbose'],                equals(options.verbose    ), reason: 'JSON-serialized value for `verbose` other than expected'    );
      expect(actual['pedantic'],               equals(options.pedantic   ), reason: 'JSON-serialized value for `pedantic` other than expected'   );
      expect(actual['tls_required'],           equals(options.tlsRequired), reason: 'JSON-serialized value for `tlsRequired` other than expected');
      expect(actual['echo'],                   equals(options.echo       ), reason: 'JSON-serialized value for `echo` other than expected'       );
      expect(actual.containsKey('name'),       isFalse,                     reason: 'JSON-serialized key `name` not omitted as expected'         );
      expect(actual.containsKey('auth_token'), isFalse,                     reason: 'JSON-serialized key `auth_token` not omitted as expected'   );
      expect(actual.containsKey('user'),       isFalse,                     reason: 'JSON-serialized key `user` not omitted as expected'         );
      expect(actual.containsKey('username'),   isFalse,                     reason: 'JSON-serialized key `user` not omitted as expected'         );
      expect(actual.containsKey('pass'),       isFalse,                     reason: 'JSON-serialized key `pass` not omitted as expected'         );
      expect(actual.containsKey('password'),   isFalse,                     reason: 'JSON-serialized key `pass` not omitted as expected'         );
    });

    test('optional properties included', () {
      options.name      = expectedName;
      options.authToken = expectedAuthToken;
      options.username  = expectedUsername;
      options.password  = expectedPassword;

      var actual = options.toMap();

      expect(actual.containsKey('lang'),     isTrue,                      reason: 'JSON-serialized key `lang` unexpectedly missing'            );
      expect(actual.containsKey('version'),  isTrue,                      reason: 'JSON-serialized key `version` unexpectedly missing'         );
      expect(actual['payloadType'],          equals(options.payloadType), reason: 'JSON-serialized value for `payloadType` other than expected');
      expect(actual['encoding'],             equals(options.encoding   ), reason: 'JSON-serialized value for `encoding` other than expected'   );
      expect(actual['protocol'],             equals(options.protocol   ), reason: 'JSON-serialized value for `protocol` other than expected'   );
      expect(actual['verbose'],              equals(options.verbose    ), reason: 'JSON-serialized value for `verbose` other than expected'    );
      expect(actual['pedantic'],             equals(options.pedantic   ), reason: 'JSON-serialized value for `pedantic` other than expected'   );
      expect(actual['tls_required'],         equals(options.tlsRequired), reason: 'JSON-serialized value for `tlsRequired` other than expected');
      expect(actual['echo'],                 equals(options.echo       ), reason: 'JSON-serialized value for `echo` other than expected'       );
      expect(actual['name'],                 equals(options.name       ), reason: 'JSON-serialized value for `name` other than expected'       );
      expect(actual['auth_token'],           equals(options.authToken  ), reason: 'JSON-serialized value for `auth_token` other than expected' );
      expect(actual['user'],                 equals(options.username   ), reason: 'JSON-serialized value for `user` other than expected'       );
      expect(actual['pass'],                 equals(options.password   ), reason: 'JSON-serialized value for `pass` other than expected'       );
      expect(actual.containsKey('username'), isFalse,                     reason: 'JSON-serialized key `user` not omitted as expected'         );
      expect(actual.containsKey('password'), isFalse,                     reason: 'JSON-serialized key `pass` not omitted as expected'         );
    });
  });
}
