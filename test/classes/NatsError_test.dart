import 'dart:mirrors';

import 'package:nats_dart/nats_dart.dart';
import 'package:test/test.dart';
import 'package:faker_extended/faker_extended.dart';

void main() {
  var testCases = [
    TestCase('BAD_AUTHENTICATION'                                                  ),
    TestCase('BAD_CREDENTIALS',                  aliases: ['BAD_CREDS'    ]        ),
    TestCase('BAD_NKEY_CREDENTIALS',             aliases: ['BAD_NKEY_SEED']        ),
    TestCase('BAD_JSON'                                                            ),
    TestCase('BAD_MESSAGE',                      aliases: ['BAD_MSG']              ),
    TestCase('BAD_REPLY'                                                           ),
    TestCase('BAD_SUBJECT'                                                         ),
    TestCase('CLIENT_CERT_REQUIRED',             aliases: ['CLIENT_CERT_REQ']      ),
    TestCase('CONNECTION_CLOSED',                aliases: ['CONN_CLOSED'    ]      ),
    TestCase('CONNECTION_DRAINING',              aliases: ['CONN_DRAINING'  ]      ),
    TestCase('CONNECTION_ERROR',                 aliases: ['CONN_ERR'       ]      ),
    TestCase('CONNECTION_TIMEOUT',               aliases: ['CONN_TIMEOUT'   ]      ),
    TestCase('INVALID_ENCODING'                                                    ),
    TestCase('NKEY_OR_JWT_REQUIRED',             aliases: ['NKEY_OR_JWT_REQ']      ),
    TestCase('NO_ECHO_NOT_SUPPORTED'                                               ),
    TestCase('NO_SEED_IN_CREDENTIALS',           aliases: ['NO_SEED_IN_CREDS'     ]),
    TestCase('NO_USER_JWT_IN_CREDENTIALS',       aliases: ['NO_USER_JWT_IN_CREDS' ]),
    TestCase('NON_SECURE_CONNECTION_REQUIRED',   aliases: ['NON_SECURE_CONN_REQ'  ]),
    TestCase('NONCE_SIGNER_NOT_FUNCTION',        aliases: ['NONCE_SIGNER_NOT_FUNC']),
    TestCase('REQUEST_TIMEOUT',                  aliases: ['REQ_TIMEOUT'          ]),
    TestCase('SECURE_CONNECTION_REQUIRED',       aliases: ['SECURE_CONN_REQ'      ]),
    TestCase('SIGNATURE_REQUIRED',               aliases: ['SIG_REQ'              ]),
    TestCase('SUBSCRIPTION_CLOSED',              aliases: ['SUB_CLOSED'           ]),
    TestCase('SUBSCRIPTION_DRAINING',            aliases: ['SUB_DRAINING'         ]),
    TestCase('SUBSCRIPTION_TIMEOUT',             aliases: ['SUB_TIMEOUT'          ]),
    TestCase('UNKNOWN'                                                             ),
  ];

  /// string names/aliases are registered with the static "constants" are first referenced. If these constants have never been referenced, getting/initializing these by string should still work.
  /// NOTE: this group should always run first to ensure the string names/aliases are resolving EVEN IF the statics haven't yet been referenced
  group('accessible by string', () {
    group('(name)', () {
      testCases.forEach((testCase) => test(testCase.name, () {
        var actual = NatsError.fromString(testCase.name);
        expect(actual.code, same(testCase.name), reason: "Expected enumeration name(string) not defined");
        testCase.expected = actual; // -- add the actual reference to our testCases array
      }));
    });

    group('(alias)', () => testCases.forEach((testCase) {
      group(testCase.name, () {
        testCase.aliases.forEach((alias) {
          test(alias, () => expect(NatsError.fromString(alias), same(testCase.expected), reason: "Expected enumeration name(string) not defined"));
        });
      });
    }));
  });

  group('constants exist', () {
    ClassMirror reflection = reflectClass(NatsError);

    testCases.map((testCase) => testCase.name).forEach((testCase) => test(testCase, () {
      expect(() => reflection.getField(Symbol(testCase)), returnsNormally, reason: "Expected enumeration value not defined");
    }));
  });

  group('can be nested', () {
    group('from instance', () {
      NatsError base;
      setUp(() {
        base = faker.randomGenerator.element(testCases.map<NatsError>((testCase) => testCase.expected).toList());
      });

      test('caused by NatsError', () {
        var cause = faker.randomGenerator.element(testCases.map<NatsError>((testCase) => testCase.expected).toList());

        var actual = NatsError.from(base, cause: cause);
        expect(actual,       equals(base),      reason: "returned instance is of an unexpected type"                                                               );
        expect(actual,       isNot(same(base)), reason: "returned instance is unexpectedly the exact same instance as the base type rather than an extension of it");
        expect(actual.cause, same(cause),       reason: "returned instance is unexpectedly not associated with the given cause"                                    );
      });

      test('caused by Error', () {
        var cause  = AssertionError();
        expect(() => NatsError.from(base, cause: cause), returnsNormally);
      });

      test('caused by Exception', () {
        var cause  = FormatException();
        expect(() => NatsError.from(base, cause: cause), returnsNormally);
      });

      test('caused by other', () {
        var cause  = faker.lorem.word();
        expect(() => NatsError.from(base, cause: cause), throwsA(TypeMatcher<AssertionError>()));
      });
    });

    group('from string', () {
      group('(by name)', () {
        NatsError expectedBase;
        String    baseName;

        setUp(() {
          expectedBase = faker.randomGenerator.element(testCases.map<NatsError>((testCase) => testCase.expected).toList());
          baseName     = expectedBase.code;
        });

        test('caused by NatsError', () {
          NatsError cause  = faker.randomGenerator.element(testCases.map<NatsError>((testCase) => testCase.expected).toList());
          NatsError actual = NatsError.fromString(baseName, cause: cause);

          expect(actual,       equals(expectedBase),      reason: "returned instance is of an unexpected type"                                                               );
          expect(actual,       isNot(same(expectedBase)), reason: "returned instance is unexpectedly the exact same instance as the base type rather than an extension of it");
          expect(actual.cause, same(cause),               reason: "returned instance is unexpectedly not associated with the given cause"                                    );
        });

        test('caused by Error', () {
          var cause  = AssertionError();
          expect(() => NatsError.fromString(baseName, cause: cause), returnsNormally);
        });

        test('caused by Exception', () {
          var cause  = FormatException();
          expect(() => NatsError.fromString(baseName, cause: cause), returnsNormally);
        });

        test('caused by other', () {
          var cause  = faker.lorem.word();
          expect(() => NatsError.fromString(baseName, cause: cause), throwsA(TypeMatcher<AssertionError>()));
        });
      });

      group('(by alias)', () {
        final List aliasedTestCases = testCases.where((testCase) => testCase.aliases.isNotEmpty).toList();

        String baseAlias;
        Object expectedBase;

        setUp(() {
          var randomTestCase = faker.randomGenerator.element(aliasedTestCases);
          baseAlias          = faker.randomGenerator.element(randomTestCase.aliases);
          expectedBase       = randomTestCase.expected;
        });

        test('caused by NatsError', () {
          NatsError cause    = faker.randomGenerator.element(testCases.map<NatsError>((testCase) => testCase.expected).toList());
          NatsError actual   = NatsError.fromString(baseAlias, cause: cause);

          expect(actual,       equals(expectedBase),      reason: "returned instance is of an unexpected type"                                                               );
          expect(actual,       isNot(same(expectedBase)), reason: "returned instance is unexpectedly the exact same instance as the base type rather than an extension of it");
          expect(actual.cause, same(cause),               reason: "returned instance is unexpectedly not associated with the given cause"                                    );
        });

        test('caused by Error', () {
          var cause  = AssertionError();
          expect(() => NatsError.fromString(baseAlias, cause: cause), returnsNormally);
        });

        test('caused by Exception', () {
          var cause  = FormatException();
          expect(() => NatsError.fromString(baseAlias, cause: cause), returnsNormally);
        });

        test('caused by other', () {
          var cause  = faker.lorem.word();
          expect(() => NatsError.fromString(baseAlias, cause: cause), throwsA(TypeMatcher<AssertionError>()));
        });
      });
    });
  });
}

class TestCase {
  String       name;
  List<String> aliases;
  Object       expected;

  TestCase(this.name, {this.aliases = const [], this.expected});
}
