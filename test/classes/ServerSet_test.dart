import 'package:nats_dart/nats_dart.dart';
import 'package:faker_extended/faker_extended.dart';
import 'package:test/test.dart';

void main() {
  test('can be created', () {
    var instance = ServerSet();

    expect(instance.servers, isA<List<Server>>());
  });

  group('Additions', () {
    ServerSet servers;
    String    expectedUrl;
    Server    expectedServer;

    setUp(() {
      servers        = ServerSet();
      expectedUrl    = faker.internet.uri(faker.randomGenerator.element(['http', 'https']));
      expectedServer = Server(expectedUrl);
    });

    test('via `add()`', () {
      servers.add(expectedServer);

      expect(servers.length, equals(1),              reason: 'server not added as expected'    );
      expect(servers.first,  equals(expectedServer), reason: 'added server other than expected');
    });
  });

  // @depends('Additions')
  group('Removal', () {
    ServerSet servers;
    Server    expectedServer;

    setUp(() {
      expectedServer = Server(faker.internet.uri(faker.randomGenerator.element(['http', 'https'])));
      servers        = ServerSet()..add(expectedServer);
    });

    test('via `remove()`', () {
      servers.remove(expectedServer);
      expect(servers.length, equals(0), reason: 'server not removed as expected');
    });
  });
}
