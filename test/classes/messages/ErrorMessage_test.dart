import 'package:nats_dart/nats_dart.dart';
import 'package:faker_extended/faker_extended.dart';
import 'package:test/test.dart';
import '../../mocks/factories/MessageProtoFactoryMock.dart';

void main() {
  container
    ..unregister<MessageProtoFactory>()..registerInstance<MessageProtoFactory>(MessageProtoFactoryMock());

  String       expectedMessage;
  ErrorMessage actual;

  setUp(() {
    expectedMessage = faker.lorem.sentence();
    actual          = ErrorMessage(expectedMessage);
  });

  group('can be created', () {
    test('default constructor', () {
      expect(actual,               isA<IncomingMessage>(),        reason: 'Message instance type other than expected'       );
      expect(actual,               isNot(isA<OutgoingMessage>()), reason: 'Message instance type other than expected'       );
      expect(actual,               isNot(isA<PayloadMessage>()),  reason: 'Message instance type other than expected'       );

      expect(actual.message,       equals(expectedMessage),       reason: 'Error message value other than expected'         );
      expect(actual.proto,         isA<ErrorMessageProto>(),      reason: 'Message proto type other than expected'          );
      expect(actual.proto.message, equals(actual.message),        reason: 'Message proto options value other than expected' );
    });

    test('from proto', () {
      var original = actual;
      actual       = ErrorMessage.fromProto(original.proto);

      expect(actual,         isNot(equals(original)),  reason: 'Message is an unexpected clone'         );
      expect(actual.message, equals(original.message), reason: 'Error message value other than expected');
    });
  });

  test('serialization', () {
    expect(actual.toProtocolString(), isA<String>(), reason: 'Serialized message type other than expected');
    expect(actual.toProtocolString(), isNotEmpty,    reason: 'Serialized message other than expected'     );
  });
}
