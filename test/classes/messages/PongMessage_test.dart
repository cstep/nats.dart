import 'package:nats_dart/nats_dart.dart';
import 'package:test/test.dart';
import '../../mocks/factories/MessageProtoFactoryMock.dart';

void main() {
  container
    ..unregister<MessageProtoFactory>()..registerInstance<MessageProtoFactory>(MessageProtoFactoryMock());

  PongMessage actual;

  setUp(() {
    actual = PongMessage();
  });

  group('can be created', () {
    test('default constructor', () {
      expect(actual,               isA<IncomingMessage>(),       reason: 'Message instance type other than expected'       );
      expect(actual,               isA<OutgoingMessage>(),       reason: 'Message instance type other than expected'       );
      expect(actual,               isNot(isA<PayloadMessage>()), reason: 'Message instance type other than expected'       );
      expect(actual.proto,         isA<PongMessageProto>(),      reason: 'Message proto type other than expected'          );
    });

    test('from proto', () {
      var original = actual;
      actual       = PongMessage.fromProto(original.proto);

      expect(actual, isNot(equals(original)), reason: 'Message is an unexpected clone');
    });
  });

  test('serialization', () {
    expect(actual.toProtocolString(), isA<String>(), reason: 'Serialized message type other than expected');
    expect(actual.toProtocolString(), isNotEmpty,    reason: 'Serialized message other than expected'     );
  });
}
