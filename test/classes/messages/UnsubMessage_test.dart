import 'package:nats_dart/nats_dart.dart';
import 'package:faker_extended/faker_extended.dart';
import 'package:test/test.dart';
import '../../mocks/factories/MessageProtoFactoryMock.dart';
import '../../mocks/factories/SubscriptionFactoryMock.dart';

void main() {
  container
    ..unregister<MessageProtoFactory>()..registerInstance<MessageProtoFactory>(MessageProtoFactoryMock())
    ..unregister<SubscriptionFactory>()..registerInstance<SubscriptionFactory>(SubscriptionFactoryMock());

  String expectedSid;
  int    expectedMax;

  setUp(() {
    expectedSid = faker.lorem.word();
    expectedMax = faker.randomGenerator.integer(10, min: 1);
  });

  group('can be created', () {
    test('with required args only', () {
      var actual = UnsubMessage(expectedSid);

      expect(actual, isA<OutgoingMessage>(),       reason: 'Message instance inherits unexpected class');
      expect(actual, isNot(isA<PayloadMessage>()), reason: 'Message instance inherits unexpected class');

      expect(actual.sid,     equals(expectedSid), reason: 'Message sid value other than expected');
      expect(actual.max,     isNull,              reason: 'Message max value other than expected');

      expect(actual.proto.sid, equals(actual.sid), reason: 'Message proto sid value other than expected'    );
      expect(actual.proto.max, equals(actual.max), reason: 'Message proto max value other than expected'    );
    });

    test('with optional args too', () {
      var actual = UnsubMessage(expectedSid, max: expectedMax);

      expect(actual.max,       expectedMax,        reason: 'Message max value other than expected');
      expect(actual.proto.max, equals(actual.max), reason: 'Message proto max value other than expected'    );
    });

    test('from subscription', () {
      var expectedSubject   = faker.lorem.word();
      var expectedQueueName = faker.lorem.word();
      var expectedSid       = faker.lorem.word();

      var subscription    = container.resolve<SubscriptionFactory>().subscription(null, expectedSubject, queue: expectedQueueName, sid: expectedSid);
      var actual          = UnsubMessage.fromSubscription(subscription, max: expectedMax);

      expect(actual.sid, equals(expectedSid), reason: 'Message sid value other than expected');
      expect(actual.max, equals(expectedMax), reason: 'Message max value other than expected');
    });
  });

  test('modification', () {
    var actual = UnsubMessage(faker.lorem.word(), max: faker.randomGenerator.integer(10, min: 1));

    actual.sid = expectedSid;
    actual.max = expectedMax;

    expect(actual.sid, equals(expectedSid), reason: 'Message sid value other than expected');
    expect(actual.max, equals(expectedMax), reason: 'Message max value other than expected');
  });

  test('serialization', () {
    var actual = UnsubMessage(expectedSid);

    expect(actual.toProtocolString(), isA<String>(), reason: 'Serialized message type other than expected');
    expect(actual.toProtocolString(), isNotEmpty,    reason: 'Serialized message other than expected'     );
  });
}
