import 'package:nats_dart/nats_dart.dart';
import 'package:faker_extended/faker_extended.dart';
import 'package:test/test.dart';
import '../../mocks/factories/MessageProtoFactoryMock.dart';

void main() {
  container
    ..unregister<MessageProtoFactory>()..registerInstance<MessageProtoFactory>(MessageProtoFactoryMock());

  String       expectedServerId;
  String       expectedVersion;
  String       expectedGoVersion;
  String       expectedHost;
  int          expectedPort;
  int          expectedMaxPayload;
  int          expectedProtocol;
  int          expectedClientId;
  bool         expectedAuthRequired;
  bool         expectedTlsRequired;
  bool         expectedTlsVerify;
  List<String> expectedConnectUrls;

  setUp(() {
    expectedServerId     = faker.lorem.word();
    expectedVersion      = faker.lorem.word();
    expectedGoVersion    = faker.lorem.word();
    expectedHost         = faker.lorem.word();
    expectedPort         = faker.randomGenerator.integer(5000, min: 1000);
    expectedMaxPayload   = faker.randomGenerator.integer(2^16, min: 2^8);
    expectedProtocol     = faker.randomGenerator.integer(1, min: 0);
    expectedClientId     = faker.randomGenerator.integer(2^64, min: 1);
    expectedAuthRequired = faker.randomGenerator.boolean();
    expectedTlsRequired  = faker.randomGenerator.boolean();
    expectedTlsVerify    = faker.randomGenerator.boolean();
    expectedConnectUrls  = List.generate(faker.randomGenerator.integer(10, min: 1), (_) => (faker.randomGenerator.boolean() ? faker.internet.httpUrl() : faker.internet.httpsUrl()));
  });

  group('can be created', () {
    test('with required args only', () {
      var actual = InfoMessage(expectedServerId, expectedVersion, expectedGoVersion, expectedHost, expectedPort, expectedMaxPayload, expectedProtocol, expectedClientId);

      expect(actual, isA<IncomingMessage>(),        reason: 'Message instance type other than expected');
      expect(actual, isNot(isA<OutgoingMessage>()), reason: 'Message instance type other than expected');
      expect(actual, isNot(isA<PayloadMessage>()),  reason: 'Message instance type other than expected');

      expect(actual.serverId,     equals(expectedServerId),   reason: 'Message serverId value other than expected'  );
      expect(actual.version,      equals(expectedVersion),    reason: 'Message version value other than expected'   );
      expect(actual.goVersion,    equals(expectedGoVersion),  reason: 'Message goVersion value other than expected' );
      expect(actual.host,         equals(expectedHost),       reason: 'Message host value other than expected'      );
      expect(actual.port,         equals(expectedPort),       reason: 'Message port value other than expected'      );
      expect(actual.maxPayload,   equals(expectedMaxPayload), reason: 'Message maxPayload value other than expected');
      expect(actual.protocol,     equals(expectedProtocol),   reason: 'Message protocol value other than expected'  );
      expect(actual.clientId,     equals(expectedClientId),   reason: 'Message clientId value other than expected'  );

      expect(actual.proto.serverId,     equals(actual.serverId),     reason: 'Message proto serverId value other than expected'    );
      expect(actual.proto.version,      equals(actual.version),      reason: 'Message proto version value other than expected'     );
      expect(actual.proto.goVersion,    equals(actual.goVersion),    reason: 'Message proto goVersion value other than expected'   );
      expect(actual.proto.host,         equals(actual.host),         reason: 'Message proto host value other than expected'        );
      expect(actual.proto.port,         equals(actual.port),         reason: 'Message proto port value other than expected'        );
      expect(actual.proto.maxPayload,   equals(actual.maxPayload),   reason: 'Message proto maxPayload value other than expected'  );
      expect(actual.proto.protocol,     equals(actual.protocol),     reason: 'Message proto protocol value other than expected'    );
      expect(actual.proto.clientId,     equals(actual.clientId),     reason: 'Message proto clientId value other than expected'    );
      expect(actual.proto.authRequired, equals(actual.authRequired), reason: 'Message proto authRequired value other than expected');
      expect(actual.proto.tlsRequired,  equals(actual.tlsRequired),  reason: 'Message proto tlsRequired value other than expected' );
      expect(actual.proto.tlsVerify,    equals(actual.tlsVerify),    reason: 'Message proto tlsVerify value other than expected'   );
      expect(actual.proto.connectUrls,  equals(actual.connectUrls),  reason: 'Message proto connectUrls value other than expected' );
    });

    test('with optional args, too', () {
      var actual = InfoMessage(expectedServerId, expectedVersion, expectedGoVersion, expectedHost, expectedPort, expectedMaxPayload, expectedProtocol, expectedClientId,
        authRequired: expectedAuthRequired,
         tlsRequired: expectedTlsRequired,
           tlsVerify: expectedTlsVerify,
         connectUrls: expectedConnectUrls,
      );

      expect(actual, isA<IncomingMessage>(),        reason: 'Message instance type other than expected');
      expect(actual, isNot(isA<OutgoingMessage>()), reason: 'Message instance type other than expected');
      expect(actual, isNot(isA<PayloadMessage>()),  reason: 'Message instance type other than expected');

      expect(actual.authRequired, equals(expectedAuthRequired), reason: 'Message authRequired value other than expected');
      expect(actual.tlsRequired,  equals(expectedTlsRequired),  reason: 'Message tlsRequired value other than expected' );
      expect(actual.tlsVerify,    equals(expectedTlsVerify),    reason: 'Message tlsVerify value other than expected'   );
      expect(actual.connectUrls,  equals(expectedConnectUrls),  reason: 'Message connectUrls value other than expected' );

      expect(actual.proto.authRequired, equals(actual.authRequired), reason: 'Message proto authRequired value other than expected');
      expect(actual.proto.tlsRequired,  equals(actual.tlsRequired),  reason: 'Message proto tlsRequired value other than expected' );
      expect(actual.proto.tlsVerify,    equals(actual.tlsVerify),    reason: 'Message proto tlsVerify value other than expected'   );
      expect(actual.proto.connectUrls,  equals(actual.connectUrls),  reason: 'Message proto connectUrls value other than expected' );
    });

    test('from proto', () {
      var original = InfoMessage(expectedServerId, expectedVersion, expectedGoVersion, expectedHost, expectedPort, expectedMaxPayload, expectedProtocol, expectedClientId,
        authRequired: expectedAuthRequired,
         tlsRequired: expectedTlsRequired,
           tlsVerify: expectedTlsVerify,
         connectUrls: expectedConnectUrls,
      );

      var actual = InfoMessage.fromProto(original.proto);

      expect(actual, isNot(equals(original)), reason: 'Message is an unexpected clone');

      expect(actual.serverId,     equals(original.serverId),     reason: 'Message serverId value other than expected'    );
      expect(actual.version,      equals(original.version),      reason: 'Message version value other than expected'     );
      expect(actual.goVersion,    equals(original.goVersion),    reason: 'Message goVersion value other than expected'   );
      expect(actual.host,         equals(original.host),         reason: 'Message host value other than expected'        );
      expect(actual.port,         equals(original.port),         reason: 'Message port value other than expected'        );
      expect(actual.maxPayload,   equals(original.maxPayload),   reason: 'Message maxPayload value other than expected'  );
      expect(actual.protocol,     equals(original.protocol),     reason: 'Message protocol value other than expected'    );
      expect(actual.clientId,     equals(original.clientId),     reason: 'Message clientId value other than expected'    );
      expect(actual.authRequired, equals(original.authRequired), reason: 'Message authRequired value other than expected');
      expect(actual.tlsRequired,  equals(original.tlsRequired),  reason: 'Message tlsRequired value other than expected' );
      expect(actual.tlsVerify,    equals(original.tlsVerify),    reason: 'Message tlsVerify value other than expected'   );
      expect(actual.connectUrls,  equals(original.connectUrls),  reason: 'Message connectUrls value other than expected' );
    });
  });

  test('serialization', () {
    var actual = InfoMessage(expectedServerId, expectedVersion, expectedGoVersion, expectedHost, expectedPort, expectedMaxPayload, expectedProtocol, expectedClientId);

    expect(actual.toProtocolString(), isA<String>(), reason: 'Serialized message type other than expected');
    expect(actual.toProtocolString(), isNotEmpty,    reason: 'Serialized message other than expected'     );
  });
}
