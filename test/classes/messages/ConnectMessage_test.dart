import 'package:nats_dart/nats_dart.dart';
import 'package:test/test.dart';
import '../../mocks/ServerOptionsMock.dart';
import '../../mocks/factories/MessageProtoFactoryMock.dart';

void main() {
  container
    ..unregister<MessageProtoFactory>()..registerInstance<MessageProtoFactory>(MessageProtoFactoryMock());

  ServerOptions  expectedOptions;
  ConnectMessage actual;

  setUp(() {
    expectedOptions = ServerOptionsMock();
    actual          = ConnectMessage(expectedOptions);
  });

  test('constructor', () {
    expect(actual,               isA<OutgoingMessage>(),        reason: 'Message instance type other than expected'      );
    expect(actual,               isNot(isA<IncomingMessage>()), reason: 'Message instance type other than expected'      );
    expect(actual,               isNot(isA<PayloadMessage>()),  reason: 'Message instance type other than expected'      );

    expect(actual.options,       isA<ServerOptions>(),          reason: 'Message options type other than expected'       );
    expect(actual.options,       equals(expectedOptions),       reason: 'Message options value other than expected'      );
    expect(actual.proto,         isA<ConnectMessageProto>(),    reason: 'Message proto type other than expected'         );
    expect(actual.proto.options, equals(actual.options),        reason: 'Message proto options value other than expected');
  });

  test('modification', () {
    var newExpectedOptions = ServerOptionsMock();
    actual.options         = newExpectedOptions;

    expect(actual.options,       isNot(equals(expectedOptions)), reason: 'Message options value unexpectedly not updated'                 );
    expect(actual.options,       equals(newExpectedOptions),     reason: 'Message options value updated, but in a way other than expected');
    expect(actual.proto.options, equals(actual.options),         reason: 'Message proto options value other than expected'                );
  });

  test('serialization', () {
    expect(actual.toProtocolString(), isA<String>(), reason: 'Serialized message type other than expected');
    expect(actual.toProtocolString(), isNotEmpty,    reason: 'Serialized message other than expected'     );
  });
}
