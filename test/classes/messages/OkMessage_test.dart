import 'package:nats_dart/nats_dart.dart';
import 'package:test/test.dart';
import '../../mocks/factories/MessageProtoFactoryMock.dart';

void main() {
  container
    ..unregister<MessageProtoFactory>()..registerInstance<MessageProtoFactory>(MessageProtoFactoryMock());

  OkMessage actual;

  setUp(() {
    actual = OkMessage();
  });

  group('can be created', () {
    test('default constructor', () {
      expect(actual,               isA<IncomingMessage>(),        reason: 'Message instance type other than expected'       );
      expect(actual,               isNot(isA<OutgoingMessage>()), reason: 'Message instance type other than expected'       );
      expect(actual,               isNot(isA<PayloadMessage>()),  reason: 'Message instance type other than expected'       );
      expect(actual.proto,         isA<OkMessageProto>(),         reason: 'Message proto type other than expected'          );
    });

    test('from proto', () {
      var original = actual;
      actual       = OkMessage.fromProto(original.proto);

      expect(actual, isNot(equals(original)), reason: 'Message is an unexpected clone');
    });
  });

  test('serialization', () {
    expect(actual.toProtocolString(), isA<String>(), reason: 'Serialized message type other than expected');
    expect(actual.toProtocolString(), isNotEmpty,    reason: 'Serialized message other than expected'     );
  });
}
