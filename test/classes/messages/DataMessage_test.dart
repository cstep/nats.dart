import 'dart:convert';
import 'dart:typed_data';
import 'package:nats_dart/nats_dart.dart';
import 'package:faker_extended/faker_extended.dart';
import 'package:test/test.dart';
import '../../mocks/factories/MessageProtoFactoryMock.dart';

void main() {
  container
    ..unregister<MessageProtoFactory>()..registerInstance<MessageProtoFactory>(MessageProtoFactoryMock());

  String                   expectedSubject;
  String                   expectedSid;
  String                   expectedDecodedPayload;
  Map<Encoding, Uint8List> encodedPayloads;
  Encoding                 expectedEncoding;
  String                   expectedReplySubject;

  setUp(() {
    expectedSubject        = faker.lorem.word();
    expectedSid            = faker.lorem.word();
    expectedReplySubject   = faker.lorem.word();
    expectedDecodedPayload = faker.lorem.sentences(faker.randomGenerator.integer(10, min: 0)).join(' ');
    expectedEncoding       = faker.randomGenerator.element([ascii, utf8, latin1]);
    encodedPayloads = {
      ascii : ascii.encode(expectedDecodedPayload),
      utf8  : utf8.encode(expectedDecodedPayload),
      latin1: latin1.encode(expectedDecodedPayload),
    };
  });

  group('can be created', () {
    group('default constructor', () {
      test('with required args only', () {
        var expectedEncodedPayload = encodedPayloads[ascii];
        var actual                 = DataMessage(expectedSubject, expectedSid, expectedEncodedPayload);

        expect(actual, isA<PayloadMessage>(),         reason: 'Message instance type other than expected');
        expect(actual, isA<IncomingMessage>(),        reason: 'Message instance type other than expected');
        expect(actual, isNot(isA<OutgoingMessage>()), reason: 'Message instance type other than expected');

        expect(actual.subject,        equals(expectedSubject),               reason: 'Message subject value other than expected'        );
        expect(actual.sid,            equals(expectedSid),                   reason: 'Message sid value other than expected'            );
        expect(actual.encoding,       equals(ascii),                         reason: 'Message default encoding other than expected'     );
        expect(actual.payload,        equals(expectedEncodedPayload),        reason: 'Message payload value other than expected'        );
        expect(actual.encodedPayload, equals(expectedEncodedPayload),        reason: 'Message encodedPayload value other than expected' );
        expect(actual.decodedPayload, equals(expectedDecodedPayload),        reason: 'Message decodedPayload value other than expected' );
        expect(actual.replySubject,   isNull,                                reason: 'Message replySubject value unexpectedly populated');
        expect(actual.byteLength,     equals(expectedEncodedPayload.length), reason: 'Message byteLength value other than expected'     );

        expect(actual.proto,              isA<DataMessageProto>(),     reason: 'Message proto type other than expected'              );
        expect(actual.proto.subject,      equals(actual.subject),      reason: 'Message proto subject value other than expected'     );
        expect(actual.proto.sid,          equals(actual.sid),          reason: 'Message proto sid value other than expected'         );
        expect(actual.proto.replySubject, equals(actual.replySubject), reason: 'Message proto replySubject value other than expected');
        expect(actual.proto.byteLength,   equals(actual.byteLength),   reason: 'Message proto byteLength value other than expected'  );
      });

      test('with optional args, too', () {
        var expectedEncodedPayload = encodedPayloads[expectedEncoding];
        var actual                 = DataMessage(expectedSubject, expectedSid, expectedEncodedPayload, encoding: expectedEncoding, replySubject: expectedReplySubject);

        expect(actual.encoding,       equals(expectedEncoding),              reason: 'Message default encoding other than expected'    );
        expect(actual.payload,        equals(expectedEncodedPayload),        reason: 'Message payload value other than expected'       );
        expect(actual.encodedPayload, equals(expectedEncodedPayload),        reason: 'Message encodedPayload value other than expected');
        expect(actual.decodedPayload, equals(expectedDecodedPayload),        reason: 'Message decodedPayload value other than expected');
        expect(actual.replySubject,   equals(expectedReplySubject),          reason: 'Message replySubject value other than expected'  );
        expect(actual.byteLength,     equals(expectedEncodedPayload.length), reason: 'Message byteLength value other than expected'    );

        expect(actual.proto,              isA<DataMessageProto>(),     reason: 'Message proto type other than expected'              );
        expect(actual.proto.replySubject, equals(actual.replySubject), reason: 'Message proto replySubject value other than expected');
        expect(actual.proto.byteLength,   equals(actual.byteLength),   reason: 'Message proto byteLength value other than expected'  );
      });
    });

    test('fromProto', () {
      var expectedEncodedPayload = encodedPayloads[expectedEncoding];
      var original               = DataMessage(expectedSubject, expectedSid, expectedEncodedPayload, encoding: expectedEncoding, replySubject: expectedReplySubject);
      var actual                 = DataMessage.fromProto(original.proto, original.payload, encoding: original.encoding);

      expect(actual, isNot(equals(original)), reason: 'Message is an unexpected clone');

      expect(actual.subject,        equals(original.subject),               reason: 'Message subject value other than expected'       );
      expect(actual.sid,            equals(original.sid),                   reason: 'Message sid value other than expected'           );
      expect(actual.encoding,       equals(original.encoding),              reason: 'Message default encoding other than expected'    );
      expect(actual.payload,        equals(original.encodedPayload),        reason: 'Message payload value other than expected'       );
      expect(actual.encodedPayload, equals(original.encodedPayload),        reason: 'Message encodedPayload value other than expected');
      expect(actual.decodedPayload, equals(original.decodedPayload),        reason: 'Message decodedPayload value other than expected');
      expect(actual.replySubject,   equals(original.replySubject),          reason: 'Message replySubject value other than expected'  );
      expect(actual.byteLength,     equals(original.encodedPayload.length), reason: 'Message byteLength value other than expected'    );
    });

    group('errors if unencoded payload and explicitly undefined encoding', () {
      Function shouldThrow;

      tearDown(() {
        expect(shouldThrow, throwsA(isA<ArgumentError>()));
      });

      test('default constructor', () {
        shouldThrow = () {
          DataMessage(expectedSubject, expectedSid, expectedDecodedPayload, encoding: null, replySubject: expectedReplySubject);
        };
      });

      test('fromProto', () {
        shouldThrow = () {
          var proto = container.resolve<MessageProtoFactory>().data(expectedSubject, expectedSid, faker.randomGenerator.integer(2^10));
          DataMessage.fromProto(proto, expectedDecodedPayload, encoding: null);
        };
      });
    });
  });

  test('serialization', () {
    var expectedEncodedPayload = encodedPayloads[ascii];
    var actual                 = DataMessage(expectedSubject, expectedSid, expectedEncodedPayload);

    expect(actual.toProtocolString(), isA<String>(), reason: 'Serialized message type other than expected');
    expect(actual.toProtocolString(), isNotEmpty,    reason: 'Serialized message other than expected'     );
  });
}
