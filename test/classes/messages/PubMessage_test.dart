import 'dart:convert';
import 'dart:typed_data';
import 'package:nats_dart/nats_dart.dart';
import 'package:faker_extended/faker_extended.dart';
import 'package:test/test.dart';
import '../../mocks/factories/MessageProtoFactoryMock.dart';

void main() {
  container
    ..unregister<MessageProtoFactory>()..registerInstance<MessageProtoFactory>(MessageProtoFactoryMock());

  String                   expectedSubject;
  String                   expectedReplySubject;
  String                   expectedDecodedPayload;
  Map<Encoding, Uint8List> encodedPayloads;
  Encoding                 expectedEncoding;

  setUp(() {
    expectedSubject        = faker.lorem.word();
    expectedReplySubject   = faker.lorem.word();
    expectedDecodedPayload = faker.lorem.sentences(faker.randomGenerator.integer(10, min: 0)).join(' ');
    expectedEncoding       = faker.randomGenerator.element([ascii, utf8, latin1]);
    encodedPayloads = {
      ascii :  ascii.encode(expectedDecodedPayload),
      utf8  :   utf8.encode(expectedDecodedPayload),
      latin1: latin1.encode(expectedDecodedPayload),
    };
  });

  group('can be created', () {
    test('with required args only', () {
      var expectedEncodedPayload = encodedPayloads[ascii];
      var actual                 = PubMessage(expectedSubject, expectedDecodedPayload);

      expect(actual, isA<OutgoingMessage>(), reason: 'Message instance inherits unexpected class');
      expect(actual, isA<PayloadMessage>(),  reason: 'Message instance inherits unexpected class');

      expect(actual.subject,        equals(expectedSubject),               reason: 'Message subject value other than expected'        );
      expect(actual.encoding,       equals(ascii),                         reason: 'Message default encoding other than expected'     );
      expect(actual.payload,        equals(expectedDecodedPayload),        reason: 'Message payload value other than expected'        );
      expect(actual.encodedPayload, equals(expectedEncodedPayload),        reason: 'Message encodedPayload value other than expected' );
      expect(actual.decodedPayload, equals(expectedDecodedPayload),        reason: 'Message decodedPayload value other than expected' );
      expect(actual.replySubject,   isNull,                                reason: 'Message replySubject value unexpectedly populated');
      expect(actual.byteLength,     equals(expectedEncodedPayload.length), reason: 'Message byteLength value other than expected'     );

      expect(actual.proto.subject,      equals(actual.subject),      reason: 'Message proto subject value other than expected'     );
      expect(actual.proto.replySubject, equals(actual.replySubject), reason: 'Message proto replySubject value other than expected');
      expect(actual.proto.byteLength,   equals(actual.byteLength),   reason: 'Message proto byteLength value other than expected'  );
    });

    test('with optional args too', () {
      var expectedEncodedPayload = encodedPayloads[expectedEncoding];
      var actual = PubMessage(expectedSubject, expectedDecodedPayload, encoding: expectedEncoding, replySubject: expectedReplySubject);

      expect(actual.subject,        equals(expectedSubject),               reason: 'Message subject value other than expected'       );
      expect(actual.encoding,       equals(expectedEncoding),              reason: 'Message default encoding other than expected'    );
      expect(actual.payload,        equals(expectedDecodedPayload),        reason: 'Message payload value other than expected'       );
      expect(actual.encodedPayload, equals(expectedEncodedPayload),        reason: 'Message encodedPayload value other than expected');
      expect(actual.decodedPayload, equals(expectedDecodedPayload),        reason: 'Message decodedPayload value other than expected');
      expect(actual.replySubject,   equals(expectedReplySubject),          reason: 'Message replySubject value other than expected'  );
      expect(actual.byteLength,     equals(expectedEncodedPayload.length), reason: 'Message byteLength value other than expected'    );

      expect(actual.proto.subject,      equals(actual.subject),      reason: 'Message proto subject value other than expected'     );
      expect(actual.proto.replySubject, equals(actual.replySubject), reason: 'Message proto replySubject value other than expected');
      expect(actual.proto.byteLength,   equals(actual.byteLength),   reason: 'Message proto byteLength value other than expected'  );
    });

    test('errors if unencoded payload and explicitly undefined encoding', () {
      var shouldThrow = () {
        PubMessage(expectedSubject, expectedDecodedPayload, encoding: null);
      };

      expect(shouldThrow, throwsA(isA<ArgumentError>()));
    });
  });

  test('serialization', () {
    var actual = PubMessage(expectedSubject, expectedDecodedPayload);

    expect(actual.toProtocolString(), isA<String>(), reason: 'Serialized message type other than expected');
    expect(actual.toProtocolString(), isNotEmpty,    reason: 'Serialized message other than expected'     );
  });
}
