import 'package:nats_dart/nats_dart.dart';
import 'package:faker_extended/faker_extended.dart';
import 'package:test/test.dart';
import '../../mocks/factories/MessageProtoFactoryMock.dart';
import '../../mocks/factories/SubscriptionFactoryMock.dart';

void main() {
  container
    ..unregister<MessageProtoFactory>()..registerInstance<MessageProtoFactory>(MessageProtoFactoryMock())
    ..unregister<SubscriptionFactory>()..registerInstance<SubscriptionFactory>(SubscriptionFactoryMock());

  String expectedSubject;
  String expectedSid;
  String expectedQueueName;

  setUp(() {
    expectedSubject   = faker.lorem.word();
    expectedSid       = faker.lorem.word();
    expectedQueueName = faker.lorem.word();
  });

  group('can be created', () {
    test('with required args only', () {
      var actual = SubMessage(expectedSubject, expectedSid);

      expect(actual, isA<OutgoingMessage>(),       reason: 'Message instance inherits unexpected class');
      expect(actual, isNot(isA<PayloadMessage>()), reason: 'Message instance inherits unexpected class');

      expect(actual.subject, equals(expectedSubject), reason: 'Message subject value other than expected');
      expect(actual.sid,     equals(expectedSid),     reason: 'Message sid value other than expected'    );

      expect(actual.proto.subject, equals(actual.subject), reason: 'Message proto subject value other than expected');
      expect(actual.proto.sid,     equals(actual.sid),     reason: 'Message proto sid value other than expected'    );
    });

    test('with optional args too', () {
      var actual = SubMessage(expectedSubject, expectedSid, queue: expectedQueueName);

      expect(actual.queue,       equals(expectedQueueName), reason: 'Message queue value other than expected'      );
      expect(actual.proto.queue, equals(actual.queue),      reason: 'Message proto queue value other than expected');
    });

    test('from subscription', () {
      var expectedSubject   = faker.lorem.word();
      var expectedQueueName = faker.lorem.word();
      var expectedSid       = faker.lorem.word();

      var subscription = container.resolve<SubscriptionFactory>().subscription(null, expectedSubject, queue: expectedQueueName, sid: expectedSid);
      var actual       = SubMessage.fromSubscription(subscription);

      expect(actual.subject, equals(expectedSubject),   reason: 'Message subject value other than expected');
      expect(actual.sid,     equals(expectedSid),       reason: 'Message sid value other than expected'    );
      expect(actual.queue,   equals(expectedQueueName), reason: 'Message queue value other than expected'  );
    });
  });

  test('modification', () {
    var actual = SubMessage(faker.lorem.word(), faker.lorem.word(), queue: faker.lorem.word());

    actual.subject = expectedSubject;
    actual.sid     = expectedSid;
    actual.queue   = expectedQueueName;

    expect(actual.subject, equals(expectedSubject),   reason: 'Message subject value other than expected');
    expect(actual.sid,     equals(expectedSid),       reason: 'Message sid value other than expected'    );
    expect(actual.queue,   equals(expectedQueueName), reason: 'Message queue value other than expected'  );
  });

  test('serialization', () {
    var actual = SubMessage(expectedSubject, expectedSid);

    expect(actual.toProtocolString(), isA<String>(), reason: 'Serialized message type other than expected');
    expect(actual.toProtocolString(), isNotEmpty,    reason: 'Serialized message other than expected'     );
  });
}
