import 'dart:async';
import 'dart:convert';
import 'dart:mirrors';
import 'package:nats_dart/nats_dart.dart';
import 'package:faker_extended/faker_extended.dart';
import 'package:pedantic/pedantic.dart';
import 'package:test/test.dart';
import 'package:mockito/mockito.dart';
import '../mocks/ProtocolHandlerMock.dart';
import '../mocks/factories/MessageFactoryMock.dart';

void main() {
  group('can be created', () {
    String expectedSubject;
    setUp(() {
      expectedSubject = faker.lorem.word();
    });

    test('with required args only', () {
      var actual = Subscription(ProtocolHandlerMock(), expectedSubject);

      expect(actual,         isA<Subscription>(),     reason: 'Subscription instance type other than expected');
      expect(actual.subject, equals(expectedSubject), reason: 'Subscription subject other than expected'      );
      expect(actual.sid,     isNotEmpty,              reason: 'Subscription SID not generated as expected'    );
    });

    group('with optional args too', () {
      test('queue name', () {
        var expectedQueueName = faker.lorem.word();
        var actual            = Subscription(ProtocolHandlerMock(), expectedSubject, queue: expectedQueueName);

        expect(actual.subject, equals(expectedSubject),   reason: 'Subscription subject other than expected'   );
        expect(actual.queue,   equals(expectedQueueName), reason: 'Subscription queue name other than expected');
        expect(actual.sid,     isNotEmpty,                reason: 'Subscription SID not generated as expected' );
      });

      test('sid', () {
        var expectedSid = faker.lorem.word();
        var actual      = Subscription(ProtocolHandlerMock(), expectedSubject, sid: expectedSid);

        expect(actual.subject, equals(expectedSubject), reason: 'Subscription subject other than expected'   );
        expect(actual.sid,     equals(expectedSid),     reason: 'Subscription queue name other than expected');
      });
    });
  });

  group('event stream', () {
    ProtocolHandlerMock protocolHandler;

    /// simulate an incoming message event and allow runLoops and events to propagate
    Future<void> simulateMessage(String sid, String payload) {
      return Future
        .sync(() => protocolHandler.fakeIncoming(MessageFactoryMock().data(faker.lorem.word(), sid, ascii.encode(payload)))) // -- Future.sync() runs this asynchronously NOW
        .then((_) => Future(() {}));                                                                                             // -- Future() allows runLoops and events to propagate before we finish
    }

    setUp(() {
      protocolHandler = ProtocolHandlerMock();
    });

    test('gets initialized', () async {
      var handler      = ProtocolHandlerMock();
      var subscription = Subscription(handler, faker.lorem.word());

      var stream = reflect(subscription).getField(MirrorSystem.getSymbol('_stream', reflectClass(Subscription).owner)).reflectee; // -- private property, need reflection
      expect(stream,                     isA<Stream<DataMessage>>(), reason: 'internal stream either not initialized or initialized other than expected');
      expect(subscription.receivedCount, isZero,                  reason: 'starting received message count other than expected'                      );
    });

    group('can listen', () {
      void messageValidator(dynamic message, String expectedSid, String expectedMessage, {String label}) {
        var prefix = ((label??'').isNotEmpty ? 'Handler ($label) : ' : '');

        expect(message,                isA<DataMessage>(),  reason: '${prefix}Captured message of type other than expected'                        );
        expect(message.sid,            equals(expectedSid), reason: '${prefix}Captured message (${message.sid}) was not intended for this listener');
        expect(message.decodedPayload, expectedMessage,     reason: '${prefix}Captured message payload other than expected'                        );
      }

      test('receives messages', () async {
        var    subscription    = Subscription(protocolHandler, 'subject', sid: faker.lorem.word());
        String expectedMessage = faker.lorem.sentence();

        // -------- set up our listeners
        subscription.listen(expectAsync1((message) => messageValidator(message, subscription.sid, expectedMessage), count: 1, reason: 'Handler heard more/less events than the expected 1'));

        // -------- send a message
        await simulateMessage(subscription.sid, expectedMessage);
        expect(subscription.receivedCount, equals(1), reason: 'internal stream not counting incoming events as expected');
      });

      test('multiple listeners', () async {
        var    subscription    = Subscription(protocolHandler, faker.lorem.word(), sid: faker.lorem.word());
        String expectedMessage = faker.lorem.sentence();

        // -------- set up our listeners
        subscription.listen(expectAsync1((message) => messageValidator(message, subscription.sid, expectedMessage, label: '1'), count: 1, reason: 'Handler (1) : heard more/less events than the expected 1'));
        subscription.listen(expectAsync1((message) => messageValidator(message, subscription.sid, expectedMessage, label: '2'), count: 1, reason: 'Handler (2) : heard more/less events than the expected 1'));

        // -------- send a message
        await simulateMessage(subscription.sid, expectedMessage);
        expect(subscription.receivedCount, equals(1), reason: 'subscription not counting incoming events as expected'); // -- despite 2 listeners, only 1 message sent, count should only be 1
      });

      test('shouldn\'t crosstalk', () async {
        var    subject              = faker.lorem.word();
        var    mainSubscription     = Subscription(protocolHandler, subject);
        var    decoySubscription    = Subscription(protocolHandler, subject);
        String expectedMainMessage  = 'this is the main message';
        String expectedDecoyMessage = 'this is the decoy message';

        // -------- set up our listeners
        mainSubscription. listen(expectAsync1((message) => messageValidator(message, mainSubscription.sid,  expectedMainMessage,  label: 'main1'), count: 1, reason: 'MainListener1 heard more/less events than the expected 1'));
        mainSubscription. listen(expectAsync1((message) => messageValidator(message, mainSubscription.sid,  expectedMainMessage,  label: 'main2'), count: 1, reason: 'MainListener2 heard more/less events than the expected 1'));
        decoySubscription.listen(expectAsync1((message) => messageValidator(message, decoySubscription.sid, expectedDecoyMessage, label: 'decoy'), count: 1, reason: 'DecoyListener heard more/less events than the expected 1'));

        // -------- send messages
        await Future(() async {
          await simulateMessage(mainSubscription.sid,  expectedMainMessage );
          await simulateMessage(decoySubscription.sid, expectedDecoyMessage);
        });

        expect(mainSubscription.receivedCount,  equals(1), reason: 'mainSubscription  not counting incoming events as expected'); // -- despite 2 listeners, only 1 message sent, count should only be 1
        expect(decoySubscription.receivedCount, equals(1), reason: 'decoySubscription not counting incoming events as expected');
      });
    });

    test('can be transformed', () async {
      void messageValidator(dynamic message, String expectedSid, String expectedMessage, {String label}) {
        var prefix = ((label??'').isNotEmpty ? 'Handler ($label) : ' : '');

        expect(message, isA<String>(),           reason: '${prefix}Transformed message of type other than expected');
        expect(message, equals(expectedMessage), reason: '${prefix}Transformed message value other than expected'  );
      }

      var    subscription    = Subscription(protocolHandler, 'subject', sid: faker.lorem.word());
      String expectedMessage = faker.lorem.sentence();

      // -------- set up our listeners
      subscription.payloadStream.listen(expectAsync1((message) => messageValidator(message, subscription.sid, expectedMessage), count: 1, reason: 'Handler heard more/less events than the expected 1'));

      // -------- send a message
      await simulateMessage(subscription.sid, expectedMessage);
      expect(subscription.receivedCount, equals(1), reason: 'internal stream not counting incoming events as expected');
    });

    //@depends('can listen')
    group('can unsubscribe', () {
      test('all listeners are cancelled', () async {
        var subscription = Subscription(protocolHandler, 'subject', sid: faker.lorem.word());

        // -------- set up our listeners
        List<StreamSubscription> allListeners = [
          subscription.listen(expectAsync1((_) => null, count: 0, reason: 'Listener (1) : handler received message after allegedly unsubscribing, but shouldn\'t have')), // -- expect this to be called 0 times!
          subscription.listen(expectAsync1((_) => null, count: 0, reason: 'Listener (2) : handler received message after allegedly unsubscribing, but shouldn\'t have')), // -- expect this to be called 0 times!
        ];

        List<StreamSubscription> trackedListeners = reflect(subscription).getField(MirrorSystem.getSymbol('_listeners', reflectClass(Subscription).owner)).reflectee;
        expect(trackedListeners, hasLength(2), reason: 'Initial number of listeners other than expected');

        // -------- unsubscribe
        await subscription.unsubscribe();

        // -------- send a message (shouldn't be received by any listeners)
        await simulateMessage(subscription.sid, faker.lorem.sentence());

        // ======== Verify Unsubscription
          // -------- listeners are actually canceled
          int acceptableStates = (
            2 | /// @see [dart:async/stream_impl.dart : _BufferingStreamSubscription._STATE_CLOSED]
            8 | /// @see [dart:async/stream_impl.dart : _BufferingStreamSubscription._STATE_CANCELED]
            16  /// @see [dart:async/stream_impl.dart : _BufferingStreamSubscription._STATE_WAIT_FOR_CANCEL]
          );
          allListeners.forEach((listener) {
            var currentState = reflect(listener).getField(MirrorSystem.getSymbol('_state', reflectClass(listener.runtimeType).owner)).reflectee; // -- private property, need reflection
            expect((currentState & acceptableStates), isNonZero, reason: 'Listener state other than expected');
          });

          verify(protocolHandler.unsubscribe(any, max: anyNamed('max'))).called(1);
          expect(trackedListeners,           isEmpty, reason: 'Listeners not removed from collection as expected');
          expect(subscription.receivedCount, isZero,  reason: 'subscription unexpectedly counted incoming event(s) after allegedly unsubscribing');
      });

      test('other subscriptions are not affected', () async {
        // -------- set up our listeners
        var subscription1 = Subscription(protocolHandler, 'subject', sid: faker.lorem.word());
        Map<String, StreamSubscription> allListeners1 = {
          'a': subscription1.listen(expectAsync1((_) => null, count: 0, reason: 'Listener (1a) : handler received message after allegedly unsubscribing, but shouldn\'t have')), // -- expect this to be called 0 times!
          'b': subscription1.listen(expectAsync1((_) => null, count: 0, reason: 'Listener (1b) : handler received message after allegedly unsubscribing, but shouldn\'t have')), // -- expect this to be called 0 times!
        };

        var subscription2 = Subscription(protocolHandler, 'subject', sid: faker.lorem.word());
        Map<String, StreamSubscription> allListeners2 = {
          'a': subscription2.listen(expectAsync1((_) => null, count: 1, reason: 'Listener (2a) : handler didn\'t receive expected message after unsubscribing a different subscription')), // -- expect this to be called 0 times!
          'b': subscription2.listen(expectAsync1((_) => null, count: 1, reason: 'Listener (2b) : handler didn\'t receive expected message after unsubscribing a different subscription')), // -- expect this to be called 0 times!
        };

        List<StreamSubscription> trackedListeners1 = reflect(subscription1).getField(MirrorSystem.getSymbol('_listeners', reflectClass(Subscription).owner)).reflectee;
        expect(trackedListeners1, hasLength(2), reason: 'Subscription1: Initial number of listeners other than expected');

        List<StreamSubscription> trackedListeners2 = reflect(subscription2).getField(MirrorSystem.getSymbol('_listeners', reflectClass(Subscription).owner)).reflectee;
        expect(trackedListeners1, hasLength(2), reason: 'Subscription2: Initial number of listeners other than expected');

        // -------- unsubscribe
        await subscription1.unsubscribe(); // -- only unsub this one, the other should be unaffected

        // -------- send a message (shouldn't be received by any listeners)
        var msg1 = simulateMessage(subscription1.sid, faker.lorem.sentence());
        var msg2 = simulateMessage(subscription2.sid, faker.lorem.sentence());

        // ======== Verify Unsubscription Stays Contained Within Scope
          int doneStates = (
            2 | /// @see [dart:async/stream_impl.dart : _BufferingStreamSubscription._STATE_CLOSED]
            8 | /// @see [dart:async/stream_impl.dart : _BufferingStreamSubscription._STATE_CANCELED]
            16  /// @see [dart:async/stream_impl.dart : _BufferingStreamSubscription._STATE_WAIT_FOR_CANCEL]
          );

          // -------- unsubscribed listeners are actually canceled
          allListeners1.forEach((String label, listener) {
            var currentState = reflect(listener).getField(MirrorSystem.getSymbol('_state', reflectClass(listener.runtimeType).owner)).reflectee; // -- private property, need reflection
            expect((currentState & doneStates), isNonZero, reason: 'Unsubscribed listener${label.toUpperCase()} state other than expected');
          });

          // -------- other listeners are unaffected
          allListeners2.forEach((String label, listener) {
            var currentState = reflect(listener).getField(MirrorSystem.getSymbol('_state', reflectClass(listener.runtimeType).owner)).reflectee; // -- private property, need reflection
            expect((currentState & doneStates), isZero, reason: 'Subscribed listener${label.toUpperCase()} state other than expected');
          });

          verify(protocolHandler.unsubscribe(any, max: anyNamed('max'))).called(1);
          expect(trackedListeners1, isEmpty,      reason: 'Listeners not removed from collection1 as expected');
          expect(trackedListeners2, hasLength(2), reason: 'Listeners unexpectedly removed from collection2'   );

          // -------- message counts are correct
          await Future.wait([msg1, msg2]); // -- send both simultaneously, but also wait for both messages to finish sending
          expect(subscription1.receivedCount, isZero,    reason: 'Subscription1: unexpectedly counted incoming event(s) after allegedly unsubscribing');
          expect(subscription2.receivedCount, equals(1), reason: 'Subscription2: not counting incoming events as expected'                            );
      });

      group('automatically', () {
        test('via setting initial limit', () {
          int expectedMax = 1;

          // ======== Subscribe
          var subscription = Subscription(protocolHandler, faker.lorem.word(), max: expectedMax);

          // ======== Inject Some Messages
          Future(() async {
            unawaited(simulateMessage(subscription.sid, faker.lorem.sentence()));
            unawaited(simulateMessage(subscription.sid, faker.lorem.sentence())); // -- max was set to 1, so our subscription should never receive this one
          });

          // ======== Verify Results
          var listeners = reflect(subscription).getField(MirrorSystem.getSymbol('_listeners', reflectClass(Subscription).owner)).reflectee;
          subscription.listen(
            expectAsync1((_) => null, count: expectedMax, reason: 'Received more/fewer message events than the expected 1'),
            onDone: () => expect(listeners, isEmpty,      reason: 'Listeners not removed from collection as expected'     )
          );
        });

        test('via setting limit later', () {
          int expectedMax = 1;

          // ======== Subscribe
          var subscription = Subscription(protocolHandler, faker.lorem.word());
          subscription.unsubscribe(max: expectedMax);

          // ======== Inject Some Messages
          Future(() async {
            unawaited(simulateMessage(subscription.sid, faker.lorem.sentence()));
            unawaited(simulateMessage(subscription.sid, faker.lorem.sentence())); // -- max was set to 1, so our subscription should never receive this one
          });

          // ======== Verify Results
          var listeners = reflect(subscription).getField(MirrorSystem.getSymbol('_listeners', reflectClass(Subscription).owner)).reflectee;
          subscription.listen(
            expectAsync1((_) => null, count: expectedMax, reason: 'Received more/fewer message events than the expected 1'),
            onDone: () => expect(listeners, isEmpty,      reason: 'Listeners not removed from collection as expected'     )
          );
        });
      });
    });
  });
}
