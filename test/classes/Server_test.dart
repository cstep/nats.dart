import 'package:nats_dart/nats_dart.dart';
import 'package:faker_extended/faker_extended.dart';
import 'package:test/test.dart';

void main() {
  group('can be created', () {
    String expectedHost;
    String expectedPath;
    int    expectedPort;
    Uri    expectedUri;

    setUp(() {
      expectedHost = faker.internet.uri(faker.randomGenerator.element(['http', 'https']));
      expectedPath = '/' + faker.lorem.word();
      expectedPort = faker.randomGenerator.integer(5000, min: 1000);
      expectedUri  = Uri.parse(expectedHost + expectedPath);
    });

    test('with string url', () {
      var actual = Server(expectedUri.toString());

      expect(actual,                isA<Server>(),                  reason: 'Server instance type other than expected');
      expect(actual.url,            isA<Uri>(),                     reason: 'Server url type other than expected');
      expect(actual.url.toString(), equals(expectedUri.toString()), reason: 'Server url other than expected');
    });

    test('with port arg', () {
      var actual = Server(expectedUri.toString(), port: expectedPort);

      expect(actual.url,      isA<Uri>(),           reason: 'Server url type other than expected');
      expect(actual.url.port, equals(expectedPort), reason: 'Server uri port other than expected');
    });

    test('with URI', () {
      var actual = Server(expectedUri);

      expect(actual.url, equals(expectedUri), reason: 'Server URI other than expected');
    });
  });
}
