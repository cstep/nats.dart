import 'dart:convert';

import 'package:nats_dart/nats_dart.dart';
import 'package:faker_extended/faker_extended.dart';
import 'package:test/test.dart';

void main() {
  String       expectedServerId;
  String       expectedVersion;
  String       expectedGoVersion;
  String       expectedHost;
  int          expectedPort;
  int          expectedMaxPayload;
  int          expectedProtocol;
  int          expectedClientId;
  bool         expectedAuthRequired;
  bool         expectedTlsRequired;
  bool         expectedTlsVerify;
  List<String> expectedConnectUrls;

  setUp(() {
    expectedServerId     = faker.lorem.word();
    expectedVersion      = faker.lorem.word();
    expectedGoVersion    = faker.lorem.word();
    expectedHost         = faker.lorem.word();
    expectedPort         = faker.randomGenerator.integer(5000, min: 1000);
    expectedMaxPayload   = faker.randomGenerator.integer(2^16, min: 2^8);
    expectedProtocol     = faker.randomGenerator.integer(1, min: 0);
    expectedClientId     = faker.randomGenerator.integer(2^64, min: 1);
    expectedAuthRequired = faker.randomGenerator.boolean();
    expectedTlsRequired  = faker.randomGenerator.boolean();
    expectedTlsVerify    = faker.randomGenerator.boolean();
    expectedConnectUrls  = List.generate(faker.randomGenerator.integer(10, min: 1), (_) => (faker.randomGenerator.boolean() ? faker.internet.httpUrl() : faker.internet.httpsUrl()));
  });

  group('constructor', () {
    test('normal', () {
      var actual = InfoMessageProto(
            serverId: expectedServerId,
             version: expectedVersion,
           goVersion: expectedGoVersion,
                host: expectedHost,
                port: expectedPort,
          maxPayload: expectedMaxPayload,
            protocol: expectedProtocol,
            clientId: expectedClientId,
        authRequired: expectedAuthRequired,
         tlsRequired: expectedTlsRequired,
           tlsVerify: expectedTlsVerify,
         connectUrls: expectedConnectUrls,
      );

      expect(actual,              isA<MessageProto>(),               reason: 'Proto instance inherits unexpected class'    );
      expect(actual,              isNot(isA<PayloadMessageProto>()), reason: 'Proto instance inherits unexpected class'    );
      expect(actual.messageType,  equals(MessageType.INFO),          reason: 'Proto messageType value other than expected' );
      expect(actual.serverId,     equals(expectedServerId),          reason: 'Proto serverId value other than expected'    );
      expect(actual.version,      equals(expectedVersion),           reason: 'Proto version value other than expected'     );
      expect(actual.goVersion,    equals(expectedGoVersion),         reason: 'Proto goVersion value other than expected'   );
      expect(actual.host,         equals(expectedHost),              reason: 'Proto host value other than expected'        );
      expect(actual.port,         equals(expectedPort),              reason: 'Proto port value other than expected'        );
      expect(actual.maxPayload,   equals(expectedMaxPayload),        reason: 'Proto maxPayload value other than expected'  );
      expect(actual.protocol,     equals(expectedProtocol),          reason: 'Proto protocol value other than expected'    );
      expect(actual.clientId,     equals(expectedClientId),          reason: 'Proto clientId value other than expected'    );
      expect(actual.authRequired, equals(expectedAuthRequired),      reason: 'Proto authRequired value other than expected');
      expect(actual.tlsRequired,  equals(expectedTlsRequired),       reason: 'Proto tlsRequired value other than expected' );
      expect(actual.tlsVerify,    equals(expectedTlsVerify),         reason: 'Proto tlsVerify value other than expected'   );
      expect(actual.connectUrls,  equals(expectedConnectUrls),       reason: 'Proto connectUrls value other than expected' );
    });

    test('convert null values', () {
      var actual = InfoMessageProto(
            serverId: expectedServerId,
             version: expectedVersion,
           goVersion: expectedGoVersion,
                host: expectedHost,
                port: expectedPort,
          maxPayload: expectedMaxPayload,
            protocol: expectedProtocol,
            clientId: expectedClientId,
        authRequired: null,
         tlsRequired: null,
           tlsVerify: null,
         connectUrls: expectedConnectUrls,
      );

      expectedAuthRequired = false;
      expectedTlsRequired  = false;
      expectedTlsVerify    = false;

      expect(actual.authRequired, equals(expectedAuthRequired), reason: 'Proto authRequired value other than expected');
      expect(actual.tlsRequired,  equals(expectedTlsRequired),  reason: 'Proto tlsRequired value other than expected' );
      expect(actual.tlsVerify,    equals(expectedTlsVerify),    reason: 'Proto tlsVerify value other than expected'   );
    });
  });

  group('serialization', () {
    test('without optional params', () {
      var actual = InfoMessageProto(
            serverId: expectedServerId,
             version: expectedVersion,
           goVersion: expectedGoVersion,
                host: expectedHost,
                port: expectedPort,
          maxPayload: expectedMaxPayload,
            protocol: expectedProtocol,
            clientId: expectedClientId,
        // authRequired: expectedAuthRequired,
        //  tlsRequired: expectedTlsRequired,
        //    tlsVerify: expectedTlsVerify,
        //  connectUrls: expectedConnectUrls,
      ).toProtocolString();

      var expectedJson = json.encode({
        'server_id'  : expectedServerId,
        'version'    : expectedVersion,
        'go'         : expectedGoVersion,
        'host'       : expectedHost,
        'port'       : expectedPort,
        'max_payload': expectedMaxPayload,
        'protocol'   : expectedProtocol,
        'client_id'  : expectedClientId,
      });

      expect(actual, equals('INFO ${expectedJson}'), reason: 'Protocol command other than expected');
    });

    group('with additional params', () {
      test('present, but unset', () {
        var actual = InfoMessageProto(
              serverId: expectedServerId,
               version: expectedVersion,
             goVersion: expectedGoVersion,
                  host: expectedHost,
                  port: expectedPort,
            maxPayload: expectedMaxPayload,
              protocol: expectedProtocol,
              clientId: expectedClientId,
          authRequired: false,
           tlsRequired: false,
             tlsVerify: false,
           connectUrls: [],
        ).toProtocolString();

        var expectedJson = json.encode({
          'server_id'  : expectedServerId,
          'version'    : expectedVersion,
          'go'         : expectedGoVersion,
          'host'       : expectedHost,
          'port'       : expectedPort,
          'max_payload': expectedMaxPayload,
          'protocol'   : expectedProtocol,
          'client_id'  : expectedClientId,
        });

        expect(actual, equals('INFO ${expectedJson}'), reason: 'Protocol command other than expected');
      });

      test('present, and set', () {
        var actual = InfoMessageProto(
              serverId: expectedServerId,
               version: expectedVersion,
             goVersion: expectedGoVersion,
                  host: expectedHost,
                  port: expectedPort,
            maxPayload: expectedMaxPayload,
              protocol: expectedProtocol,
              clientId: expectedClientId,
          authRequired: true,
           tlsRequired: true,
             tlsVerify: true,
           connectUrls: expectedConnectUrls,
        ).toProtocolString();

        var expectedJson = json.encode({
          'server_id'    : expectedServerId,
          'version'      : expectedVersion,
          'go'           : expectedGoVersion,
          'host'         : expectedHost,
          'port'         : expectedPort,
          'max_payload'  : expectedMaxPayload,
          'protocol'     : expectedProtocol,
          'client_id'    : expectedClientId,
          'auth_required': true,
          'tls_required' : true,
          'tls_verify'   : true,
          'connect_urls' : expectedConnectUrls,
        });

        expect(actual, equals('INFO ${expectedJson}'), reason: 'Protocol command other than expected');
      });
    });
  });
}
