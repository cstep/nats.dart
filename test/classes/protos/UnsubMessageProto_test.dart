import 'package:nats_dart/nats_dart.dart';
import 'package:faker_extended/faker_extended.dart';
import 'package:test/test.dart';

void main() {
  String expectedSid;
  int    expectedMax;

  setUp(() {
    expectedSid = faker.lorem.word();
    expectedMax = faker.randomGenerator.integer(5, min: 1);
  });

  group('can be created', () {
    test('with required args only', () {
      var actual = UnsubMessageProto(expectedSid);

      expect(actual,             isA<MessageProto>(),               reason: 'Proto instance inherits unexpected class'   );
      expect(actual,             isNot(isA<PayloadMessageProto>()), reason: 'Proto instance inherits unexpected class'   );
      expect(actual.messageType, equals(MessageType.UNSUB),         reason: 'Proto messageType value other than expected');
      expect(actual.sid,         equals(expectedSid),               reason: 'Proto sid value other than expected'        );
    });

    test('with optional args too', () {
      var actual = UnsubMessageProto(expectedSid, max: expectedMax);

      expect(actual,             isA<MessageProto>(),               reason: 'Proto instance inherits unexpected class'   );
      expect(actual,             isNot(isA<PayloadMessageProto>()), reason: 'Proto instance inherits unexpected class'   );
      expect(actual.messageType, equals(MessageType.UNSUB),         reason: 'Proto messageType value other than expected');
      expect(actual.sid,         equals(expectedSid),               reason: 'Proto sid value other than expected'        );
      expect(actual.max,         equals(expectedMax),               reason: 'Proto max value other than expected'      );
    });
  });

  group('serialization', () {
    test('with required args only', () {
      var actual = UnsubMessageProto(expectedSid).toProtocolString();

      expect(actual, equals('UNSUB ${expectedSid}'), reason: 'Protocol command other than expected');
    });

    test('with optional args too', () {
      var actual = UnsubMessageProto(expectedSid, max: expectedMax).toProtocolString();

      expect(actual, equals('UNSUB ${expectedSid} ${expectedMax}'), reason: 'Protocol command other than expected');
    });
  });
}
