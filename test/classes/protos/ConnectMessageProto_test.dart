import 'package:nats_dart/nats_dart.dart';
import 'package:test/test.dart';
import '../../mocks/ServerOptionsMock.dart';

void main() {
  test('constructor', () {
    var expectedOptions = ServerOptionsMock();
    var actual          = ConnectMessageProto(expectedOptions);

    expect(actual,             isA<MessageProto>(),               reason: 'Proto instance type other than expected');
    expect(actual,             isNot(isA<PayloadMessageProto>()), reason: 'Proto instance inherits unexpected class');
    expect(actual.messageType, equals(MessageType.CONNECT),       reason: 'Proto messageType value other than expected');
    expect(actual.options,     isA<ServerOptions>(),              reason: 'Proto options type other than expected' );
    expect(actual.options,     equals(expectedOptions),           reason: 'Proto options value other than expected');
  });

  test('serialization', () {
    var expectedOptions = ServerOptionsMock();
    var actual          = ConnectMessageProto(expectedOptions).toProtocolString();

    expect(actual, equals('CONNECT {optionsJson}'), reason: 'Protocol command other than expected');
  });
}
