import 'package:nats_dart/nats_dart.dart';
import 'package:test/test.dart';

void main() {
  test('constructor', () {
    var actual = OkMessageProto();

    expect(actual,             isA<MessageProto>(),               reason: 'Proto instance inherits unexpected class'    );
    expect(actual,             isNot(isA<PayloadMessageProto>()), reason: 'Proto instance inherits unexpected class'    );
    expect(actual.messageType, equals(MessageType.OK),            reason: 'Proto messageType value other than expected' );
  });

  test('serialization', () {
    var actual = OkMessageProto().toProtocolString();

    expect(actual, equals('+OK'), reason: 'Protocol command other than expected');
  });
}
