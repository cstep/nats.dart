import 'package:nats_dart/nats_dart.dart';
import 'package:faker_extended/faker_extended.dart';
import 'package:test/test.dart';

void main() {
  String expectedSubject;
  int    expectedByteLength;
  String expectedReplySubject;

  setUp(() {
    expectedSubject      = faker.lorem.word();
    expectedByteLength   = faker.randomGenerator.integer(1024, min: 8);
    expectedReplySubject = faker.lorem.word();
  });

  group('can be created', () {
    test('with required args only', () {
      var actual = PubMessageProto(expectedSubject, expectedByteLength);

      expect(actual,              isA<PayloadMessageProto>(),  reason: 'Proto instance inherits unexpected class'    );
      expect(actual.messageType,  equals(MessageType.PUB),     reason: 'Proto messageType value other than expected' );
      expect(actual.subject,      equals(expectedSubject),     reason: 'Proto subject value other than expected'     );
      expect(actual.byteLength,   equals(expectedByteLength),  reason: 'Proto byteLength value other than expected'  );
      expect(actual.replySubject, isNull,                      reason: 'Proto replySubject value other than expected');
    });

    test('with optional args too', () {
      var actual = PubMessageProto(expectedSubject, expectedByteLength, replySubject: expectedReplySubject);

      expect(actual,              isA<PayloadMessageProto>(),   reason: 'Proto instance inherits unexpected class'    );
      expect(actual.messageType,  equals(MessageType.PUB),      reason: 'Proto messageType value other than expected' );
      expect(actual.subject,      equals(expectedSubject),      reason: 'Proto subject value other than expected'     );
      expect(actual.byteLength,   equals(expectedByteLength),   reason: 'Proto byteLength value other than expected'  );
      expect(actual.replySubject, equals(expectedReplySubject), reason: 'Proto replySubject value other than expected');
    });
  });

  group('serialization', () {
    test('with required args only', () {
      var actual = PubMessageProto(expectedSubject, expectedByteLength).toProtocolString();

      expect(actual, equals('PUB ${expectedSubject} ${expectedByteLength}'), reason: 'Protocol command other than expected');
    });

    test('with optional args too', () {
      var actual = PubMessageProto(expectedSubject, expectedByteLength, replySubject: expectedReplySubject).toProtocolString();

      expect(actual, equals('PUB ${expectedSubject} ${expectedReplySubject} ${expectedByteLength}'), reason: 'Protocol command other than expected');
    });
  });
}
