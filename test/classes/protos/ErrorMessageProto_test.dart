import 'package:nats_dart/nats_dart.dart';
import 'package:faker_extended/faker_extended.dart';
import 'package:test/test.dart';

void main() {
  test('constructor', () {
    var expectedMessage = faker.lorem.sentence();
    var actual          = ErrorMessageProto(expectedMessage);

    expect(actual,             isA<MessageProto>(),               reason: 'Proto instance inherits unexpected class'    );
    expect(actual,             isNot(isA<PayloadMessageProto>()), reason: 'Proto instance inherits unexpected class'    );
    expect(actual.messageType, equals(MessageType.ERROR),         reason: 'Proto messageType value other than expected' );
    expect(actual.message,     equals(expectedMessage),           reason: 'Proto message value other than expected'     );
  });

  test('serialization', () {
    var expectedMessage = faker.lorem.sentence();
    var actual          = ErrorMessageProto(expectedMessage).toProtocolString();

    expect(actual, equals('-ERR ${expectedMessage}'), reason: 'Protocol command other than expected');
  });
}
