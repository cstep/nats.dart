import 'package:nats_dart/nats_dart.dart';
import 'package:test/test.dart';

void main() {
  test('constructor', () {
    var actual = PingMessageProto();

    expect(actual,             isA<MessageProto>(),               reason: 'Proto instance inherits unexpected class'    );
    expect(actual,             isNot(isA<PayloadMessageProto>()), reason: 'Proto instance inherits unexpected class'    );
    expect(actual.messageType, equals(MessageType.PING),          reason: 'Proto messageType value other than expected' );
  });

  test('serialization', () {
    var actual = PingMessageProto().toProtocolString();

    expect(actual, equals('PING'), reason: 'Protocol command other than expected');
  });
}
