import 'package:nats_dart/nats_dart.dart';
import 'package:faker_extended/faker_extended.dart';
import 'package:test/test.dart';

void main() {
  String expectedSubject;
  String expectedSid;
  String expectedQueueName;

  setUp(() {
    expectedSubject   = faker.lorem.word();
    expectedSid       = faker.lorem.word();
    expectedQueueName = faker.lorem.word();
  });

  group('can be created', () {
    test('with required args only', () {
      var actual = SubMessageProto(expectedSubject, expectedSid);

      expect(actual,              isA<MessageProto>(),               reason: 'Proto instance inherits unexpected class'   );
      expect(actual,              isNot(isA<PayloadMessageProto>()), reason: 'Proto instance inherits unexpected class'   );
      expect(actual.messageType,  equals(MessageType.SUB),           reason: 'Proto messageType value other than expected');
      expect(actual.subject,      equals(expectedSubject),           reason: 'Proto subject value other than expected'    );
      expect(actual.sid,          equals(expectedSid),               reason: 'Proto sid value other than expected'        );
    });

    test('with optional args too', () {
      var actual = SubMessageProto(expectedSubject, expectedSid, queue: expectedQueueName);

      expect(actual,              isA<MessageProto>(),               reason: 'Proto instance inherits unexpected class'   );
      expect(actual,              isNot(isA<PayloadMessageProto>()), reason: 'Proto instance inherits unexpected class'   );
      expect(actual.messageType,  equals(MessageType.SUB),           reason: 'Proto messageType value other than expected');
      expect(actual.subject,      equals(expectedSubject),           reason: 'Proto subject value other than expected'    );
      expect(actual.sid,          equals(expectedSid),               reason: 'Proto sid value other than expected'        );
      expect(actual.queue,        equals(expectedQueueName),         reason: 'Proto queue value other than expected'      );
    });
  });

  group('serialization', () {
    test('with required args only', () {
      var actual = SubMessageProto(expectedSubject, expectedSid).toProtocolString();

      expect(actual, equals('SUB ${expectedSubject} ${expectedSid}'), reason: 'Protocol command other than expected');
    });

    test('with optional args too', () {
      var actual = SubMessageProto(expectedSubject, expectedSid, queue: expectedQueueName).toProtocolString();

      expect(actual, equals('SUB ${expectedSubject} ${expectedQueueName} ${expectedSid}'), reason: 'Protocol command other than expected');
    });
  });
}
