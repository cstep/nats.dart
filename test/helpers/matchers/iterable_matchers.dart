import 'package:matcher/matcher.dart';
import 'package:matcher/src/feature_matcher.dart';

abstract class _IterableMatcher extends FeatureMatcher<Iterable> {
  const _IterableMatcher();
}

Matcher startsWithInOrder(Iterable expected) => _StartsWithInOrder(expected);

class _StartsWithInOrder extends _IterableMatcher {
  final Iterable _expected;
  final Matcher  _matcher;

  _StartsWithInOrder(this._expected): _matcher = orderedEquals(_expected);

  @override
  bool typedMatches(Iterable item, Map matchState) => _matcher.matches(item.take(_expected.length), matchState);

  @override
  Description describe(Description description) => description.add('starts with ').addDescriptionOf(_expected).add(' in order');

  @override
  Description describeTypedMismatch(Iterable item, Description mismatchDescription, Map matchState, bool verbose) {
    return _matcher.describeMismatch(item, mismatchDescription, matchState, verbose);
  }
}

Matcher startsWithUnordered(Iterable expected) => _StartsWithUnordered(expected);

class _StartsWithUnordered extends _IterableMatcher {
  final Iterable _expected;
  final Matcher  _matcher;

  _StartsWithUnordered(this._expected): _matcher = unorderedEquals(_expected);

  @override
  bool typedMatches(Iterable item, Map matchState) => _matcher.matches(item.take(_expected.length), matchState);

  @override
  Description describe(Description description) => description.add('starts with ').addDescriptionOf(_expected).add(' in any order');

  @override
  Description describeTypedMismatch(Iterable item, Description mismatchDescription, Map matchState, bool verbose) {
    return _matcher.describeMismatch(item, mismatchDescription, matchState, verbose);
  }
}
