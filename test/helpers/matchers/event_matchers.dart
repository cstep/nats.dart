import 'package:nats_dart/nats_dart.dart';
import 'package:matcher/matcher.dart';

Matcher matchesSid<T>(dynamic expectedSid) => isA<T>().having(_resolveSid, 'SID', equals(_resolveSid(expectedSid)));

String _resolveSid(dynamic value) {
  if (value is String) {
    return value;
  } else if (value is num) {
    return '$value';
  } else if (value is Subscription || value is DataMessage || value is SubMessage || value is UnsubMessage) {
    return value.sid;
  } else if (value is SubscriptionEvent) {
    return value.subscription.sid;
  } else {
    throw ArgumentError.value(value, '', 'MatchesSid() can only match against an instance of String|Subscription|DataMessage|SubMessage|UnsubMessage|SubscriptionEvent');
  }
}
