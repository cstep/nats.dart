import 'dart:io';
import 'package:test/test.dart' as test_core;
import '../mocks/SocketMock.dart';

export 'package:test/test.dart' hide group, test, setUp, tearDown, setUpAll, tearDownAll;

Future<SocketMock> socketConnectOverride(dynamic host, int port, {dynamic sourceAddress, Duration timeout}) {
  return Future.value(SocketMock.connect());
}

Future overrideZoned(void Function() callback) {
  return IOOverrides.runZoned(callback, socketConnect: socketConnectOverride); // -- system-level overrides/mocks
}

// ignore: provide_deprecation_message
void group(dynamic description, Function() body, {String testOn, test_core.Timeout timeout, dynamic skip, dynamic tags, Map<String, dynamic> onPlatform, int retry, @deprecated bool solo = false}) {
  // ignore: deprecated_member_use
  return test_core.group(description, () => overrideZoned(body), testOn: testOn, timeout: timeout, skip: skip, tags: tags, onPlatform: onPlatform, retry: retry, solo: solo);
}

// ignore: provide_deprecation_message
void test(dynamic description, Function() body, {String testOn, test_core.Timeout timeout, dynamic skip, dynamic tags, Map<String, dynamic> onPlatform, int retry, @deprecated bool solo = false}) {
  // ignore: deprecated_member_use
  return test_core.test(description, () => overrideZoned(body), testOn: testOn, timeout: timeout, skip: skip, onPlatform: onPlatform, tags: tags, retry: retry, solo: solo);
}

void setUp(      Function() callback) => test_core.setUp(() => overrideZoned(callback)      );
void tearDown(   Function() callback) => test_core.tearDown(() => overrideZoned(callback)   );
void setUpAll(   Function() callback) => test_core.setUpAll(() => overrideZoned(callback)   );
void tearDownAll(Function() callback) => test_core.tearDownAll(() => overrideZoned(callback));
