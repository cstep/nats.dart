. as $parent |
[$parent[] | select(type=="object" and has("test") and (.test.name|(contains("loading")|not)))] as $tests |
[$parent[] | select(type=="object" and has("testID") and .hidden? != true)] as $results |
{
	total  : $tests|length,
	passed : [$results[]|select(.result == "success" and .skipped? != true )]|length,
	skipped: [$results[]|select(.result == "success" and .skipped? != false)]|length,
	failed : [$results[]|select(.isFailure?)]|length,
	reasons: [$results[]|select(.isFailure? or .skipped?)]|
		map(. as $failure | .testID as $id | ($tests[] | select(type=="object" and .test?.id == $id) | map(.))[0] |
			{
				name: .name,
				error: ($failure.error // "skipped")
			}
		)
}