![Bitbucket Pipelines branch](https://img.shields.io/bitbucket/pipelines/cstep/nats.dart/develop?label=build)
![Codecov](https://img.shields.io/codecov/c/bitbucket/cstep/nats.dart/develop?label=coverage)
![Dart Version](https://img.shields.io/badge/dart-%5E2.5-blue)
![License](https://img.shields.io/badge/license-Apache%202-blue)

# nats.dart
A Dart client for [NATS](https://nats.io), a lightweight, high-performance cloud native messaging system

---

## Note: this package is still in pre-release and not all features/functionalities are yet implemented.

## Usage
```dart
// create a connection
var client = await Client.connectSingle(natsHost, username: natsUsername, password: natsPassword);

// subscribe to a topic
client.subscribe('myTopic', handler: (DataMessage message, {NatsError error}) {
  log.general.info('ooh, looky! A message for meeeeeee! ::: ${message.decodedPayload}');
});

// publish a message
await client.publish('myTopic', payload: 'this is an event on topic: myTopic')); // -- let the client encode it for us (with the default encoder (ascii))

// publish a message with a custom encoding
await client.publish('myTopic', payload: 'this is another event on topic: myTopic'), encoding: utf8); // -- specify our own encoding

// publish a message of raw bytes
var alreadyEncodedMessageBytes = ascii.encode('this is a pre-encoded event on topic: myTopic');
await client.publish('myTopic', payload: alreadyEncodedMessageBytes, encoding: null); // -- passing a null-encoding tells the client encoding has already been done
```

See [example files](./example/README.md) for more awesomeness.

---

## Note: this package is still in pre-release and not all features/functionalities are yet implemented.

### Contributors and PRs welcome!
