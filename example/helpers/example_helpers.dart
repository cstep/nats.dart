import 'dart:async';
import 'dart:collection';
import 'dart:io';
import 'package:nats_dart/nats_dart.dart';
import 'package:quiver/collection.dart';
import 'package:logging/logging.dart';
import 'package:quiver_log/log.dart';
import 'package:intl/intl.dart';

String natsHost     = Platform.environment['NATS_HOST']??'localhost';
int    natsPort     = int.parse(Platform.environment['NATS_PORT']??'4222');
String natsUsername = Platform.environment['NATS_USERNAME'];
String natsPassword = Platform.environment['NATS_PASSWORD'];

ColorSet defaultColorSet  = ColorSet(  0, 255, 255, 255);
ColorSet producerColorSet = ColorSet(127, 200, 199, 198);
ColorSet consumerColorSet = ColorSet( 22,  84,  83,  82);

String _colorize(String string, {Symbol color = #DEFAULT_COLOR}) => '\u001B[38;5;${(Zone.current['colorSet']??defaultColorSet)[color]}m$string\u001B[0m';
String     _bold(String string)                                  => '\u001B[1m$string\u001B[21;24m';
String   _invert(String string)                                  => '\u001B[7m$string\u001B[27m';
String      _dim(String string)                                  => '\u001B[2m$string\u001B[22m';

class ColorSet extends DelegatingMap {
  @override final UnmodifiableMapView<Symbol, int> delegate;

  static const Symbol DEFAULT  = #DEFAULT_COLOR;
  static const Symbol OUTBOUND = #OUTBOUND_COLOR;
  static const Symbol INBOUND  = #INBOUND_COLOR;
  static const Symbol HANDLER  = #HANDLER_COLOR;

  ColorSet(int dfault, int outbound, int inbound, int handler): delegate = UnmodifiableMapView({
    DEFAULT : dfault,
    OUTBOUND: outbound,
    INBOUND : inbound,
    HANDLER : handler,
  });
}

class ZoneColorFormatter extends BasicLogFormatter {
  static final DateFormat _dateFormat = DateFormat('HH:mm:ss.SSS');

  const ZoneColorFormatter();

  @override
  String call(LogRecord record) {
    var color  = ColorSet.DEFAULT;
    if (record.loggerName == 'nats' || record.loggerName == 'nats.general') {
      if (record.level >= Level.INFO) {
        color  = ColorSet.HANDLER;
      }
    } else {
      if (record.loggerName == 'nats.incoming') {
        color = ColorSet.INBOUND;
      } else if (record.loggerName == 'nats.outgoing') {
        color = ColorSet.OUTBOUND;
      }
    }

    var message = record.zone.run(() => _colorize(
      '${_bold(
        '${_dateFormat.format(record.time)} '
        '${record.level.toString().padLeft(7, ' ')} '
        '${((record.zone['name'] as String)??'').trim().isNotEmpty ? '${record.zone['name']} ' : ''}'
        '| '
      )}'
      '${color == ColorSet.HANDLER ? _invert(_bold(record.message)) : _dim(record.message)}', color: color)
    );

    if (record.error != null) {
      message += ', error: ${record.error}';
    }

    if (record.stackTrace != null) {
      message += ', stackTrace: ${record.stackTrace}';
    }

    return message;
  }
}

void setupLogger() {
  Logger.root.level = Level.FINEST;
  PrintAppender(ZoneColorFormatter()).attachLogger(Logger.root);

//  log.incoming.level = Level.FINEST;
//  log.outgoing.level = Level.FINEST;
}

void withConsumerColorSet(void Function(Client) callback) => runZoned(() async {
  var client = await (Client.connectSingle(natsHost, port: natsPort, username: natsUsername, password: natsPassword, serverOptions: ServerOptions(name: 'consumer'))
      .then((Client client) { log.general.config('NATS Consumer connected'); return client; }));

  Zone.current.runUnary(callback, client);
}, zoneValues: {'name': 'consumer', 'colorSet': consumerColorSet});

void withProducerColorSet(void Function(Client) callback) => runZoned(() async {
  var client = await (Client.connectSingle(natsHost, port: natsPort, username: natsUsername, password: natsPassword, serverOptions: ServerOptions(name: 'producer'))
      .then((Client client) { log.general.config('NATS Producer connected'); return client; }));

  Zone.current.runUnary(callback, client);
}, zoneValues: {'name': 'producer', 'colorSet': producerColorSet});
