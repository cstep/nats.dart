import 'package:nats_dart/nats_dart.dart';
import 'helpers/example_helpers.dart';

void _main() {
  // ======== Our Consumer
  withConsumerColorSet((consumer) async {
    // -------- listen for and reply to incoming messages
    consumer.subscribe('myTopic', handler: (DataMessage message, {NatsError error}) async {
      log.general.info('Ooh! Someone\'s talking to me! ::: ${message.decodedPayload} ::: replyTo: ${message.replySubject}');

      // -------- send a reply
      await consumer.request(message.replySubject, payload: 'Oh. What seems to be the problem, officer?')
        .then((DataMessage reply) {
          log.general.info('He\'s still talking to me! ::: ${reply.decodedPayload}');

          // -------- send another reply
          consumer.request(reply.replySubject, payload: 'Oh, you like me! Will you be my friend!?', timeout: Duration(seconds: 3),
            onTimeout: (_) => log.general.info('Damn... ignored again :('));
        });
    });
  });

  // ======== Our Producer
  withProducerColorSet((producer) async {
    // -------- will publish a request and wait for a reply
    await producer.request('myTopic', payload: 'License and registration, please!')
      .then((DataMessage reply) async {
        log.general.info('OMG, A REPLY!!! ::: ${reply.decodedPayload}');

        // -------- send a reply
        await producer.request(reply.replySubject, payload: 'you were simply being *too* fabulous') // this 2nd reply will be ignored by original publisher since it already received a reply
          .then((DataMessage reply) async {
            log.general.info('omg, they\'re still talking to me? ::: ${reply.decodedPayload}');
          });
      });
  });
}

void main() {
  setupLogger();
  _main();
}
