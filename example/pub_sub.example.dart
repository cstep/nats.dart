import 'dart:async';
import 'dart:convert';
import 'package:nats_dart/nats_dart.dart';
import 'helpers/example_helpers.dart';

int maxMessages = 5; // -- 0 = infinite

void _main() {
  log.general.config('starting up...');

  // ======== Consumer
  withConsumerColorSet((consumer) async { // -- this wrapper is just to help with colorizing our output
    consumer.subscribe('myTopic', handler: (DataMessage message, {NatsError error}) =>
      log.general.info('ooh, looky! I\'ve got mail! ::: ${message.decodedPayload}'));
  });

  // ======== Producer
  withProducerColorSet((producer) async { // -- this wrapper is just to help with colorizing our output
    var counter = 0;
    await Future.delayed(Duration(seconds: 0), () => producer.publish('myTopic', payload: 'this is an event on topic: myTopic')); // -- let the client encode it for us (with the default encoder (ascii))
    counter++;

    Timer.periodic(Duration(seconds: 1), (timer) {
      producer.publish('myTopic', payload: ascii.encode('this is another event (#${++counter}) on topic: myTopic'), encoding: null);
      if ((maxMessages??0) > 0 && counter >= maxMessages) {
        timer.cancel();
      }
    }); // -- encode it ourselves and tell the client not to worry about it
  });
}

void main() {
  setupLogger();
  _main();
}
