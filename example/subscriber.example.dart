import 'package:nats_dart/nats_dart.dart';
import 'helpers/example_helpers.dart';

void _main() {
  log.general.config('starting up...');

  // ======== Consumer
  withConsumerColorSet((client) async { // -- this wrapper is just to help with colorizing our output
    Subscription subscription;
    subscription = client.subscribe('myTopic', handler: (DataMessage message, {NatsError error}) =>
      log.general.info('Message (#${subscription.receivedCount}) Received! ::: ${message.decodedPayload}'));
  });
}

void main() {
  setupLogger();
  _main();
}
