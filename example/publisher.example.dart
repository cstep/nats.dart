import 'dart:async';
import 'package:nats_dart/nats_dart.dart';
import 'helpers/example_helpers.dart';

int maxMessages = 0; // -- 0 = infinite

void _main() {
  log.general.config('starting up...');

  withProducerColorSet((producer) async { // -- this wrapper is just to help with colorizing our output
    var counter = 0;
    Timer.periodic(Duration(seconds: 1), (timer) {
      producer.publish('myTopic', payload: 'this is an event (#${++counter}) on topic: myTopic');
      if ((maxMessages??0) > 0 && counter >= maxMessages) {
        timer.cancel();
      }
    }); // -- encode it ourselves and tell the client not to worry about it
  });
}

void main() {
  setupLogger();
  _main();
}
