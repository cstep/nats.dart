part of nats_dart;

Stream<T> interleave<T>(Iterable<Stream<T>> streams) {
  var liveStreams = streams.length;
  var controller = StreamController<T>();
  for(var stream in streams) {
    stream.listen(controller.add, onError: controller.addError, onDone: () { liveStreams--; if (liveStreams <= 0) controller.close(); });
  }
  return controller.stream;
}

Stream<T> asNonBroadcastStream<T>(Stream<T> stream) => (StreamController<T>()..addStream(stream)).stream;
