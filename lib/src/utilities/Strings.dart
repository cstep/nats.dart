part of nats_dart;

// TODO: the following function may be removed once it's merged into the dart/quiver package and released. @see PR https://github.com/google/quiver-dart/pull/513
String ifBlank(String test, String dfault) {
  return (isBlank(test) ? dfault : test);
}

// TODO: the following function may be removed once it's merged into the dart/quiver package and released. @see PR https://github.com/google/quiver-dart/pull/513
String ifEmpty(String test, String dfault) {
  return (isEmpty(test) ? dfault : test);
}
