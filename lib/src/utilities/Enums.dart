part of nats_dart;

class Enum {
  static final Map<Type, Map> _registrations = {
    MessageType: {
      InfoMessageProto.control   : MessageType.INFO,
      ConnectMessageProto.control: MessageType.CONNECT,
      PubMessageProto.control    : MessageType.PUB,
      SubMessageProto.control    : MessageType.SUB,
      UnsubMessageProto.control  : MessageType.UNSUB,
      DataMessageProto.control   : MessageType.MESSAGE,
      'DATA'/*alias*/            : MessageType.MESSAGE,
      PingMessageProto.control   : MessageType.PING,
      PongMessageProto.control   : MessageType.PONG,
      OkMessageProto.control     : MessageType.OK,
      'OK'/*alias*/              : MessageType.OK,
      ErrorMessageProto.control  : MessageType.ERROR,
      'ERROR'/*alias*/           : MessageType.ERROR,
    }
  };

  static E fromString<E>(String string, {bool failOnError = false}) {
    string = string.toUpperCase();

    if (!_registrations.containsKey(E)) {
      return (!failOnError ? null : throw ArgumentError('${E} has no registered string values'));
    }

    var aliasRegistry = _registrations[E];
    if (!aliasRegistry.containsKey(string)) {
      return (!failOnError ? null : throw ArgumentError('ErrorType `$string` is an invalid MessagType'));
    }

    return aliasRegistry[string];
  }

  static String asString(dynamic enumItem) {
    if (enumItem == null) {
      return null;
    }
    // else

    return enumItem.toString().split('.')[1];
  }
}
