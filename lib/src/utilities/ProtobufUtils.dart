import 'dart:convert';
import 'package:fixnum/fixnum.dart';
import 'package:protobuf/protobuf.dart';

abstract class ProtobufUtils {
  static Map<String, dynamic> messageToMap(GeneratedMessage message, {bool onlyPresent = true, Encoding encoding}) {
    if (message == null) {
      return null;
    }

    Map<String, dynamic> namedValues = {};
    Iterable<FieldInfo>  fields      = message.info_.fieldInfo.values;

    for (var field in fields) {
      if (onlyPresent == true && !message.hasField(field.tagNumber)) {
        continue;
      }
      // else

      var value = message.getFieldOrNull(field.tagNumber);
      if (value is Iterable) {
        if (value is List<int>) {
          if (encoding != null) {
            value = encoding.decode(value);
          } else {
            value = '${value.length} bytes';
          }
        } else {
          value = (value as Iterable).toList();
        }
      } else if (value is Int64 || value is Int32) {
        value = value.toInt();
      } else if (value is ProtobufEnum) {
        value = value.name;
      }

      namedValues[field.name] = value;
    }

    return namedValues;
  }
}
