part of nats_dart;

// for debugging failed tests
void printEvent(Event event, {int color}) {
  color ??= 0;

  String message;
       if (event is Message)         message = event.toProtocolString();             // ignore: curly_braces_in_flow_control_structures
  else if (event is ConnectionEvent) message = '${event.runtimeType}: ${event.url}'; // ignore: curly_braces_in_flow_control_structures
  else                               message = event.toString();                     // ignore: curly_braces_in_flow_control_structures

  if (event is DataMessage      ) message += ' sid: ${event.sid}';
  if (event is SubscriptionEvent) message += ' sid: ${event.subscription.sid}';

  print('\u001B[${color}m${DateTime.now()} => \u001B[1m$message\u001B[0m');
}
