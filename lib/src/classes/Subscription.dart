part of nats_dart;

class Subscription {
  final ProtocolHandler _protocolHandler;

  final String subject;
  final String queue;
  final String sid;

  int      _max;
  int  get  max => _max??0;
  void set  max(int value) {
    if (value != _max) { // -- value is changing
      unsubscribe(max: value);
    }
  }

  int     _receivedCount =  0;
  int get  receivedCount => _receivedCount; // -- read-only

  bool _doClose   = false;
  bool _isClosing = false;

  final List<StreamSubscription> _listeners = [];

  Stream<DataMessage>     _stream;
  Stream<DataMessage> get stream => _stream;

  Subscription(this._protocolHandler, this.subject, {this.queue, int max, String sid}): this.sid = ifBlank(sid, Uuid().v4()) {
    this.max = max;

    // NOTE: for broadcast streams (which our protocolHandler uses), stream.where() is called once for each listener that might be on the stream
    //       since we only want this filter to run ONCE per event (regardless of how many listeners we have), we must:
    //       1. convert it to a regular, non-broadcast stream
    //       2. filter it
    //       3. convert it back into a broadcast stream
    _stream =
      asNonBroadcastStream(_protocolHandler.on<DataMessage>())
      .where((message) {
        var isRelevant = (!_doClose && !_isClosing && (message.sid == this.sid));
        if (isRelevant) {
          _receivedCount++;
          if ((_max??0) > 0 && _receivedCount >= _max && !_isClosing) {
            _doClose = true;
            Future.microtask(() { // -- microtask() ensures that we dont unsubscribe until the next runloop (so we don't cancel our listeners before they handle this last event)
              unsubscribe(max: _max);
            });
          }
        }

        return isRelevant;
      })
      .asBroadcastStream();
  }

  StreamSubscription<DataMessage> listen(void Function(DataMessage event) onData, {Function onError, void Function() onDone, bool cancelOnError}) {
    var listener = _stream.listen(onData, onError: onError, onDone: onDone, cancelOnError: cancelOnError);
    _listeners.add(listener);

    return listener;
  }

  Future<void> unsubscribe({int max}) async {
    this._max = (max ??= 0);

    List<Future> cleanupPromises = [];
    if ((this._receivedCount >= this._max || _doClose) && !_isClosing) {
      _isClosing = true;
      cleanupPromises = [
        ..._listeners.map((StreamSubscription listener) => listener.cancel().then((_) => _listeners.remove(listener))),
      ];
      await Future.wait(cleanupPromises);
    }

    return _protocolHandler.unsubscribe(this, max: this._max);
  }

  Stream<S> transform<S>(StreamTransformer<DataMessage, S> streamTransformer) {
    return streamTransformer.bind(this._stream);
  }

  Stream<String> get payloadStream {
    return this.transform(StreamTransformer<DataMessage, String>.fromHandlers(
      handleData : (DataMessage event, sink) => sink.add(event.decodedPayload),
      handleError: (e, s, sink)              => sink.addError(e, s),
      handleDone : (sink)                    => sink.close()
    ));
  }
}
