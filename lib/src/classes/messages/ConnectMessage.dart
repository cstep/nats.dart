part of nats_dart;

class ConnectMessage extends OutgoingMessage {
  @override final ConnectMessageProto proto;

  ServerOptions  get options                      => proto.options;
  void           set options(ServerOptions value) => proto.options = value;

  ConnectMessage(ServerOptions options): proto = _protoFactory.connect(options);
}
