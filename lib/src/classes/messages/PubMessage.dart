part of nats_dart;

/// allows dependency injection via our ioc container
class PubMessage extends PayloadMessage implements OutgoingMessage {
  @override covariant PubMessageProto proto;

  @override final dynamic  payload;
  @override final Encoding encoding;

  /// @param [encoding] set to null to treat payload as already-encoded
  PubMessage(String subject, this.payload, {this.encoding = ascii, String replySubject}) {
    proto = _protoFactory.pub(subject, encodedPayload.length, replySubject: replySubject);
  }
}
