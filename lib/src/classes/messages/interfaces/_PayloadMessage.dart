part of nats_dart;

abstract class PayloadMessage extends Message {
  @override covariant PayloadMessageProto proto;

  Encoding get encoding;
  dynamic  get payload;

  String get subject      => proto.subject;
  String get replySubject => proto.replySubject;
  int    get byteLength   => proto.byteLength;

  PayloadMessage() {
    _checkPayloadEncoding(payload, encoding);
  }

  List<int>     _encodedPayload;
  Uint8List get  encodedPayload {
    if (_encodedPayload == null) { // -- needs encoding for first time
      if (_isAlreadyEncoded(payload, encoding)) { // -- no encoding needed
        _encodedPayload = payload;
      } else { // -- encoding needed
        _encodedPayload = encoding.encode('$payload');
      }
    }

    return _encodedPayload;
  }

  String get decodedPayload {
    if (encoding == null) { // -- never encoded to begin with OR pre-encoded and we don't know which codec was used
      if (payload is String) {
        return payload;
      } else {
        log.general.fine("${this.runtimeType}.decodedPayload getter: Payload was supplied already-encoded and without a codec. We don't know how to decode this payload. If you want the raw payload as-supplied, use `${this.runtimeType}.payload` instead.${this is DataMessage ? ' (Message subjectId: ${(this as DataMessage).sid})' : ''}");
        return 'Pre-encoded: ${payload.length} bytes';
      }
    } else { // -- we know our codec
      return encoding.decode(encodedPayload);
    }
  }

  // --

  static bool _isAlreadyEncoded(dynamic payload, Encoding encoding) {
    return ((encoding == null && payload is List<int>) || payload is Uint8List);
  }

  static void _checkPayloadEncoding(dynamic payload, Encoding encoding) {
    if(encoding == null && payload is! List<int>) {
      throw ArgumentError('If encoding is null, then payload must be instance of List<int>');
    }
  }
}
