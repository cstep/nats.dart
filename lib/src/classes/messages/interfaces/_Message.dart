part of nats_dart;

abstract class Message<E extends MessageProto> extends MessageEvent {
  MessageProto proto;

  String toProtocolString() => proto.toProtocolString();
}
