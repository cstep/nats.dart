part of nats_dart;

class PongMessage extends PresenceMessage {
  @override final PongMessageProto proto;

  PongMessage(): this.fromProto(_protoFactory.pong());

  PongMessage.fromProto(this.proto);
}
