part of nats_dart;

class UnsubMessage extends OutgoingMessage {
  @override final UnsubMessageProto proto;

  String get sid               => proto.sid;
  void   set sid(String value) => proto.sid = value;

  int    get max            => proto.max;
  void   set max(int value) => proto.max = value;

  UnsubMessage(String sid, {int max}): proto = _protoFactory.unsub(sid, max: max);

  UnsubMessage.fromSubscription(Subscription subscription, {int max}): this(subscription.sid, max: max);
}
