part of nats_dart;

class SubMessage extends OutgoingMessage {
  @override final SubMessageProto proto;

  String get subject               => proto.subject;
  void   set subject(String value) => proto.subject = value;

  String get queue               => proto.queue;
  void   set queue(String value) => proto.queue = value;

  String get sid               => proto.sid;
  void   set sid(String value) => proto.sid = value;

  SubMessage(String subject, String sid, {String queue}): proto = _protoFactory.sub(subject, sid, queue: queue);

  SubMessage.fromSubscription(Subscription subscription): this(subscription.subject, subscription.sid, queue: subscription.queue);
}
