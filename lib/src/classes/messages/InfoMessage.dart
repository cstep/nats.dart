part of nats_dart;

class InfoMessage extends IncomingMessage {
  @override final InfoMessageProto proto;

  String       get serverId     => proto.serverId;
  String       get version      => proto.version;
  String       get goVersion    => proto.goVersion;
  String       get host         => proto.host;
  int          get port         => proto.port;
  int          get maxPayload   => proto.maxPayload;
  int          get protocol     => proto.protocol;
  int          get clientId     => proto.clientId;
  bool         get authRequired => proto.authRequired;
  bool         get tlsRequired  => proto.tlsRequired;
  bool         get tlsVerify    => proto.tlsVerify;
  List<String> get connectUrls  => proto.connectUrls;

  InfoMessage(String serverId, String version, String goVersion, String host, int port, int maxPayload, int protocol, int clientId, {bool authRequired, bool tlsRequired, bool tlsVerify, List<String> connectUrls}):
    this.fromProto(_protoFactory.info(serverId: serverId, version: version, goVersion: goVersion, host: host, port: port, maxPayload: maxPayload, protocol: protocol, clientId: clientId, authRequired: authRequired, tlsRequired: tlsRequired, tlsVerify: tlsVerify, connectUrls: connectUrls));

  InfoMessage.fromProto(this.proto);
}
