part of nats_dart;

class PingMessage extends PresenceMessage {
  @override final PingMessageProto proto;

  PingMessage(): this.fromProto(_protoFactory.ping());

  PingMessage.fromProto(this.proto);
}
