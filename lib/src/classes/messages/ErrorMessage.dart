part of nats_dart;

class ErrorMessage extends IncomingMessage {
  @override final ErrorMessageProto proto;

  String get message => proto.message;

  ErrorMessage(String message): this.fromProto(_protoFactory.error(message));

  ErrorMessage.fromProto(this.proto);

  @override String toString() => 'ErrorMessage: ${message}';
}
