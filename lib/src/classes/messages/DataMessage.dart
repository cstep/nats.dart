part of nats_dart;

class DataMessage extends PayloadMessage implements IncomingMessage {
  @override final DataMessageProto proto;

  @override final dynamic  payload;
  @override final Encoding encoding;

  String get sid => proto.sid;

  factory DataMessage(String subject, String sid, dynamic payload, {Encoding encoding = ascii, String replySubject}) {
    PayloadMessage._checkPayloadEncoding(payload, encoding);

    var byteLength = (PayloadMessage._isAlreadyEncoded(payload, encoding) ? payload.length : encoding.encode(payload).length);
    var proto      = _protoFactory.data(subject, sid, byteLength, replySubject: replySubject);

    return DataMessage.fromProto(proto, payload, encoding: encoding);
  }

  DataMessage.fromProto(this.proto, this.payload, {this.encoding = ascii}) {
    proto.byteLength = payload.length;
  }
}
