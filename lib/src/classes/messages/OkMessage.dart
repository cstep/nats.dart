part of nats_dart;

class OkMessage extends IncomingMessage {
  @override final OkMessageProto proto;

  OkMessage(): this.fromProto(_protoFactory.ok());

  OkMessage.fromProto(this.proto);
}
