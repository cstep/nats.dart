part of nats_dart;

class ServerOptions {
  bool     _isFinalized =  false;
  bool get  isFinalized => _isFinalized;

  static const String language = 'Dart';  // The implementation language of the client
  static const String version  = VERSION; // The version of the client

  /// STRING is standard
  PayloadType     _payloadType = PayloadType.STRING;
  PayloadType get  payloadType                    => _payloadType;
  void        set  payloadType(PayloadType value) => _payloadType = _checkFinalized(value);

  /// Sets the encoding type used when dealing with [[PayloadType.STRING]] messages
  Encoding     _encoding = ascii;
  Encoding get  encoding                 => _encoding;
  void     set  encoding(Encoding value) => _encoding = _checkFinalized(value);

  /// 0 is standard. 1 will periodically receive `INFO` messages with known servers
  int      _protocol = 0;
  int  get  protocol            => _protocol;
  void set  protocol(int value) => _protocol = _checkFinalized(value);

  /// Turns on +OK protocol acknowledgements
  bool     _verbose = false;
  bool get  verbose             => _verbose;
  void set  verbose(bool value) => _verbose = _checkFinalized(value);

  /// Turns on additional strict format checking, e.g. for properly formed subjects
  bool    _pedantic = false;
  bool get pedantic             => _pedantic;
  void set pedantic(bool value) => _pedantic = _checkFinalized(value);

  /// Indicates whether the client requires an SSL connection
  bool     _tlsRequired = false;
  bool get  tlsRequired             => _tlsRequired;
  void set  tlsRequired(bool value) => _tlsRequired = _checkFinalized(value);

  /// If set to true, the server (version 1.2.0+) will not send originating messages from this connection to its own subscriptions.
  bool     _echo = false;
  bool get  echo             => _echo;
  void set  echo(bool value) => _echo = _checkFinalized(value);

  // ======== Optional Properties
  /// Client name
  String     _name;
  String get  name               => _name;
  void   set  name(String value) => _name = _checkFinalized(isNotBlank(value) ? value : null);

  /// Client authorization token (if auth_required is set)
  String     _authToken;
  String get  authToken               => _authToken;
  void   set  authToken(String value) => _authToken = _checkFinalized(isNotBlank(value) ? value : null);

  /// Connection username (if auth_required is set)
  String     _username;
  String get  username               => _username;
  void   set  username(String value) => _username = _checkFinalized(isNotBlank(value) ? value : null);

  /// Connection password (if `auth_required` is set)
  String     _password;
  String get  password               => _password;
  void   set  password(String value) => _password = _checkFinalized(isNotBlank(value) ? value : null);

  ServerOptions({String name, PayloadType payloadType, Encoding encoding = ascii, int protocol, bool verbose, bool pedantic, bool tlsRequired, bool echo, String authToken, String username, String password}):
    this.fromMap({
      'name'        : name,
      'payloadType' : payloadType,
      'encoding'    : encoding,
      'protocol'    : protocol,
      'verbose'     : verbose,
      'pedantic'    : pedantic,
      'tls_required': tlsRequired,
      'echo'        : echo,
      'auth_token'  : authToken,
      'user'        : username,
      'pass'        : password,
     });

  ServerOptions.from(ServerOptions other): this.fromMap(other.toMap());

  ServerOptions.fromJson(String jsonString): this.fromMap(json.decode(jsonString));

  ServerOptions.fromMap(Map map) {
    if (map['payloadType']  != null) this._payloadType = (map['payloadType']  as PayloadType);
    if (map.containsKey('encoding')) this._encoding    = (map['encoding']     as Encoding   );
    if (map['protocol']     != null) this._protocol    = (map['protocol']     as int        );
    if (map['verbose']      != null) this._verbose     = (map['verbose']      as bool       );
    if (map['pedantic']     != null) this._pedantic    = (map['pedantic']     as bool       );
    if (map['tls_required'] != null) this._tlsRequired = (map['tls_required'] as bool       );
    if (map['echo']         != null) this._echo        = (map['echo']         as bool       );
    this._name        = map['name'];
    this._authToken   = map['auth_token'];
    this._username    = map['user'];
    this._password    = map['pass'];

    // -------- validate authentication
    if (isNotBlank(_authToken) && (isNotBlank(_username) || isNotBlank(_password))) {
      throw NatsError.BAD_AUTHENTICATION;
    }
  }

  dynamic _checkFinalized(dynamic value) {
    return (!_isFinalized ? value : throw StateError("Already finalized, can't modify property(ies)"));
  }

  void finalize() => _isFinalized = (!isFinalized ? true : throw StateError("Can't finalize already finalized object"));

  Map<String, dynamic> toMap() {
    return {
      'lang'        : language,
      'version'     : version,
      'payloadType' : payloadType,
      'encoding'    : encoding,
      'protocol'    : protocol,
      'verbose'     : verbose,
      'pedantic'    : pedantic,
      'tls_required': tlsRequired,
      'echo'        : echo,
      if (     name != null) 'name'      : name,
      if (authToken != null) 'auth_token': authToken,
      if ( username != null) 'user'      : username,
      if ( password != null) 'pass'      : password,
    };
  }

  Map<String, dynamic> toJson() => toJsonEncodable();

  Map<String, dynamic> toJsonEncodable() {
    return toMap()..remove('payloadType')..remove('encoding');
  }

  String toJsonString({bool pretty  =false}) => (pretty ? JsonEncoder.withIndent('  ') : JsonEncoder()).convert(toJsonEncodable());

  @override
  String toString() => toJsonString();
}
