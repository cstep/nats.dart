part of nats_dart;

abstract class MessageProto {
  String get _control;
  // extending classes MUST define
  // @override String get _control => control;
  // static const String control = 'asdf';

  covariant MessageType     _messageType;
  MessageType           get  messageType => (_messageType ??= Enum.fromString<MessageType>(_control));

  String toProtocolString();
}
