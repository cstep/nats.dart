part of nats_dart;

abstract class PayloadMessageProto extends MessageProto {
  String subject;
  String replySubject;
  int    byteLength;

  PayloadMessageProto(this.subject, this.byteLength, {this.replySubject});
}
