part of nats_dart;

class PubMessageProto extends PayloadMessageProto {
  @override    String get _control => control;
  static const String      control =  'PUB';

  PubMessageProto(String subject, int byteLength, {String replySubject}): super(subject, byteLength, replySubject: replySubject);

  @override
  String toProtocolString() => '$control $subject${(replySubject??'').isNotEmpty ? ' $replySubject' : ''} $byteLength';
}
