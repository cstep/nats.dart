part of nats_dart;

class UnsubMessageProto extends MessageProto {
  @override    String get _control => control;
  static const String      control =  'UNSUB';

  String sid;
  int    max;

  UnsubMessageProto(this.sid, {this.max});

  @override
  String toProtocolString() => '$control $sid${(max??0) != 0 ? ' $max' : ''}';
}
