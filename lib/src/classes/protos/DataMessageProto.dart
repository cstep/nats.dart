part of nats_dart;

class DataMessageProto extends PayloadMessageProto {
  @override    String get _control => control;
  static const String      control =  'MSG';

  @override final String subject;
  @override final String replySubject;
  @override final int    byteLength;

  final String sid;

  DataMessageProto(this.subject, this.sid, this.byteLength, {this.replySubject}): super(subject, byteLength, replySubject: replySubject);

  @override
  String toProtocolString() => '$control $subject $sid${(replySubject??'').isNotEmpty ? ' $replySubject' : ''} $byteLength';
}
