part of nats_dart;

class ErrorMessageProto extends MessageProto {
  @override    String get _control => control;
  static const String      control =  '-ERR';

  final String message;

  ErrorMessageProto(this.message);

  @override
  String toProtocolString() => '$control $message';
}
