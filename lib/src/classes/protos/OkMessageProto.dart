part of nats_dart;

class OkMessageProto extends MessageProto {
  @override    String get _control => control;
  static const String      control =  '+OK';

  @override
  String toProtocolString() => '$control';
}
