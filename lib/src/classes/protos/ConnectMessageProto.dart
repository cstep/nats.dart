part of nats_dart;

class ConnectMessageProto extends MessageProto {
  @override    String get _control => control;
  static const String      control =  'CONNECT';

  ServerOptions options;

  ConnectMessageProto(this.options);

  @override
  String toProtocolString() => '$control ${options.toJsonString()}';
}
