part of nats_dart;

class PingMessageProto extends MessageProto {
  @override    String get _control => control;
  static const String      control =  'PING';

  @override
  String toProtocolString() => '$control';
}
