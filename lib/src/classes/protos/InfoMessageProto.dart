part of nats_dart;

class InfoMessageProto extends MessageProto {
  @override    String get _control => control;
  static const String      control =  'INFO';

  final String       serverId;     // The unique identifier of the NATS server
  final String       version;      // The version of the NATS server
  final String       goVersion;    // The version of golang the NATS server was built with
  final String       host;         // The IP address used to start the NATS server, by default this will be 0.0.0.0 and can be configured with -client_advertise host; //port
  final int          port;         // The port number the NATS server is configured to listen on
  final int          maxPayload;   // Maximum payload size, in bytes, that the server will accept from the client.
  final int          protocol;     // An integer indicating the protocol version of the server. The server version 1.2.0 sets this to 1 to indicate that it supports the "Echo" feature.
  final int          clientId;     // An optional unsigned integer (64 bits) representing the internal client identifier in the server. This can be used to filter client connections in monitoring, correlate with error logs, etc...
  final bool         authRequired; // If this is set, then the client should try to authenticate upon connect.
  final bool         tlsRequired;  // If this is set, then the client must perform the TLS/1.2 handshake. Note, this used to be ssl_required and has been updated along with the protocol from SSL to TLS.
  final bool         tlsVerify;    // If this is set, the client must provide a valid certificate during the TLS handshake.
  final List<String> connectUrls;  // An optional list of server urls that a client can connect to.

  InfoMessageProto({this.serverId, this.version, this.goVersion, this.host, this.port, this.maxPayload, this.protocol, this.clientId, authRequired, tlsRequired, tlsVerify, this.connectUrls}):
    this.authRequired = (authRequired??false), // -- don't allow nulls to be explicitly set
    this.tlsRequired  = ( tlsRequired??false), // -- don't allow nulls to be explicitly set
    this.tlsVerify    = (   tlsVerify??false); // -- don't allow nulls to be explicitly set

  @override
  String toProtocolString() {
    var map = {
      'server_id'  : serverId,
      'version'    : version,
      'go'         : goVersion,
      'host'       : host,
      'port'       : port,
      'max_payload': maxPayload,
      'protocol'   : protocol,
      'client_id'  : clientId,
      if (authRequired??false         ) 'auth_required': authRequired,
      if ( tlsRequired??false         ) 'tls_required' : tlsRequired,
      if (   tlsVerify??false         ) 'tls_verify'   : tlsVerify,
      if ((connectUrls??[]).isNotEmpty) 'connect_urls' : connectUrls,
    };

    return '$control ${json.encode(map)}';
  }
}
