part of nats_dart;

class PongMessageProto extends MessageProto {
  @override    String get _control => control;
  static const String      control =  'PONG';

  @override
  String toProtocolString() => '$control';
}
