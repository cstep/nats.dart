part of nats_dart;

class SubMessageProto extends MessageProto {
  @override    String get _control => control;
  static const String      control =  'SUB';

  String subject;
  String queue;
  String sid;

  SubMessageProto(this.subject, this.sid, {this.queue});

  @override
  String toProtocolString() => '$control $subject${(queue??'').isNotEmpty ? ' $queue' : ''} $sid';
}
