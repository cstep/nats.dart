part of nats_dart;

class RequestSubscription extends Subscription {
  RequestSubscription(ProtocolHandler protocolHandler, String inbox): super(protocolHandler, inbox);
}
