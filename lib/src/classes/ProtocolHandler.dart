part of nats_dart;

class ProtocolHandler with Events {
  // TODO: initially copied from nats.ts lib -- needs revisiting
  // ServerInfo     _info             = ServerInfo();
  // bool           _infoReceived     = false;
  // MessageBuffer  _msgBuffer; // nullable
  // Timer          _pingTimer; // nullable
  // List           _pongs            = [];
  // int            _pout             = 0;
  // bool           _reconnecting     = false;
  // int            _reconnects       = 0;
  // Server         _currentServer;
  // int            _ssid             = 1;
  // ParserState    _state            = ParserState.AWAITING_CONTROL;
  // bool           _wasConnected     = false;
  // bool           _noMorePublishing = false;
  // StreamController<Message> _outbound = StreamController();
  // MuxSubscriptions          muxSubscriptions = MuxSubscriptions();

  ProtocolParser            _parser;
  ServerOptions             options;
  SubscriptionList          subscriptions = SubscriptionList();
  ServerSet                 _servers;
  Socket                    _socket;
  Stream<Message>           _inbound;

  bool     _isDraining = false;
  bool get  isDraining => _isDraining;

  bool     _isConnected =  false; // -- read-only
  bool get  isConnected => this._isConnected;

  bool     _isClosed =  false; // -- read-only
  bool get  isClosed => this._isClosed;

  ProtocolHandler(this._servers) {
    // TODO: initially copied from nats.ts lib -- needs revisiting
    // var servers = ServerSet(!this.options.noRandomize, this.options.servers, firstServerUrl: this.options.url);
    // options.encoding    ??= Utf8Codec(allowMalformed: true);
    // options.payloadType ??= PayloadType.STRING;
  }

  Future<void> connect(ServerOptions options, {Duration timeout}) async {
    timeout ??= DEFAULT_CONNECT_TIMEOUT; // -- don't use explicitly-passed nulls
    _prepareConnection();
    bool isReconnect = false;
    if (_servers.currentServer.didConnect) {
      isReconnect = true;
      emitEvent(ReconnectEvent(_servers.currentServer.url.toString()));
    }

    Completer completer = Completer();

    StreamSubscription errorListener;
    StreamSubscription infoListener = on<InfoMessage>().listen((InfoMessage message) {
      sendCommand(_messageFactory.connect(options));

      // -------- listen for an error
      errorListener = on<ErrorMessage>().listen((ErrorMessage errorMessage) {
        completer.completeError(errorMessage);
      });

      // -------- nats doesn't send a confirmation, so if we haven't received an error after 1 second, assume everything's fine
      Future.delayed(Duration(seconds: 1), () {
        if (!completer.isCompleted) {
          _isConnected = true;
          _servers.currentServer.didConnect = true;
          completer.complete();
          if (!isReconnect) {
            emitEvent(ConnectEvent(_servers.currentServer.url.toString()));
          }
        }
      });
    }, onError: completer.completeError);

    unawaited(completer.future
      .timeout(timeout, onTimeout: () => throw TimeoutException('Server failed to respond', timeout))
      .whenComplete(() {
        infoListener.cancel();
        errorListener?.cancel();
      }));

    // ======== Connect to our Server
    log.general.finest('establishing connection to ${_servers.currentServer.url} ...');
    if (options.tlsRequired) {
      _socket = await SecureSocket.connect(_servers.currentServer.url.host, _servers.currentServer.url.port, timeout: timeout);
    } else {
      _socket = await Socket.connect(_servers.currentServer.url.host, _servers.currentServer.url.port, timeout: timeout);
    }
    _socket.encoding = ascii;
    log.general.finest('tcp socket connection established to ${_servers.currentServer.url}');

    // ======== Listen for Messages
    Stream broadcast = _socket.asBroadcastStream();

    broadcast.listen(null, cancelOnError: false, onDone: () {
      _isConnected = false;
      _isClosed    = true;
      emitEvent(DisconnectEvent(_servers.currentServer.url.toString()));
    });

    _parser = _protocolParserFactory.create(options.encoding);
    this._inbound = broadcast.transform(_parser);
    _inbound.listen((Message message) {
      var payload = (message is PayloadMessage ? ' <<< ${message.payload.length} bytes' : '');
      log.incoming.fine('RX <<< ${message.toProtocolString()}${payload}');

      this.emitEvent(message);
    });

    return completer.future;
  }

  void _prepareConnection() {
    // TODO: initially copied from nats.ts lib -- needs porting
    // Commands may have been queued during reconnect. Discard everything except:
    // 1) ping requests with a pong callback
    // 2) publish requests
    //
    // Rationale: CONNECT and SUBs are written directly upon connecting, any PONG
    // response is no longer relevant, and any UNSUB will be accounted for when we
    // sync our SUBs. Without this, users of the client may miss state transitions
    // via callbacks, would have to track the client's internal connection state,
    // and may have to double buffer messages (which we are already doing) if they
    // wanted to ensure their messages reach the server.

    // copy outbound and reset it
    /*
    _outbound.sink
    List<MessageHandler> pongs = [];
    if (buffers.length) {
        let pongIndex = 0;
        // find all the pings with associated callback, and pubs
        buffers.forEach((buf) => {
            let cmd = buf.toString('binary');
            if (PING.test(cmd) && this.pongs !== null && pongIndex < this.pongs.length) {
                let f = this.pongs[pongIndex++];
                if (f) {
                    this.outbound.fill(buf);
                    pongs.push(f);
                }
            } else if (cmd.length > 3 && cmd[0] === 'P' && cmd[1] === 'U' && cmd[2] === 'B') {
                this.outbound.fill(buf);
            }
        });
    }
    this.pongs = pongs;
    this.state = ParserState.AWAITING_CONTROL;

    // Clear info processing.
    this.info = {} as ServerInfo;
    this.infoReceived = false;
    */

    // Select a server to connect to
    _servers.next();
  }

  void publish(String subject, dynamic payload, {String replySubject, Encoding encoding = ascii}) {
    if (isClosed  ) throw NatsError.CONNECTION_CLOSED;
    if (isDraining) throw NatsError.CONNECTION_DRAINING;

    sendCommand(_messageFactory.pub(subject, payload, replySubject: replySubject, encoding: encoding));
  }

  Subscription subscribe(String subject, {String queue, int max}) {
    if (isClosed  ) throw NatsError.CONNECTION_CLOSED;
    if (isDraining) throw NatsError.CONNECTION_DRAINING;

    var subscription = _subscriptionFactory.subscription(this, subject, queue: queue);

    sendCommand(_messageFactory.subFromSubscription(subscription));
    subscriptions.add(subscription);
    emitEvent(SubscribeEvent(subscription));

    if ((max??0) > 0) {
      unsubscribe(subscription, max: max);
    }

    return subscription;
  }

  void unsubscribe(Subscription subscription, {int max}) {
    if (isClosed  ) throw NatsError.CONNECTION_CLOSED;
    if (isDraining) throw NatsError.CONNECTION_DRAINING;

    subscription.max = max;

    var doSendCommand = true;
    if (max == 0 || subscription.max <= subscription.receivedCount) { // -- unsubscribe immediately
      doSendCommand = subscriptions.remove(subscription);
      if (doSendCommand) {
        emitEvent(UnsubscribeEvent(subscription));
      }
    }

    if (doSendCommand) { // -- schedule unsubscription
      sendCommand(_messageFactory.unsubFromSubscription(subscription, max: subscription.max));
    }
  }

  void sendCommand(OutgoingMessage command) {
    String protocolString = command.toProtocolString();
    _socket.add(_socket.encoding.encode(protocolString) + _parser.SEPARATOR);

    String payloadString = '';
    if (command is PayloadMessage) {
      payloadString = '${(command as PayloadMessage).payload.length} bytes';

      _socket.add((command as PayloadMessage).encodedPayload + _parser.SEPARATOR);
    }

    log.outgoing.fine('TX >>> ${protocolString}${(payloadString??'').isNotEmpty ? ' >>> $payloadString' : ''}');
  }

  void sendPing() => sendCommand(_messageFactory.ping());

  void sendPong() => sendCommand(_messageFactory.pong());

  Subscription registerRequest(String subject) {
    // -------- setup subscription
    String       inbox        = "INBOX_${Uuid().v4()}";
    Subscription subscription = _subscriptionFactory.requestSubscription(this, inbox);

    // -------- add to our collection
    subscriptions.add(subscription);

    // -------- tell the server
    sendCommand(_messageFactory.subFromSubscription(subscription));
    emitEvent(RequestEvent(subscription));
    unsubscribe(subscription, max: 1);

    return subscription;
  }

  // TODO: the below is never actually used. We need to subscribe to incoming INFO messages and route them through this method
  ServersChangedEvent processServerUpdate(InfoMessage info) {
    List<String> added           = [];
    List<String> deleted         = [];
    List<Server> reportedServers = [];

    // ======== Parse Reported Servers
    info.connectUrls.forEach((String urlString) {
      String currentScheme = _servers.currentServer.url.scheme;
      Uri    newUrl        = Uri.parse(urlString).replace(scheme: '${currentScheme}://${urlString}');
      reportedServers.add(Server(newUrl.toString(), isImplicit: true));
    });

    // ======== Remove Servers That Are No Longer Reported
    _servers
      .where((Server server) => server.isImplicit)
      .forEach((Server trackedServer) {
        if (trackedServer.toString() != _servers.currentServer.toString() && !reportedServers.contains(trackedServer)) { // -- our record is not included in the reported list
          _servers.remove(trackedServer);
          deleted.add(trackedServer.toString());
        }

        reportedServers.remove(trackedServer); // -- remove this entry from the reported list
      });

    // ======== Add New Servers to Our Collection
    reportedServers.forEach((Server server) { // -- any remaining servers are new
      _servers.add(server);
      added.add(server.toString());
    });

    return ServersChangedEvent(added: added, deleted: deleted);
  }
}
