part of nats_dart;

class NatsError extends Error /*extends enum*/ {
  final String code;
  final String description;
  final Object cause;


  static final NatsError BAD_AUTHENTICATION             = NatsError._register('BAD_AUTHENTICATION',             'Token and [username|password] can not both be provided'                                                             );
  static final NatsError BAD_CREDENTIALS                = NatsError._register('BAD_CREDENTIALS',                'Bad user credentials',                                    aliases: ['BAD_CREDS']                     );
  static final NatsError BAD_NKEY_CREDENTIALS           = NatsError._register('BAD_NKEY_CREDENTIALS',           'Bad nkey credentials',                                    aliases: ['BAD_NKEY_SEED']                 );
  static final NatsError BAD_JSON                       = NatsError._register('BAD_JSON',                       'Message should be a non-circular JSON-serializable value'                                            );
  static final NatsError BAD_MESSAGE                    = NatsError._register('BAD_MESSAGE',                    'Message cannot be a function',                            aliases: ['BAD_MSG']                       );
  static final NatsError BAD_REPLY                      = NatsError._register('BAD_REPLY',                      'Reply cannot be a function'                                                                          );
  static final NatsError BAD_SUBJECT                    = NatsError._register('BAD_SUBJECT',                    'Subject must be supplied'                                                                            );
  static final NatsError CLIENT_CERT_REQUIRED           = NatsError._register('CLIENT_CERT_REQUIRED',           'Server requires a client certificate.',                   aliases: ['CLIENT_CERT_REQ']               );
  static final NatsError CONNECTION_CLOSED              = NatsError._register('CONNECTION_CLOSED',              'Connection closed',                                       aliases: ['CONN_CLOSED']                   );
  static final NatsError CONNECTION_DRAINING            = NatsError._register('CONNECTION_DRAINING',            'Connection draining',                                     aliases: ['CONN_DRAINING']                 );
  static final NatsError CONNECTION_ERROR               = NatsError._register('CONNECTION_ERROR',               'Could not connect to server',                             aliases: ['CONN_ERR']                      );
  static final NatsError CONNECTION_TIMEOUT             = NatsError._register('CONNECTION_TIMEOUT',             'Connection timeout',                                      aliases: ['CONN_TIMEOUT']                  );
  static final NatsError STALE_CONNECTION               = NatsError._register('STALE_CONNECTION',               'Connection failed to respond to PING',                    aliases: ['PING_TIMEOUT']                  );
  static final NatsError INVALID_ENCODING               = NatsError._register('INVALID_ENCODING',               'Invalid message encoding'                                                                            );
  static final NatsError NKEY_OR_JWT_REQUIRED           = NatsError._register('NKEY_OR_JWT_REQUIRED',           'An Nkey or User JWT callback is required.',               aliases: ['NKEY_OR_JWT_REQ']               );
  static final NatsError NO_ECHO_NOT_SUPPORTED          = NatsError._register('NO_ECHO_NOT_SUPPORTED',          'No echo option is not supported by this server'                                                      );
  static final NatsError NO_SEED_IN_CREDENTIALS         = NatsError._register('NO_SEED_IN_CREDENTIALS',         'Cannot locate signing key in credentials',                aliases: ['NO_SEED_IN_CREDS']              );
  static final NatsError NO_USER_JWT_IN_CREDENTIALS     = NatsError._register('NO_USER_JWT_IN_CREDENTIALS',     'Cannot locate user jwt in credentials.',                  aliases: ['NO_USER_JWT_IN_CREDS']          );
  static final NatsError NON_SECURE_CONNECTION_REQUIRED = NatsError._register('NON_SECURE_CONNECTION_REQUIRED', 'Server does not support a secure connection.',            aliases: ['NON_SECURE_CONN_REQ']           );
  static final NatsError NONCE_SIGNER_NOT_FUNCTION      = NatsError._register('NONCE_SIGNER_NOT_FUNCTION',      'nonce signer is not a function',                          aliases: ['NONCE_SIGNER_NOT_FUNC']         );
  static final NatsError REQUEST_TIMEOUT                = NatsError._register('REQUEST_TIMEOUT',                'Request timed out.',                                      aliases: ['REQ_TIMEOUT']                   );
  static final NatsError SECURE_CONNECTION_REQUIRED     = NatsError._register('SECURE_CONNECTION_REQUIRED',     'Server requires a secure connection.',                    aliases: ['SECURE_CONN_REQ']               );
  static final NatsError SIGNATURE_REQUIRED             = NatsError._register('SIGNATURE_REQUIRED',             'Server requires an nkey signature.',                      aliases: ['SIG_REQ']                       );
  static final NatsError SUBSCRIPTION_CLOSED            = NatsError._register('SUBSCRIPTION_CLOSED',            'Subscription closed',                                     aliases: ['SUB_CLOSED']                    );
  static final NatsError SUBSCRIPTION_DRAINING          = NatsError._register('SUBSCRIPTION_DRAINING',          'Subscription draining',                                   aliases: ['SUB_DRAINING']                  );
  static final NatsError SUBSCRIPTION_TIMEOUT           = NatsError._register('SUBSCRIPTION_TIMEOUT',           'Subscription timed out.',                                 aliases: ['SUB_TIMEOUT']                   );
  static final NatsError UNKNOWN                        = NatsError._register('UNKNOWN',                        'An unknown error occurred'                                                                           );

  static final Map<String, NatsError> _byName = {};
  static bool _isInitialized = false;
  static void _initialize() {
    if (_isInitialized) return; // early

    // simply reference EVERY error constant to cause them to register (ugh)
    BAD_AUTHENTICATION;
    BAD_CREDENTIALS;
    BAD_NKEY_CREDENTIALS;
    BAD_JSON;
    BAD_MESSAGE;
    BAD_REPLY;
    BAD_SUBJECT;
    CLIENT_CERT_REQUIRED;
    CONNECTION_CLOSED;
    CONNECTION_DRAINING;
    CONNECTION_ERROR;
    CONNECTION_TIMEOUT;
    STALE_CONNECTION;
    INVALID_ENCODING;
    NKEY_OR_JWT_REQUIRED;
    NO_ECHO_NOT_SUPPORTED;
    NO_SEED_IN_CREDENTIALS;
    NO_USER_JWT_IN_CREDENTIALS;
    NON_SECURE_CONNECTION_REQUIRED;
    NONCE_SIGNER_NOT_FUNCTION;
    REQUEST_TIMEOUT;
    SECURE_CONNECTION_REQUIRED;
    SIGNATURE_REQUIRED;
    SUBSCRIPTION_CLOSED;
    SUBSCRIPTION_DRAINING;
    SUBSCRIPTION_TIMEOUT;
    UNKNOWN;

    _isInitialized = true;
  }

  NatsError._anonymous(this.code, this.description, {this.cause}): assert(cause == null || cause is Error || cause is Exception, "if passed, cause must be an instance of either Error or Exception");

  factory NatsError._register(String name, String description, {List<String> aliases = const <String>[]}) {
    NatsError instance = NatsError._anonymous(name, description);

    var check = {
      name,
      ...aliases
    };

    check.forEach((alias) {
      _byName[alias] = (!_byName.containsKey(alias) ? instance : throw ArgumentError('name or alias already registered: `$alias`'));
    });

    return instance;
  }

  factory NatsError.from(NatsError base, {cause}) => NatsError._anonymous(base.code, base.description, cause: cause);

  factory NatsError.fromString(String code, {cause}) {
    _initialize();

    var base = _byName["$code".toUpperCase()];
    if (base == null) { // -- invalid code
      throw ArgumentError('ErrorType `$code` is an invalid NatsError type');
    }

    return (cause != null ? NatsError.from(base, cause: cause) : base);
  }

  @override String toString() => 'NatsError: $code ($description)';

  @override bool operator ==(dynamic other) => (other is NatsError && this.code == other.code); // ignore: hash_and_equals
}
