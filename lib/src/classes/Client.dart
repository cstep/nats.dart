part of nats_dart;

class Client with Events {
  final ProtocolHandler _protocolHandler;
  final ClientOptions   options;
  Heartbeat             _heartbeat;

  Client(ProtocolHandler handler, [ClientOptions clientOptions]): _protocolHandler = handler, options = (clientOptions??ClientOptions()) {
    _heartbeat = Heartbeat(options.pingInterval, this._sendPing, maxMissed: options.maxPingsOutstanding)..cancel(); // -- don't start it until we're connected

    _protocolHandler
      ..on<Message>().listen((Message message) {
        switch (message.runtimeType) {
          case PingMessage: _protocolHandler.sendPong(); break;
          case PongMessage: _heartbeat.heartbeat();      break;

          default:
            emitEvent(message); // -- re-emit from the client
        }
      })
      ..on<ConnectEvent   >().listen((   ConnectEvent event) => _heartbeat.reset())   // -- start our watchdog
      ..on<DisconnectEvent>().listen((DisconnectEvent event) => _heartbeat.cancel()); // -- stop our watchdog

     this._addDebugHandlers();
  }

  static Future<Client> connectSingle(String host, {int port, ServerOptions serverOptions, ClientOptions clientOptions, String username, String password}) {
    var server = Server(host, port: port);

    return connect(ServerSet()..add(server), serverOptions: serverOptions, clientOptions: clientOptions, username: username, password: password);
  }

  static Future<Client> connect(ServerSet servers, {ServerOptions serverOptions, ClientOptions clientOptions, String username, String password}) {
    serverOptions ??= ServerOptions();

    if (username != null || password != null) { // -- we have modifications to make
      if (serverOptions.isFinalized) { // -- we have modifications, but options have been finalized
        serverOptions = ServerOptions.from(serverOptions); // -- re-open our options
      }

      serverOptions.username = username??serverOptions.username;
      serverOptions.password = password??serverOptions.password;
    }

    if (!serverOptions.isFinalized) {
      serverOptions.finalize();
    }

    var handler = _protocolHandlerFactory.create(servers);
    var client  = Client(handler, clientOptions);

    return handler.connect(serverOptions, timeout: client.options.connectTimeout)
      .then((_) => client);
  }

  /// Closes the connection to the NATS server. A closed client cannot be reconnected.
  // void close() => _protocolHandler.close(); TODO

  /// Publish a message to the given subject, with optional payload and reply subject.
  /// @param subject
  /// @param data optional (can be a string, JSON object, or Buffer. Must match [ServerOptions.payloadType].)
  /// @param reply optional the subject to which subscribers may publish a reply message
  void publish(String subject, {dynamic payload, String replySubject, Encoding encoding = ascii}) {
    assert(isNotBlank(subject), throw NatsError.BAD_SUBJECT);

    _protocolHandler.publish(subject, payload, replySubject: replySubject, encoding: encoding);
  }

  /// Subscribe to a given subject. Messages are passed to the provided callback.
  /// @param subject
  /// @param cb
  /// @param opts   Optional subscription options
  /// @return Promise<Subscription>
  Subscription subscribe(String subject, {String queue, int max, MessageHandler handler}) {
    assert(isNotBlank(subject), throw NatsError.BAD_SUBJECT);

    var subscription = _protocolHandler.subscribe(subject, queue: queue, max: max);
    if (handler != null) {
      subscription._stream.listen(handler);
    }

    return subscription;
  }

  void sendPing() => _protocolHandler.sendPing();

  /// Publish a request message with an implicit inbox listener as the reply. Message is optional.
  /// This should be treated as a subscription. The subscription is auto-cancelled after the
  /// first reply is received or the timeout in millisecond is reached.
  ///
  /// If a timeout is reached, the promise is rejected. Returns the received message if resolved.
  ///
  /// @param subject
  /// @param timeout
  /// @param data optional (can be a string, JSON object, or Buffer. Must match specified Payload option)
  /// @return Promise<Msg>
  Future<DataMessage> request(String subject, {dynamic payload, Encoding encoding = ascii, Duration timeout, void Function(Object error) onTimeout}) {
    assert(isNotBlank(subject), throw NatsError.BAD_SUBJECT);
    timeout   ??= ClientOptions.DEFAULT_REQUEST_TIMEOUT;
    onTimeout ??= (_) => throw NatsError.REQUEST_TIMEOUT;

    Subscription inbox = _protocolHandler.registerRequest(subject);
    publish(subject, payload: payload, replySubject: inbox.subject, encoding: encoding);

    Completer completer = Completer<DataMessage>();
    inbox.listen(completer.complete, onError: completer.completeError, cancelOnError: false);

    return completer.future
      .timeout(timeout)
      .catchError(onTimeout, test: (Object error) => error is TimeoutException)
      .whenComplete(inbox.unsubscribe);
  }

  void _onPingIntervalUpdate(Duration pingInterval) {
    log.general.finest('PINGing every $pingInterval');
    _heartbeat.interval = pingInterval;
  }

  void _sendPing() {
    _protocolHandler.sendPing();
    log.general.finest('Outstanding pings: ${_heartbeat.outstandingCount}/${_heartbeat.maxMissed}');
  }

  // TODO: initially copied from nats.ts lib -- needs revisiting
  /*private static defaultOptions(): ConnectionOptions {
    return {
    encoding: 'utf8',
    maxPingOut: DEFAULT_MAX_PING_OUT,
    maxReconnectAttempts: DEFAULT_MAX_RECONNECT_ATTEMPTS,
    noRandomize: false,
    pedantic: false,
    pingInterval: DEFAULT_PING_INTERVAL,
    reconnect: true,
    reconnectTimeWait: DEFAULT_RECONNECT_TIME_WAIT,
    tls: undefined,
    verbose: false,
    waitOnFirstConnect: false
    } as ConnectionOptions;
  }*/

  // TODO: initially copied from nats.ts lib -- needs revisiting
  /// Flush outbound queue to server and call optional callback when server has processed all data.
  /// @param cb is optional, if not provided a Promise is returned. Flush is completed when promise resolves.
  /// @return Promise<void> or void if a callback was provided.
  /*FutureOr<void> flush([FlushCallback callback]) {
    return _protocolHandler.flush(callback);
  }*/

  // TODO: initially copied from nats.ts lib -- needs revisiting
  /// Drains all subscriptions. Returns a Promise that when resolved, indicates that all subscriptions have finished,
  /// and the client closed. Note that after calling drain, it is impossible to create new
  /// subscriptions or make any requests. As soon as all messages for the draining subscriptions are processed,
  /// it is also impossible to publish new messages.
  /// A drained connection is closed when the Promise resolves.
  /// @see [[Subscription.drain]]
  //Future<dynamic> drain() => _protocolHandler.drain();

  // TODO: initially copied from nats.ts lib -- needs porting
  void _addDebugHandlers() {
    // let events = [
    //   'close',
    //   'connect',
    //   'connecting',
    //   'disconnect',
    //   'error',
    //   'permissionError',
    //   'pingcount',
    //   'pingtimer',
    //   'reconnect',
    //   'reconnecting',
    //   'serversChanged',
    //   'subscribe',
    //   'unsubscribe',
    //   'yield',
    // ];
    //
    // function handler(name: string) {
    //   return function(arg: any) {
    //     console.log('debughdlr', name, [arg]);
    //   }
    // }
    //
    // events.forEach((e) => {
    //   this.on(e, handler(e));
    // });
  }
}
