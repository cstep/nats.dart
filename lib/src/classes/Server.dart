part of nats_dart;

class Server {
  Uri  url;
  bool didConnect = false;
  int  reconnects = 0;

  bool     _isImplicit;
  bool get  isImplicit => _isImplicit;

  List<String> get credentials => (url.userInfo.isNotEmpty ? url.userInfo.split(':') : null);

  Server(dynamic url, {int port, bool isImplicit = false}) {
    url ??= '';
    assert(url is String || url is Uri, 'url must be either a String or instance of Uri');

    if (url is String) {
      if (!RegExp(r'^.*?:?\/\/.*').hasMatch(url??'')) { // -- string doesn't have a scheme
        url = 'nats://${url}';
      }
      url = Uri.parse(url);

    } else { // -- this is a URI
      if (!url.hasScheme) { // -- URI doesn't have a scheme
        url = url.replace(scheme: 'nats');
      }
    }

    port            ??= (url.port??0);
    this.url          = url.replace(host: ifBlank(url.host, DEFAULT_HOST), port: (port != 0 ? port : DEFAULT_PORT));
    this._isImplicit  = isImplicit;
  }

  @override String toString() => url.toString();
}
