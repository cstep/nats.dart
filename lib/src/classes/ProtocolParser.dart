part of nats_dart;

class ProtocolParser implements StreamTransformer<Uint8List, Message> {
  // -------- constants
  static const int       _CR         = 13;
  static const int       _LF         = 10;
  static const List<int> _ENDING     = [_CR, _LF];
  List<int> get SEPARATOR => _ENDING;

  // -------- delegation
  StreamTransformer<Uint8List, Message> _delegate;
  @override Stream<Message>             bind(Stream<Uint8List> stream) => _delegate.bind(stream);
  @override StreamTransformer<RS, RT>   cast<RS, RT>()                 => _delegate.cast<RS, RT>();

  // -------- properties
  final Encoding encoding;
  Uint8List      dataBuffer = Uint8List(0);

  ProtocolParser([this.encoding = ascii]) {
    _delegate = StreamTransformer<Uint8List, Message>.fromHandlers(
      handleData: (Uint8List messageData, EventSink<Message> sink) {
        _parseMessages(messageData).forEach(sink.add);
      }
    );
  }

  List<Message> _parseMessages(Uint8List messageData) {
    dataBuffer = messageData;

    List<Message> messages = [];
    while (dataBuffer.isNotEmpty) {
      MessageProto proto = _parseProtoString(_yankProtoString());

      switch (proto.messageType) {
        case MessageType.MESSAGE:
          var payload = _yankRange((proto as DataMessageProto).byteLength, ignoreTail: 2);
          messages.add(_messageFactory.dataFromProto(proto, payload, encoding: encoding));
          break;

        case MessageType.INFO : messages.add(_messageFactory.infoFromProto(proto));  break;
        case MessageType.PING : messages.add(_messageFactory.ping());                break;
        case MessageType.PONG : messages.add(_messageFactory.pong());                break;
        case MessageType.OK   : messages.add(_messageFactory.ok());                  break;
        case MessageType.ERROR: messages.add(_messageFactory.errorFromProto(proto)); break;

        default: throw FormatException('Invalid message packet received: Unsupported incoming message type: ${proto.messageType}');
      }
    }

    return messages;
  }

  String _yankProtoString() {
    bool hitCR    = false;
    int  protoLen = 0;

    dataBuffer.takeWhile((rune) {
      if (!hitCR && rune == _CR) { // \r
        hitCR = true;
      } else if (hitCR && rune == _LF) { // \n
        return false;
      } else {
        hitCR = false; // -- any \r must be directly followed by \n, otherwise, it's just a plain \r and we don't care about it
        protoLen++;
      }
      return true;
    }).toList();

    return ascii.decode(_yankRange(protoLen, ignoreTail: 2));
  }

  MessageProto _parseProtoString(String protoString) {
    List   args    = protoString.split(RegExp(r'\s')); // -- split and drop the final \r\n
    String control = args.removeAt(0);

    MessageType messageType;
    try {
      messageType = Enum.fromString<MessageType>(control);
    } catch(e) {
      throw FormatException('Invalid message packet received: Unsupported incoming message type: $control');
    }

    switch (messageType) {
      case MessageType.MESSAGE: return _protoFactory.data(args[0], args[1], int.parse(args.last), replySubject: (args.length == 4 ? args[2] : null));
      case MessageType.PING   : return _protoFactory.ping();
      case MessageType.PONG   : return _protoFactory.pong();
      case MessageType.OK     : return _protoFactory.ok();
      case MessageType.ERROR  : return _protoFactory.error(protoString.substring(control.length + 1));
      case MessageType.INFO   :
        var info = json.decode(args[0]);
        return _protoFactory.info(
          serverId    : info['server_id'],
          version     : info['version'],
          goVersion   : info['go'],
          host        : info['host'],
          port        : info['port'],
          maxPayload  : info['max_payload'],
          protocol    : info['proto'],
          clientId    : info['client_id'],
          authRequired: info['auth_required'],
          tlsRequired : info['tls_required'],
          tlsVerify   : info['tls_verify'],
          connectUrls : (info['connect_urls']??[]).cast<String>(),
        );

      default:
        throw UnsupportedError('Unsupported message packet received: Unsupported incoming message type: ${Enum.asString(messageType)}');
    }
  }

  Uint8List _yankRange(int length, {int ignoreHead = 0, int ignoreTail = 0}) {
    var yanked = dataBuffer.sublist(ignoreHead, length);             // -- take what we want
    dataBuffer = dataBuffer.sublist(ignoreHead + length + ignoreTail); // -- damn the consequences (remove from buffer)

    return yanked;
  }
}
