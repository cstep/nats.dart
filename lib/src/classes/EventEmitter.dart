part of nats_dart;

mixin EventEmitter {
  final EventBus _eventBus = EventBus();

  void emitEvent(Event event) => _eventBus.fire(event);
}

mixin EventSubscriber {
  final EventBus                 _eventBus     = EventBus();
  final Map<Type, Stream<Event>> _eventStreams = {};

  Stream<E> on<E extends Event>() => (_eventStreams.putIfAbsent(E, () => _eventBus.on<E>()) as Stream<E>);
}

abstract class Events implements EventEmitter, EventSubscriber /* with EventEmitter, EventSubscriber */ {
  // NOTE: this duplicates, verbatim, the code from EventEmitter and EventSubscriber
  //       any changes are made to this mixin should be reflected, verbatim, in those mixins as well (and vice-versa)

  @override final EventBus                 _eventBus     = EventBus();
  @override final Map<Type, Stream<Event>> _eventStreams = {};

  @override void emitEvent(Event event) => _eventBus.fire(event);

  @override Stream<E> on<E extends Event>() => (_eventStreams.putIfAbsent(E, () => _eventBus.on<E>()) as Stream<E>);
}
