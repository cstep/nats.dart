part of nats_dart;

class ClientOptions {
  static const Duration DEFAULT_PING_INTERVAL          = Duration(minutes: 2);
  static const int      DEFAULT_MAX_PINGS_OUTSTANDING  = 1;
  static const bool     DEFAULT_ATTEMPT_TO_RECONNECT   = true;
  static const int      DEFAULT_MAX_RECONNECT_ATTEMPTS = 10;
  static const Duration DEFAULT_RECONNECT_DELAY        = Duration(seconds: 2);
  static const Duration DEFAULT_REQUEST_TIMEOUT        = Duration(seconds: 3);

  Client _client;

  /// Interval in milliseconds that the client will send PINGs to the server. @see [maxPingOut]
  Duration     _pingInterval;
  Duration get  pingInterval => _pingInterval;
  void     set  pingInterval(Duration value) {
    _pingInterval = value;
    _client?._onPingIntervalUpdate(_pingInterval);
  }

  /// Maximum number of client PINGs that can be outstanding before the connection is considered stale
  int maxPingsOutstanding;

  /// Maximum number of consecutive reconnect attempts before the client closes the connection. Specify `-1` to retry forever.
  int maxReconnectAttempts;

  /// Specifies whether the client should attempt reconnects.
  bool attemptToReconnect;

  /// Specifies the interval in milliseconds between reconnect attempts.
  Duration reconnectDelay;

  /// Maximum time to wait while attempting to connect
  Duration connectTimeout;

  ClientOptions({Duration pingInterval, this.maxPingsOutstanding, this.maxReconnectAttempts, this.attemptToReconnect, this.reconnectDelay, this.connectTimeout}) {
    this.pingInterval      = pingInterval??DEFAULT_PING_INTERVAL;
    maxPingsOutstanding  ??= DEFAULT_MAX_PINGS_OUTSTANDING;
    maxReconnectAttempts ??= DEFAULT_MAX_RECONNECT_ATTEMPTS;
    attemptToReconnect   ??= DEFAULT_ATTEMPT_TO_RECONNECT;
    reconnectDelay       ??= DEFAULT_RECONNECT_DELAY;
    connectTimeout       ??= DEFAULT_CONNECT_TIMEOUT;
  }

  factory ClientOptions.from(ClientOptions other) {
    return ClientOptions(
      pingInterval        : other.pingInterval,
      maxPingsOutstanding : other.maxPingsOutstanding,
      maxReconnectAttempts: other.maxReconnectAttempts,
      attemptToReconnect  : other.attemptToReconnect,
      reconnectDelay      : other.reconnectDelay,
      connectTimeout      : other.connectTimeout,
    );
  }

  ClientOptions clone() => ClientOptions.from(this); // ignore: use_to_and_as_if_applicable
}
