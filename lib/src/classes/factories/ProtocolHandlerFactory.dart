part of nats_dart;

class ProtocolHandlerFactory {
  ProtocolHandler create(ServerSet servers) => ProtocolHandler(servers);
}
