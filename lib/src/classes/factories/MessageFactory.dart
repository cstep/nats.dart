part of nats_dart;

/// allows dependency injection via our ioc container
class MessageFactory {
  InfoMessage info(String serverId, String version, String go, String host, int port, int maxPayload, int protocol, int clientId, {bool authRequired, bool tlsRequired, bool tlsVerify, List<String> connectUrls}) {
    return InfoMessage(serverId, version, go, host, port, maxPayload, protocol, clientId, authRequired: authRequired, tlsRequired: tlsRequired, tlsVerify: tlsVerify, connectUrls: connectUrls);
  }

  @factory
  ConnectMessage connect(ServerOptions options) {
    return ConnectMessage(options);
  }

  @factory
  PubMessage pub(String subject, dynamic payload, {Encoding encoding = ascii, String replySubject}) {
    return PubMessage(subject, payload, replySubject: replySubject, encoding: encoding);
  }

  @factory
  SubMessage sub(String subject, String sid, {String queue}) {
    return SubMessage(subject, sid, queue: queue);
  }

  @factory
  UnsubMessage unsub(String sid, {int max}) {
    return UnsubMessage(sid, max: max);
  }

  @factory
  DataMessage data(String subject, String sid, Uint8List payload, {Encoding encoding = ascii, String replySubject}) {
    return DataMessage(subject, sid, payload, replySubject: replySubject, encoding: encoding);
  }

  @factory
  PingMessage ping() {
    return PingMessage();
  }

  @factory
  PongMessage pong() {
    return PongMessage();
  }

  @factory
  OkMessage ok() {
    return OkMessage();
  }

  @factory
  ErrorMessage error(String message) {
    return ErrorMessage(message);
  }

  // --

  @factory
  InfoMessage infoFromProto(InfoMessageProto proto) {
    return InfoMessage.fromProto(proto);
  }

  @factory
  DataMessage dataFromProto(DataMessageProto proto, Uint8List payload, {Encoding encoding = ascii}) {
    return DataMessage.fromProto(proto, payload, encoding: encoding);
  }

  @factory
  PingMessage pingFromProto(PingMessageProto proto) {
    return PingMessage.fromProto(proto);
  }

  @factory
  PongMessage pongFromProto(PongMessageProto proto) {
    return PongMessage.fromProto(proto);
  }

  @factory
  OkMessage okFromProto(OkMessageProto proto) {
    return OkMessage.fromProto(proto);
  }

  @factory
  ErrorMessage errorFromProto(ErrorMessageProto proto) {
    return ErrorMessage.fromProto(proto);
  }

  // --

  @factory
  SubMessage subFromSubscription(Subscription subscription) {
    return SubMessage.fromSubscription(subscription);
  }

  @factory
  UnsubMessage unsubFromSubscription(Subscription subscription, {int max}) {
    return UnsubMessage.fromSubscription(subscription, max: max);
  }
}
