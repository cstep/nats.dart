part of nats_dart;

class MessageProtoFactory {
  @factory
  InfoMessageProto info({String serverId, String version, String goVersion, String host, int port, int maxPayload, int protocol, int clientId, bool authRequired, bool tlsRequired, bool tlsVerify, List<String> connectUrls}) {
    return InfoMessageProto(serverId: serverId, version: version, goVersion: goVersion, host: host, port: port, maxPayload: maxPayload, protocol: protocol, clientId: clientId, authRequired: authRequired, tlsRequired: tlsRequired, tlsVerify: tlsVerify, connectUrls: connectUrls);
  }

  @factory
  ConnectMessageProto connect(ServerOptions options) {
    return ConnectMessageProto(options);
  }

  @factory
  PubMessageProto pub(String subject, int byteLength, {String replySubject}) {
    return PubMessageProto(subject, byteLength, replySubject: replySubject);
  }

  @factory
  SubMessageProto sub(String subject, String sid, {String queue}) {
    return SubMessageProto(subject, sid, queue: queue);
  }

  @factory
  UnsubMessageProto unsub(String sid, {int max}) {
    return UnsubMessageProto(sid, max: max);
  }

  @factory
  DataMessageProto data(String subject, String sid, int byteLength, {String replySubject}) {
    return DataMessageProto(subject, sid, byteLength, replySubject: replySubject);
  }

  @factory
  PingMessageProto ping() {
    return PingMessageProto();
  }

  @factory
  PongMessageProto pong() {
    return PongMessageProto();
  }

  @factory
  OkMessageProto ok() {
    return OkMessageProto();
  }

  @factory
  ErrorMessageProto error(String message) {
    return ErrorMessageProto(message);
  }

}
