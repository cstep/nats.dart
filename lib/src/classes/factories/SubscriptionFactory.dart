part of nats_dart;

class SubscriptionFactory {
  Subscription subscription(ProtocolHandler protocolHandler, String subject, {String queue, int max, String sid}) => Subscription(protocolHandler, subject, queue: queue, max: max, sid: sid);

  RequestSubscription requestSubscription(ProtocolHandler protocolHandler, String inbox) => RequestSubscription(protocolHandler, inbox);
}
