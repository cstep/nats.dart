part of nats_dart;

class ProtocolParserFactory {
  ProtocolParser create(Encoding encoding) => ProtocolParser(encoding);
}

