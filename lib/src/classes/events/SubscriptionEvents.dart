part of nats_dart;

abstract class SubscriptionEvent extends NonMessageEvent {
  final Subscription subscription;

  String get sid     => subscription.sid;
  String get subject => subscription.subject;
  String get queue   => subscription.queue;

  SubscriptionEvent(this.subscription);
}

class   SubscribeEvent extends SubscriptionEvent {   SubscribeEvent(Subscription subscription): super(subscription); }
class UnsubscribeEvent extends SubscriptionEvent { UnsubscribeEvent(Subscription subscription): super(subscription); }
class     RequestEvent extends SubscriptionEvent {     RequestEvent(Subscription subscription): super(subscription); }
