part of nats_dart;

class ServersChangedEvent extends NonMessageEvent {
  final List<String> added;
  final List<String> deleted;

  ServersChangedEvent({this.added = const [], this.deleted = const []});
}
