part of nats_dart;

abstract class Event {
}

abstract class NonMessageEvent extends Event {
}

abstract class MessageEvent extends Event {
}
