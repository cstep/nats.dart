part of nats_dart;

abstract class ConnectionEvent extends NonMessageEvent {
  final String url;

  ConnectionEvent(this.url);
}

class ConnectEvent extends ConnectionEvent {
  ConnectEvent(String url): super(url);
}

class ReconnectEvent extends ConnectionEvent {
  ReconnectEvent(String url): super(url);
}

class DisconnectEvent extends ConnectionEvent {
  DisconnectEvent(String url): super(url);
}
