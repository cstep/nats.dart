part of nats_dart;

class SubscriptionList extends DelegatingList<Subscription> with EventEmitter {
  @override final List<Subscription> delegate = [];

  void unsubscribeAll() => toList().forEach(this.unsubscribe);

  bool unsubscribe(Subscription subscription) {
    var didExist = super.remove(subscription);
    if (didExist) {
      subscription.unsubscribe(max: 0);
    }

    return didExist;
  }

  @override bool remove(covariant Subscription subscription) => unsubscribe(subscription);

  @override void add(covariant Subscription subscription) => super.add(subscription);

  @override void operator []=(int index, Subscription subscription) {
    throw UnsupportedError("Index access not supported for Subscriptions collection");
  }
}
