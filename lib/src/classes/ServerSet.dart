part of nats_dart;

class ServerSet extends SetBase<Server> {
  final LinkedHashSet<Server> _servers = LinkedHashSet();

  int               __currentIndex = 0;
  int  get           _currentIndex => __currentIndex;
  void set           _currentIndex(int value) => (__currentIndex %= _servers.length);

  Server        get currentServer => (_currentIndex < 0 ? null : _servers.toList()[_currentIndex]);
  @override int get length        => _servers.length;
  List<Server>  get servers       => UnmodifiableListView(_servers);

  ServerSet();

  ServerSet.from(List<dynamic> urls, {String firstServerUrl, bool randomize = false}) {
      urls ??= [];

      if (randomize) {
        urls.shuffle();
      }

      urls = {
        if ((firstServerUrl??'').isNotEmpty) firstServerUrl,
        ...urls
      }.toList();

      if (urls.isEmpty) {
        urls.add(null);
      }
      urls.forEach((url) => _servers.add(Server(url)));
  }

  Server next() { // previously named `selectServer()`
    _currentIndex++;
    return currentServer;
  }

  @override
  bool add(Server server) => _servers.add(server);

  @override
  bool remove(covariant Server server) => _servers.remove(server); // TODO: if this was the currentServer, select next server in line?

  @override
  bool contains(covariant Server element) => _servers.contains(element);

  @override
  Iterator<Server> get iterator => _servers.iterator;

  @override
  Server lookup(covariant Server element) => _servers.lookup(element);

  @override
  Set<Server> toSet() => _servers.toSet();
}
