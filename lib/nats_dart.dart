library nats_dart;

// ======== Imports
import 'dart:async';
import 'dart:collection';
import 'dart:convert';
import 'dart:core';
import 'dart:core' as core;
import 'dart:io';
import 'dart:typed_data';
import 'package:meta/meta.dart';
import 'package:event_bus/event_bus.dart';
import 'package:logging/logging.dart';
import 'package:pedantic/pedantic.dart';
import 'package:quiver/strings.dart';
import 'package:quiver/collection.dart';
import 'package:uuid/uuid.dart';
import 'package:kiwi/kiwi.dart';
import 'watchdog.dart';

// ======== Parts
part 'src/classes/NatsError.dart';
part 'src/classes/Subscription.dart';
part 'src/classes/factories/SubscriptionFactory.dart';
part 'src/classes/SubscriptionList.dart';
part 'src/classes/RequestSubscription.dart';
part 'src/classes/EventEmitter.dart';
part 'src/classes/ProtocolParser.dart';
part 'src/classes/factories/ProtocolParserFactory.dart';
part 'src/classes/ProtocolHandler.dart';
part 'src/classes/factories/ProtocolHandlerFactory.dart';
part 'src/classes/events/_Event.dart';
part 'src/classes/events/ConnectionEvents.dart';
part 'src/classes/events/ServerEvents.dart';
part 'src/classes/events/SubscriptionEvents.dart';
part 'src/classes/ServerOptions.dart';
part 'src/classes/Client.dart';
part 'src/classes/ClientOptions.dart';
part 'src/classes/Server.dart';
part 'src/classes/ServerSet.dart';
part 'src/utilities/Enums.dart';
part 'src/utilities/Strings.dart';
part 'src/utilities/Streams.dart';
part 'src/utilities/misc.dart';

  // -------- messages
    part 'src/classes/factories/MessageFactory.dart';

    // -------- interfaces
    part 'src/classes/messages/interfaces/_Message.dart';
    part 'src/classes/messages/interfaces/_IncomingMessage.dart';
    part 'src/classes/messages/interfaces/_OutgoingMessage.dart';
    part 'src/classes/messages/interfaces/_BidirectionalMessage.dart';
    part 'src/classes/messages/interfaces/_PresenceMessage.dart';
    part 'src/classes/messages/interfaces/_PayloadMessage.dart';

    // -------- outgoing messages
    part 'src/classes/messages/ConnectMessage.dart';
    part 'src/classes/messages/PubMessage.dart';
    part 'src/classes/messages/SubMessage.dart';
    part 'src/classes/messages/UnsubMessage.dart';

    // -------- incoming messages
    part 'src/classes/messages/InfoMessage.dart';
    part 'src/classes/messages/DataMessage.dart';
    part 'src/classes/messages/OkMessage.dart';
    part 'src/classes/messages/ErrorMessage.dart';

    // -------- bidirectional messages
    part 'src/classes/messages/PingMessage.dart';
    part 'src/classes/messages/PongMessage.dart';

  // -------- protos
    // -------- interfaces
    part 'src/classes/factories/MessageProtoFactory.dart';
    part 'src/classes/protos/interfaces/_MessageProto.dart';
    part 'src/classes/protos/interfaces/_PayloadMessageProto.dart';

    // -------- implementations
    part 'src/classes/protos/InfoMessageProto.dart';
    part 'src/classes/protos/ConnectMessageProto.dart';
    part 'src/classes/protos/PubMessageProto.dart';
    part 'src/classes/protos/SubMessageProto.dart';
    part 'src/classes/protos/UnsubMessageProto.dart';
    part 'src/classes/protos/DataMessageProto.dart';
    part 'src/classes/protos/PingMessageProto.dart';
    part 'src/classes/protos/PongMessageProto.dart';
    part 'src/classes/protos/OkMessageProto.dart';
    part 'src/classes/protos/ErrorMessageProto.dart';

// ======== Exports

// ======== Declarations
const VERSION                 = '0.0.1';
const DEFAULT_PORT            = 4222;
const DEFAULT_HOST            = 'localhost';
const DEFAULT_CONNECT_TIMEOUT = Duration(seconds: 10);

typedef MessageHandler = void Function(DataMessage message, {NatsError error});

enum MessageType {
  INFO,    /// | C <--   S | Sent to client after initial TCP/IP connection
  CONNECT, /// | C   --> S | Sent to server to specify connection information
  PUB,     /// | C   --> S | Publish a message to a subject, with optional reply subject
  SUB,     /// | C   --> S | Subscribe to a subject (or subject wildcard)
  UNSUB,   /// | C   --> S | Unsubscribe (or auto-unsubscribe) from subject
  MESSAGE, /// | C <--   S | Delivers a message payload to a subscriber
  PING,    /// | C <---> S | PING keep-alive message
  PONG,    /// | C <---> S | PONG keep-alive response
  OK,      /// | C <--   S | Acknowledges well-formed protocol message in `verbose` mode
  ERROR,   /// | C <--   S | Indicates a protocol error. May cause client disconnect.
}

enum PayloadType {
    STRING,/// Specifies a string payload. This is standard
    JSON,  /// Specifies payloads are JSON
    BINARY /// Specifies payloads are binary (Buffer)
}

abstract class log {
  static Logger get general  => Logger('nats.general' );
  static Logger get outgoing => Logger('nats.outgoing');
  static Logger get incoming => Logger('nats.incoming');
}

KiwiContainer container = KiwiContainer()
  ..registerInstance(MessageProtoFactory())
  ..registerInstance(MessageFactory())
  ..registerInstance(SubscriptionFactory())
  ..registerInstance(ProtocolHandlerFactory())
  ..registerInstance(ProtocolParserFactory());

MessageFactory         get _messageFactory         => container.resolve<MessageFactory>();
MessageProtoFactory    get _protoFactory           => container.resolve<MessageProtoFactory>();
SubscriptionFactory    get _subscriptionFactory    => container.resolve<SubscriptionFactory>();
ProtocolHandlerFactory get _protocolHandlerFactory => container.resolve<ProtocolHandlerFactory>();
ProtocolParserFactory  get _protocolParserFactory  => container.resolve<ProtocolParserFactory>();
