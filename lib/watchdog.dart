// TODO: move this out to its very own lib

library watchdog;

import 'dart:async';

part 'watchdog/_Watchdog.dart';
part 'watchdog/SyncWatchdog.dart';
part 'watchdog/AsyncWatchdog.dart';
part 'watchdog/Heartbeat.dart';
