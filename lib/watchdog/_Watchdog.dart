part of watchdog;

abstract class Watchdog {
  Timer    _timer;
  DateTime _lastCheck;
  Duration _currentInterval;
  Function _checkCallback;
  Function _failureCallback;

  Duration get interval => _currentInterval;
  void     set interval(Duration newInterval) => _buildTimer(newInterval);

  void doCheck();

  void cancel() => _timer?.cancel();

  void reset() {
    var newInterval = _currentInterval;

    cancel();
    _lastCheck       = null;
    _currentInterval = null;

    _buildTimer(newInterval);
  }

  void _buildTimer(Duration newInterval) {
    if (newInterval == _currentInterval) { // -- hasn't actually changed
      return; // -- early
    }

    Duration partialWait;

    // ======== Cancel Our Existing Timer
    if (_timer?.isActive??false) { // -- we have an existing timer
      _timer.cancel();

      var sinceLast = DateTime.now().difference(_lastCheck);
      if (sinceLast < newInterval) { // -- we need a partial initial wait before firing our new timer
        partialWait = newInterval - sinceLast;
      }
    }

    // ======== Create Our New Timer
    _currentInterval = newInterval;

    _timer = Timer(partialWait??Duration(), () {
      if (partialWait != null) { // -- we had a partial wait time
        // -------- fire immediately, then create periodic timer
        doCheck();
      }

      // -------- create periodic timer
      _timer = Timer.periodic(newInterval, (_) => doCheck());
    });
  }
}
