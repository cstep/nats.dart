part of watchdog;

typedef SyncWatchdogCheck = bool Function();

class SyncWatchdog extends Watchdog {
  @override covariant SyncWatchdogCheck _checkCallback;

  SyncWatchdog(Duration interval, SyncWatchdogCheck checkCallback, {void Function() failureCallback}) {
    ArgumentError.checkNotNull(interval,      'interval'     );
    ArgumentError.checkNotNull(checkCallback, 'checkCallback');

    this._checkCallback   = checkCallback;
    this._failureCallback = failureCallback??(() => throw TimeoutException('Watchdog check failed', _currentInterval));
    this.interval         = interval;
  }

  @override
  void doCheck() {
    var result = _checkCallback()??false;
    if (!result) {
      _failureCallback();
    }
  }
}
