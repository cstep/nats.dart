part of watchdog;

typedef HeartbeatCheck = FutureOr<void> Function();

class Heartbeat extends Watchdog {
  @override covariant HeartbeatCheck _checkCallback;

  int maxMissed;

  int _outstandingCount = 0;
  int get outstandingCount => _outstandingCount; // -- read only

  Heartbeat(Duration interval, HeartbeatCheck checkCallback, {void Function() failureCallback, this.maxMissed}) {
    maxMissed ??= 1;
    assert(maxMissed > 0, 'maxMissed, if supplied, must be greater than 0');
    ArgumentError.checkNotNull(interval,      'interval'     );
    ArgumentError.checkNotNull(checkCallback, 'checkCallback');

    this._outstandingCount = 0;
    this._checkCallback    = checkCallback;
    this._failureCallback  = failureCallback??(() => throw TimeoutException('Maximum number of missed heartbeats ($maxMissed) exceeded'));
    this.interval          = interval;
  }

  @override
  void reset() {
    super.reset();
    _outstandingCount = 0;
  }

  @override
  void doCheck() {
    if (_outstandingCount >= maxMissed) {
      _failureCallback();
    } else {
      _outstandingCount++;
      _checkCallback();
    }
  }

  void heartbeat() {
    reset();
  }
}
