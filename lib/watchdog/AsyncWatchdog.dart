part of watchdog;

typedef AsyncWatchdogCheck = FutureOr<bool> Function();

class AsyncWatchdog extends Watchdog {
  @override covariant AsyncWatchdogCheck _checkCallback;

  AsyncWatchdog(Duration interval, AsyncWatchdogCheck checkCallback, {void Function() failureCallback}) {
    ArgumentError.checkNotNull(interval,      'interval'     );
    ArgumentError.checkNotNull(checkCallback, 'checkCallback');

    this._checkCallback   = checkCallback;
    this._failureCallback = failureCallback??(() => throw TimeoutException('Watchdog check failed', _currentInterval));
    this.interval         = interval;
  }

  @override
  void doCheck() {
    Future(_checkCallback).then((result) {
      result ??= false;
      if (!result) {
        _failureCallback();
      }
    });
  }
}
